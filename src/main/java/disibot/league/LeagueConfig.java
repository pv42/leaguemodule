package disibot.league;

import disibot.config.ConfigSection;

import java.time.DayOfWeek;

import static java.time.DayOfWeek.THURSDAY;

public class LeagueConfig extends ConfigSection {
    private static final String WTL_HOUR = "wtlHour";
    private static final String WTL_DAY = "wtlDay";
    private static final String HIDE_INACTIVE = "hideInactivePlayers";
    private static final String SEND_WHEN_EMPTY = "sendWhenEmpty";

    public LeagueConfig() { // init with default values
        super();
        setInt(WTL_DAY, THURSDAY.getValue());
        setInt(WTL_HOUR, 18);
        setBoolean(HIDE_INACTIVE, false);
        setBoolean(SEND_WHEN_EMPTY, false);
    }

    public LeagueConfig(ConfigSection configSection) {
        super(configSection);
    }

    public DayOfWeek getWtlDay() {
        return DayOfWeek.of(getInt(WTL_DAY));
    }

    public int getWtlHour() {
        return getInt(WTL_HOUR);
    }

    public boolean isHideInactivePlayers() {
        return getBoolean(HIDE_INACTIVE);
    }

    public boolean doSendWhenEmpty() {
        return getBoolean(SEND_WHEN_EMPTY);
    }
}
