package disibot.league.api.impl;

import com.google.gson.Gson;
import disibot.league.api.exceptions.LeagueAPIException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import java.io.IOException;
import java.lang.reflect.Type;
import java.net.URL;
import java.time.Duration;

public class TimedGsonCachedURLDownloader<T> extends ParameterizedGsonCachedURLDownloader<T, Void> {
    private static final Logger LOG = LoggerFactory.getLogger(TimedGsonCachedURLDownloader.class);

    public TimedGsonCachedURLDownloader(Duration validityDuration, URL url, @Nullable String diskCachePath, Gson gson, Class<T> clazz) {
        super(validityDuration, parameter -> url, diskCachePath, gson, clazz);
    }

    public TimedGsonCachedURLDownloader(Duration validityDuration, URL url, @Nullable String diskCachePath, Gson gson, Type type) {
        super(validityDuration, parameter -> url, diskCachePath, gson, type);
    }

    public T get() throws IOException, LeagueAPIException {
        return get(null);
    }
}
