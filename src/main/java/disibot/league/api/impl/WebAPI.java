package disibot.league.api.impl;

import javax.annotation.Nonnull;
import java.net.HttpURLConnection;
import java.net.ProtocolException;

public abstract class WebAPI {
    protected static void setConnectionParams(@Nonnull HttpURLConnection connection) throws ProtocolException {
        connection.setRequestMethod("GET");
        connection.setConnectTimeout(5000);
        connection.setReadTimeout(5000);
        connection.setInstanceFollowRedirects(false);
    }


    public abstract void setDownloader(HttpURLDownloader downloader);
}
