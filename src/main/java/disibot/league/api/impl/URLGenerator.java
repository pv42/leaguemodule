package disibot.league.api.impl;

import disibot.league.api.exceptions.LeagueAPIException;

import java.io.IOException;
import java.net.URL;

public interface URLGenerator<P> {
    URL getURL(P parameter) throws IOException, LeagueAPIException;
}
