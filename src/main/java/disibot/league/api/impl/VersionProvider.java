package disibot.league.api.impl;

import disibot.league.api.dataDragon.entities.Version;
import disibot.league.api.exceptions.LeagueAPIException;

import java.io.IOException;

public interface VersionProvider {
    Version get() throws IOException, LeagueAPIException;
}
