package disibot.league.api.impl;

import disibot.league.api.exceptions.BadGatewayException;
import disibot.league.api.exceptions.BadRequestException;
import disibot.league.api.exceptions.GatewayTimeOutException;
import disibot.league.api.exceptions.InternalServerErrorException;
import disibot.league.api.exceptions.InvalidAPIKeyException;
import disibot.league.api.exceptions.LeagueAPIException;
import disibot.league.api.exceptions.NoAPIKeyException;
import disibot.league.api.exceptions.NotFoundException;
import disibot.league.api.exceptions.RateLimitException;
import net.dv8tion.jda.api.exceptions.RateLimitedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;

// R object, U up to date parameter (version or time)
public abstract class CachedURLDownloader<R, U, P> {
    private static final Logger LOG = LoggerFactory.getLogger(CachedURLDownloader.class);
    /*
    -> T in cache ? -Y->                                                            Object in Cache -> return cache
               |     open download InputStream -> create T using generator and write to cache ^
               N      ^              ^                ^
               v      N              N                |
          use disk cache -Y-> disk cache valid ? -Y-> |
    */

    protected final Path diskCachePath;
    protected Map<P, R> cachedObject;
    private HttpURLDownloader downloader = new HttpURLDownloader();

    public CachedURLDownloader(Path diskCachePath) {
        this.diskCachePath = diskCachePath;
        cachedObject = new HashMap<>();
    }

    protected static void setConnectionParams(@Nonnull HttpURLConnection connection) throws ProtocolException {
        connection.setRequestMethod("GET");
        connection.setConnectTimeout(5000);
        connection.setReadTimeout(5000);
        connection.setInstanceFollowRedirects(false);
    }

    protected static void checkResponse(HttpURLConnection connection) throws LeagueAPIException, IOException {
        int response = connection.getResponseCode();
        if (response == HttpURLConnection.HTTP_OK) return;
        if (response == HttpURLConnection.HTTP_BAD_REQUEST) throw new BadRequestException();
        if (response == HttpURLConnection.HTTP_UNAUTHORIZED) throw new NoAPIKeyException();
        if (response == HttpURLConnection.HTTP_FORBIDDEN) throw new InvalidAPIKeyException();
        if (response == HttpURLConnection.HTTP_NOT_FOUND) throw new NotFoundException();
        if (response == 429) {
            String retryString =  connection.getHeaderField("retry-after");
            int retry_after = 0;
            try {
                retry_after = Integer.parseInt(retryString);
            } catch (NumberFormatException ignored) {
            }
            throw new RateLimitException(retry_after);
        }
        if (response == HttpURLConnection.HTTP_INTERNAL_ERROR) throw new InternalServerErrorException();
        if (response == HttpURLConnection.HTTP_BAD_GATEWAY) throw new BadGatewayException();
        if (response == HttpURLConnection.HTTP_GATEWAY_TIMEOUT) throw new GatewayTimeOutException();
        throw new LeagueAPIException("The server answered with an unexpected response code:"  + response);
    }

    public void setDownloader(HttpURLDownloader downloader) {
        this.downloader = downloader;
    }

    public abstract R get(P p) throws IOException, LeagueAPIException;

    protected abstract void ensureCacheIsUpToDate(U u, P p) throws IOException, LeagueAPIException;

    protected abstract boolean isCacheExpired(U u, P p);

    protected abstract R deserialize(InputStream in);

    protected void updateCache(U u, P p) throws IOException, LeagueAPIException {
        URL url = getURL(u, p);
        LOG.info("Renewing cache for " + url);
        InputStream inputStream = getSourceStream(u, p);
        cachedObject.put(p, deserialize(inputStream));
        inputStream.close();
        if (cachedObject.get(p) == null) {
            LOG.warn("Disk cache for " + url + " seems to be corrupted, deleting");
            deleteDiskCache(u, p);
            inputStream = getSourceStream(u, p);
            cachedObject.put(p, deserialize(inputStream));
            inputStream.close();
        }
    }

    public void deleteDiskCache(U u, P p) throws IOException {
        if (Files.exists(getDiskCachePath(u, p))) Files.delete(getDiskCachePath(u, p));
    }

    protected abstract Path getDiskCachePath(U u, P p);

    protected abstract boolean isDiskCacheValid(U u, P p);

    private void ensureDiskCachePathExists(U u, P p) throws IOException {
        Path cachePath = getDiskCachePath(u, p);
        if (cachePath.getParent() != null && !Files.exists(cachePath.getParent())) {
            Files.createDirectories(cachePath.getParent());
        }
    }

    protected abstract URL getURL(U u, P p) throws IOException, LeagueAPIException;

    protected InputStream getSourceStream(U u, P p) throws IOException, LeagueAPIException {
        if (diskCachePath != null) {
            if (isDiskCacheValid(u, p)) {
                return new BufferedInputStream(Files.newInputStream(getDiskCachePath(u, p)));
            } else {
                ensureDiskCachePathExists(u, p);
                InputStream in = connect(u, p);
                OutputStream out = Files.newOutputStream(getDiskCachePath(u, p));
                // return a stream, that write to disk while reading
                return new CloningInputStream(in, out);
            }
        } else {
            return connect(u, p);
        }
    }

    protected InputStream connect(U u, P p) throws IOException, LeagueAPIException {
        URL url = getURL(u, p);
        LOG.info("Connecting to " + url);
        HttpURLConnection connection = downloader.download(url);
        setConnectionParams(connection);
        connection.connect();
        try {
            checkResponse(connection);
        } catch (NoAPIKeyException e) {
            String s = "";
            int c;
            while (-1 != (c = connection.getErrorStream().read())){
                s += (char)c;
            }
            System.out.println("\nNoApiKeyError Stream:" + s);
            throw e;
        }
        return connection.getInputStream();
    }

    protected static class CloningInputStream extends InputStream {

        private InputStream source;
        private OutputStream target;

        CloningInputStream(InputStream source, OutputStream target) {
            this.source = source;
            this.target = target;
        }

        @Override
        public int read() throws IOException {
            int b = source.read();
            if (b != -1) target.write(b);
            return b;
        }

        @Override
        public void close() throws IOException {
            source.close();
            target.close();
        }
    }
}
