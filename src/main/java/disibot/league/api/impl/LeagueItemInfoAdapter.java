package disibot.league.api.impl;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import disibot.league.api.dataDragon.entities.LeagueItem;
import disibot.league.api.dataDragon.entities.LeagueItemInfos;

import java.io.IOException;

public class LeagueItemInfoAdapter extends TypeAdapter<LeagueItem> {
    private LeagueItemInfos items;

    public LeagueItemInfoAdapter(LeagueItemInfos items) {
        this.items = items;
    }

    @Override
    public void write(JsonWriter out, LeagueItem value) throws IOException {
        out.value(value.getId());
    }

    @Override
    public LeagueItem read(JsonReader in) throws IOException {
        return items.getItemById(in.nextLong());
    }
}
