package disibot.league.api.impl;

import com.google.gson.Gson;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.nio.file.Path;

public abstract class GsonCachedURLDownloader<R, U, P> extends CachedURLDownloader<R, U, P> {
    private final Gson gson;
    private final Type type;

    public GsonCachedURLDownloader(Path diskCachePath, Gson gson, Class<R> clazz) {
        super(diskCachePath);
        this.gson = gson;
        this.type = clazz;
    }


    public GsonCachedURLDownloader(Path diskCachePath, Gson gson, Type type) {
        super(diskCachePath);
        this.gson = gson;
        this.type = type;
    }

    @Override
    protected R deserialize(InputStream in) {
        return gson.fromJson(new InputStreamReader(in), type);
    }
}
