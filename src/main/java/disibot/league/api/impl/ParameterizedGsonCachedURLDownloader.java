package disibot.league.api.impl;

import com.google.gson.Gson;
import disibot.league.api.exceptions.LeagueAPIException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import java.io.IOException;
import java.lang.reflect.Type;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.FileTime;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

// parameter P, Response type R
public class ParameterizedGsonCachedURLDownloader<R, P> extends GsonCachedURLDownloader<R, Void, P> {
    private static final Logger LOG = LoggerFactory.getLogger(ParameterizedGsonCachedURLDownloader.class);
    /*
             -> R in cache ? -Y->                                                            Object in Cache -> return cache
                        |     open download InputStream -> create T using generator and write to cache ^
                        N      ^              ^                ^
                        v      N              N                |
                   use disk cache -Y-> disk cache valid ? -Y-> |


         */
    private final URLGenerator<P> url;
    private final Duration validityDuration;
    private final String diskCachePathFormat; // null -> dont use disk cache
    private final Map<P, LocalDateTime> cacheTimeStamps;

    public ParameterizedGsonCachedURLDownloader(Duration validityDuration, URLGenerator<P> url, @Nullable String diskCachePathFormat, Gson gson, Class<R> clazz) {
        super(diskCachePathFormat == null ? null : Paths.get("."), gson, clazz); /// only checks for null /not null
        this.url = url;
        this.validityDuration = validityDuration;
        this.diskCachePathFormat = diskCachePathFormat;
        cacheTimeStamps = new HashMap<>();
    }

    public ParameterizedGsonCachedURLDownloader(Duration validityDuration, URLGenerator<P> url, @Nullable String diskCachePathFormat, Gson gson, Type type) {
        super(diskCachePathFormat == null ? null : Paths.get("."), gson, type); /// only checks for null /not null
        this.url = url;
        this.validityDuration = validityDuration;
        this.diskCachePathFormat = diskCachePathFormat;
        cacheTimeStamps = new HashMap<>();
    }

    @Override
    public R get(P param) throws IOException, LeagueAPIException {
        ensureCacheIsUpToDate(null, param);
        return cachedObject.get(param);
    }

    @Override
    protected void ensureCacheIsUpToDate(Void u, P param) throws IOException, LeagueAPIException {
        if (isCacheExpired(u, param)) {
            updateCache(u, param);
            cacheTimeStamps.put(param, LocalDateTime.now());
        }
    }

    @Override
    protected boolean isCacheExpired(Void aVoid, P param) {
        LocalDateTime now = LocalDateTime.now();
        if (!cachedObject.containsKey(param)) return true;
        return !cacheTimeStamps.get(param).plus(validityDuration).isAfter(now);
    }

    @Override
    protected boolean isDiskCacheValid(Void u, P param) {
        Path path = getDiskCachePath(u, param);
        if (Files.notExists(path)) return false;
        FileTime lastModified;
        try {
            lastModified = Files.getLastModifiedTime(path);
        } catch (IOException e) {
            LOG.warn("Could not read cache files last modified timestamp");
            return false;
        }
        return lastModified.toInstant().isAfter(Instant.now().minus(validityDuration));
    }

    @Override
    protected URL getURL(Void u, P param) throws IOException, LeagueAPIException {
        return url.getURL(param);
    }

    @Override
    protected Path getDiskCachePath(Void aVoid, P p) {
        return Paths.get(String.format(diskCachePathFormat, p == null ? null : p.toString()));
    }
}
