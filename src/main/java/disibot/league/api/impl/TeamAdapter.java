package disibot.league.api.impl;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import disibot.league.api.entities.Team;

import java.io.IOException;

public class TeamAdapter extends TypeAdapter<Team> {
    @Override
    public void write(JsonWriter out, Team team) throws IOException {
        int id = team == Team.RED ? 200 : 100;
        out.value(id);
    }

    @Override
    public Team read(JsonReader in) throws IOException {
        return Team.byId(in.nextLong());
    }
}
