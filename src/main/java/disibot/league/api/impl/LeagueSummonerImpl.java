package disibot.league.api.impl;

import disibot.league.api.dataDragon.entities.Champion;
import disibot.league.api.dataDragon.entities.ProfileIcon;
import disibot.league.api.entities.ClashTeamMember;
import disibot.league.api.entities.CurrentGameInfo;
import disibot.league.api.entities.LeagueSummoner;
import disibot.league.api.entities.MatchHistory;
import disibot.league.api.entities.SummonerChampionInfo;
import disibot.league.api.entities.SummonerQueueInfo;
import disibot.league.api.exceptions.LeagueAPIException;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;

import static disibot.util.Util.ofEpochMillis;

@SuppressWarnings("unused")
public class LeagueSummonerImpl extends LeagueAPIEntity implements LeagueSummoner {
    private String name;
    private int profileIconId;
    private String puuid;
    private long summonerLevel;
    private long revisionDate;
    private String id;
    private String accountId;

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getAccountId() {
        return accountId;
    }

    public long getSummonerLevel() {
        return summonerLevel;
    }

    public MatchHistory getMatchlist(@Nullable List<Champion> championFilter) throws IOException, LeagueAPIException {
        return getAPI().getMatchlist(this, championFilter);
    }

    public int getTotalChampionMastery() throws IOException, LeagueAPIException {
        return getAPI().getTotalChampionMastery(this);
    }

    public List<SummonerChampionInfo> getAllChampionInfo() throws IOException, LeagueAPIException {
        return getAPI().getAllChampionInfo(this);
    }

    public List<SummonerQueueInfo> getQueueInfos() throws IOException, LeagueAPIException {
        return getAPI().getQueueInfos(this);
    }

    @Override
    public SummonerQueueInfo getTFTQueueInfo() throws IOException, LeagueAPIException {
        return getAPI().getTFTQueueInfo(this);
    } // todo fix all urls

    public ProfileIcon getProfileIcon() throws IOException, LeagueAPIException {
        return getDataDragon().getProfileIcons().get(profileIconId);
    }

    @Override
    @Nullable
    public CurrentGameInfo getCurrentGame() throws IOException, LeagueAPIException {
        return getAPI().getCurrentGame(this);
    }

    @Nonnull
    @Override
    public String getPuuId() {
        return puuid;
    }

    @Nonnull
    @Override
    public LocalDateTime getRevisionDate() {
        return ofEpochMillis(revisionDate);
    }

    @Override
    public List<ClashTeamMember> getClashTeams() throws IOException, LeagueAPIException {
        return getAPI().getClashTeamsBySummoner(this);
    }

    @Override
    public String toString() {
        return "S(" + name + ':' + id + ')';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LeagueSummonerImpl that = (LeagueSummonerImpl) o;
        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }
}
