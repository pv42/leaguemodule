package disibot.league.api.impl;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import disibot.league.api.dataDragon.entities.Champion;
import disibot.league.api.dataDragon.entities.LeagueChampionInfos;

import javax.annotation.Nonnull;
import java.io.IOException;

public class LeagueChampionInfoAdapter extends TypeAdapter<Champion> {
    private LeagueChampionInfos infos;

    public LeagueChampionInfoAdapter(@Nonnull LeagueChampionInfos infos) {
        this.infos = infos;
    }

    @Override
    public void write(JsonWriter out, Champion value) throws IOException {
        out.value(value.getKey());
    }

    @Override
    public Champion read(JsonReader in) throws IOException {
        long l = in.nextLong();
        return infos.getByKey(l);
    }
}
