package disibot.league.api.impl;

import disibot.league.api.dataDragon.entities.Champion;
import disibot.league.api.entities.Lane;
import disibot.league.api.entities.LeagueMatch;
import disibot.league.api.staticAPI.LeagueSeason;
import disibot.league.api.entities.MatchHistoryEntry;
import disibot.league.api.entities.Region;
import disibot.league.api.exceptions.LeagueAPIException;
import disibot.league.api.staticAPI.LeagueQueue;

import java.io.IOException;
import java.time.LocalDateTime;

import static disibot.util.Util.ofEpochMillis;

public class MatchHistoryEntryImpl extends LeagueAPIEntity implements MatchHistoryEntry {
    private Lane lane;
    private long gameId;
    private Champion champion;
    private Region platformId;
    private LeagueSeason season;
    private LeagueQueue queue;
    private String role;
    private long timestamp;

    @Override
    public Lane getLane() {
        return lane;
    }

    @Override
    public Region getRegion() {
        return platformId;
    }

    @Override
    public Champion getChampion() {
        return champion;
    }

    @Override
    public LeagueMatch getGame() throws IOException, LeagueAPIException {
        return getAPI().getMatch(gameId);
    }

    @Override
    public LeagueSeason getSeason() {
        return season;
    }

    @Override
    public LocalDateTime getTime() {
        return ofEpochMillis(timestamp);
    }

    @Override
    public String getRole() {
        return role;
    }

    @Override
    public String toString() {
        return "MatchHistoryEntry{" +
                "lane='" + lane + '\'' +
                ", gameId=" + gameId +
                ", championInfo=" + champion.getName() +
                ", region='" + platformId + '\'' +
                //", season='" + season + '\'' +
                //", queue='" + queue + '\'' +
                ", role='" + role + '\'' +
                ", timestamp=" + timestamp +
                '}';
    }
}
