package disibot.league.api.impl;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import disibot.league.api.entities.SummonerSpell;

import java.io.IOException;
import java.util.List;

public class SummonerSpellAdapter extends TypeAdapter<SummonerSpell> {
    private List<SummonerSpell> spells;

    public SummonerSpellAdapter(List<SummonerSpell> spells) {
        this.spells = spells;
    }

    @Override
    public void write(JsonWriter out, SummonerSpell value) throws IOException {
        out.value(value.getKey());
    }

    @Override
    public SummonerSpell read(JsonReader in) throws IOException {
        int id = in.nextInt();
        for (SummonerSpell spell: spells) {
            if(spell.getKey() == id) return spell;
        }
        throw new IllegalArgumentException("invalid summoner spell id");
    }
}
