package disibot.league.api.impl;

import disibot.league.api.dataDragon.DataDragonAPI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class DataDragonAPIEntity {
    private static transient Logger logger = LoggerFactory.getLogger(DataDragonAPIEntity.class);
    private transient DataDragonAPI dataDragon;
    public void setDataDragon(DataDragonAPI dataDragon) {
        if(this.dataDragon != null) logger.warn("cant change a set data dragon");
        this.dataDragon = dataDragon;
    }
    public DataDragonAPI getDataDragon() {
        return dataDragon;
    }
}
