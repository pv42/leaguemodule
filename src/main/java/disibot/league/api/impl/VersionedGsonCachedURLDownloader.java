package disibot.league.api.impl;

import com.google.gson.Gson;
import disibot.league.api.dataDragon.entities.Version;
import disibot.league.api.exceptions.LeagueAPIException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public abstract class VersionedGsonCachedURLDownloader<T> extends GsonCachedURLDownloader<T, Version, Void> {
    private static final Logger LOG = LoggerFactory.getLogger(VersionedGsonCachedURLDownloader.class);
    private final VersionProvider versionProvider;
    private final URLGenerator<Version> url;
    private Version currentVersion;

    public VersionedGsonCachedURLDownloader(URLGenerator<Version> url, VersionProvider versionProvider, @Nullable Path diskCachePath, Gson gson, Class<T> clazz) {
        super(diskCachePath,gson, clazz);
        this.versionProvider = versionProvider;
        this.url = url;
    }

    public VersionedGsonCachedURLDownloader(URLGenerator<Version> url, VersionProvider versionProvider, @Nullable String diskCachePath, Gson gson, Class<T> clazz) {
        super(diskCachePath != null ? Paths.get(diskCachePath) : null,gson, clazz);
        this.versionProvider = versionProvider;
        this.url = url;
    }

    public T get() throws IOException, LeagueAPIException {
        return get(null);
    }


    public T get(Void v) throws IOException, LeagueAPIException {
        Version newestVersion = versionProvider.get();
        ensureCacheIsUpToDate(newestVersion, null);
        return cachedObject.get(null);
    }

    protected void ensureCacheIsUpToDate(Version newestVersion, Void v) throws IOException, LeagueAPIException {
        if (isCacheExpired(newestVersion, null)) {
            updateCache(newestVersion, null);
            currentVersion = newestVersion;
        }
    }

    protected boolean isCacheExpired(Version newestVersion, Void v) {
        return cachedObject == null || !newestVersion.equals(currentVersion);
    }

    protected Path getDiskCachePath(Version version, Void p) {
        assert diskCachePath != null;
        return diskCachePath.resolveSibling(diskCachePath.getFileName() + version.toString());
    }


    protected boolean isDiskCacheValid(Version newestVersion, Void v) {
        Path path = getDiskCachePath(newestVersion, v);
        return Files.exists(path);
    }

    @Override
    protected URL getURL(Version version, Void p) throws IOException, LeagueAPIException {
        return url.getURL(version);
    }
}
