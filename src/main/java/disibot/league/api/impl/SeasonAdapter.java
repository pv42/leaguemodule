package disibot.league.api.impl;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import disibot.league.api.staticAPI.LeagueSeason;

import java.io.IOException;
import java.util.List;

public class SeasonAdapter extends TypeAdapter<LeagueSeason> {
    private List<? extends LeagueSeason> seasons;
    public SeasonAdapter(List<? extends LeagueSeason> seasons) {
        this.seasons = seasons;
    }

    @Override
    public void write(JsonWriter out, LeagueSeason value) throws IOException {
        out.value(value.getId());
    }

    @Override
    public LeagueSeason read(JsonReader in) throws IOException {
        int id = in.nextInt();
        for(LeagueSeason season: seasons) {
            if(season.getId() == id) return season;
        }
        throw new IllegalArgumentException("Invalid season id");
    }
}
