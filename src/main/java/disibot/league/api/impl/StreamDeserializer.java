package disibot.league.api.impl;

import java.io.InputStream;

public interface StreamDeserializer<T> {
    T deserialize(InputStream inputStream);
}
