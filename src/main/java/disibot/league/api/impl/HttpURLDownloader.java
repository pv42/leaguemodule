package disibot.league.api.impl;

import disibot.league.WeeklyTopListManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

public class HttpURLDownloader {
    private static final Logger LOG = LoggerFactory.getLogger(HttpURLDownloader.class);

    private final Map<String, String> headerParameter = new HashMap<>();

    public HttpURLConnection download(URL url) throws IOException {
        LOG.info("Creating connection to " + url);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();

        for (String key : headerParameter.keySet()) {
            connection.setRequestProperty(key, headerParameter.get(key));
        }
        return connection;
    }

    public void addHeaderParam(String key, String value) {
        headerParameter.put(key, value);
    }
}
