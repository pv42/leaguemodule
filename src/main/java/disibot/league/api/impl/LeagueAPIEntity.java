package disibot.league.api.impl;

public abstract class LeagueAPIEntity extends DataDragonAPIEntity {
    private transient RiotAPIImpl api;
    public void setAPI(RiotAPIImpl api) {
        //if(this.api != null) throw new UnsupportedOperationException("cant change a set api");
        this.api = api;
        setDataDragon(api.getDataDragonAPI());
    }

    public RiotAPIImpl getAPI() {
        return api;
    }
}
