package disibot.league.api.impl;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import disibot.league.api.RiotAPI;
import disibot.league.api.dataDragon.DataDragonAPI;
import disibot.league.api.dataDragon.LeagueQueueAdapter;
import disibot.league.api.dataDragon.entities.Champion;
import disibot.league.api.dataDragon.entities.LeagueItem;
import disibot.league.api.entities.ClashTeam;
import disibot.league.api.entities.ClashTeamMember;
import disibot.league.api.entities.CurrentGameInfo;
import disibot.league.api.entities.FreeChampionInfo;
import disibot.league.api.entities.LeagueMatch;
import disibot.league.api.entities.LeagueSummoner;
import disibot.league.api.entities.MatchHistory;
import disibot.league.api.entities.Region;
import disibot.league.api.entities.SummonerChampionInfo;
import disibot.league.api.entities.SummonerQueueInfo;
import disibot.league.api.entities.SummonerSpell;
import disibot.league.api.entities.Team;
import disibot.league.api.exceptions.BadRequestException;
import disibot.league.api.exceptions.InvalidAPIKeyException;
import disibot.league.api.exceptions.LeagueAPIException;
import disibot.league.api.exceptions.NoAPIKeyException;
import disibot.league.api.exceptions.NotFoundException;
import disibot.league.api.exceptions.RateLimitException;
import disibot.league.api.staticAPI.LeagueQueue;
import disibot.league.api.staticAPI.LeagueSeason;
import disibot.league.api.staticAPI.StaticAPI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.time.Duration;
import java.util.Date;
import java.util.List;

public class RiotAPIImpl extends WebAPI implements RiotAPI {
    private static final int RATE_LIMIT_EXCEEDED = 429;
    private static final Logger LOG = LoggerFactory.getLogger(RiotAPIImpl.class);
    private static final int MAX_RETRY = 4;

    private String apiKey;
    private DataDragonAPI dataDragonAPI;
    private StaticAPI staticAPI;
    private Gson gson;
    private boolean disabled = false; // disables api after 429 or 403
    private Region region;
    private HttpURLDownloader downloader;
    private ParameterizedGsonCachedURLDownloader<LeagueSummonerImpl, String> summonersById;
    private ParameterizedGsonCachedURLDownloader<LeagueSummonerImpl, String> summonersByName;
    private ParameterizedGsonCachedURLDownloader<LeagueSummonerImpl, String> summonersByAccount;

    public RiotAPIImpl(@Nonnull String apiKey, @Nonnull StaticAPI staticAPI, @Nonnull DataDragonAPI dataDragonAPI, @Nonnull Region region, @Nonnull HttpURLDownloader downloader) throws IOException, LeagueAPIException {
        this.apiKey = apiKey;
        this.staticAPI = staticAPI;
        this.dataDragonAPI = dataDragonAPI;
        this.region = region;
        this.downloader = downloader;
        initGson();
        initSummoners();
        downloader.addHeaderParam("X-Riot-Token", apiKey);
        setDownloader(this.downloader);
    }

    public void setDownloader(HttpURLDownloader downloader) {
        this.downloader = downloader;
        summonersById.setDownloader(downloader);
        summonersByAccount.setDownloader(downloader);
        summonersByName.setDownloader(downloader);
    }

    private void initGson() throws IOException, LeagueAPIException {
        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapter(Date.class, new LongDateTypeAdapter());
        builder.registerTypeAdapter(Team.class, new TeamAdapter());
        builder.registerTypeAdapter(LeagueSeason.class, new SeasonAdapter(staticAPI.getSeasons()));
        builder.registerTypeAdapter(LeagueQueue.class, new LeagueQueueAdapter(staticAPI.getQueues()));
        builder.registerTypeAdapter(Champion.class, new LeagueChampionInfoAdapter(dataDragonAPI.getAllChampionInfos()));
        builder.registerTypeAdapter(SummonerSpell.class, new SummonerSpellAdapter(dataDragonAPI.getSummonerSpells()));
        builder.registerTypeAdapter(LeagueItem.class, new LeagueItemInfoAdapter(dataDragonAPI.getItems()));
        gson = builder.create();
    }

    private void initSummoners() {
        summonersById = new ParameterizedGsonCachedURLDownloader<>(Duration.ofHours(1),
                summonerId -> getAPIUrl("/lol/summoner/v4/summoners/" + summonerId, null),
                "cache/lol/summoner/by-id/%s.json",
                gson, LeagueSummonerImpl.class);
        summonersByName = new ParameterizedGsonCachedURLDownloader<>(Duration.ofHours(1),
                summonerId -> getAPIUrl("/lol/summoner/v4/summoners/by-name/" + summonerId, null),
                "cache/lol/summoner/by-name/%s.json",
                gson, LeagueSummonerImpl.class);
        summonersByAccount = new ParameterizedGsonCachedURLDownloader<>(Duration.ofHours(1),
                summonerId -> getAPIUrl("/lol/summoner/v4/summoners/by-account/" + summonerId, null),
                "cache/lol/summoner/by-account/%s.json",
                gson, LeagueSummonerImpl.class);
    }

    public LeagueSummoner getSummonerByName(String name) throws IOException, LeagueAPIException {
        LeagueSummonerImpl summoner = summonersByName.get(name);
        summoner.setAPI(this);
        return summoner;
    }

    public LeagueSummoner getSummonerByAccountId(String accountId) throws IOException, LeagueAPIException {
        LeagueSummonerImpl summoner = summonersByAccount.get(accountId);
        summoner.setAPI(this);
        return summoner;
    }

    public LeagueSummoner getSummonerById(String summonerId) throws IOException, LeagueAPIException {
        LeagueSummonerImpl summoner = summonersById.get(summonerId);
        summoner.setAPI(this);
        return summoner;
    }

    public FreeChampionInfo getFreeChampions() throws IOException, LeagueAPIException {
        return getFromApi("/lol/platform/v3/champion-rotations", FreeChampionInfo.class);
    }

    @Override
    public boolean isDisabled() {
        return disabled;
    }

    @Override
    public LeagueMatch getMatchById(long id) throws IOException, LeagueAPIException {
        return getFromApi("/lol/match/v4/matches/" + id, LeagueMatch.class);
    }

    protected List<SummonerQueueInfo> getQueueInfos(@Nonnull LeagueSummonerImpl summoner) throws IOException, LeagueAPIException {
        Type leagueQueueInfosListType = new TypeToken<List<SummonerQueueInfo>>() {}.getType();
        List<SummonerQueueInfo> infos = getFromApi("/lol/league/v4/entries/by-summoner/" + summoner.getId(), leagueQueueInfosListType);
        for (SummonerQueueInfo info : infos) {
            info.setAPI(this);
        }
        return infos;
    }

    protected List<SummonerChampionInfo> getAllChampionInfo(@Nonnull LeagueSummonerImpl summoner) throws IOException, LeagueAPIException {
        Type summonerChampionInfoListType = new TypeToken<List<SummonerChampionInfo>>() {}.getType();
        List<SummonerChampionInfo> infos = getFromApi("/lol/champion-mastery/v4/champion-masteries/by-summoner/" + summoner.getId(), summonerChampionInfoListType);
        for (SummonerChampionInfo info : infos) {
            info.setAPI(this);
        }
        return infos;
    }

    @Nullable
    protected CurrentGameInfo getCurrentGame(@Nonnull LeagueSummoner summoner) throws IOException, LeagueAPIException {
        try {
            CurrentGameInfo game = getFromApi("/lol/spectator/v4/active-games/by-summoner/" + summoner.getId(), CurrentGameInfo.class);
            game.setAPI(this);
            return game;
        } catch (NotFoundException e) {
            return null;
        }
    }

    protected int getTotalChampionMastery(@Nonnull LeagueSummonerImpl summoner) throws IOException, LeagueAPIException {
        HttpURLConnection connection = connectToRiotAPI("/lol/champion-mastery/v4/scores/by-summoner/" + summoner.getId());
        StringBuilder buffer = new StringBuilder();
        int c;
        InputStream stream = connection.getInputStream();
        while ((c = stream.read()) != -1) {
            buffer.append((char) c);
            if (buffer.length() == 1000) System.out.println(buffer);
        }
        return Integer.parseInt(buffer.toString());
    }


    protected MatchHistory getMatchlist(@Nonnull LeagueSummoner summoner, @Nullable List<Champion> championFilter) throws IOException, LeagueAPIException {
        StringBuilder paramBuilder = new StringBuilder();
        if (championFilter != null) {
            for (Champion champion : championFilter) {
                paramBuilder.append("champion=").append(champion.getKey()).append('&');
            }
        }
        String param = paramBuilder.length() > 0 ? paramBuilder.substring(0, paramBuilder.length() - 1) : null; //remove last &
        MatchHistory history = getFromApi("/lol/match/v4/matchlists/by-account/" + summoner.getAccountId(), param,
                MatchHistory.class);
        for (MatchHistoryEntryImpl entry : history.getMatches()) {
            entry.setAPI(this);
        }
        return history;
    }

    protected List<ClashTeamMember> getClashTeamsBySummoner(@Nonnull LeagueSummoner summoner) throws IOException, LeagueAPIException {
        Type type = new TypeToken<List<ClashTeamMember>>() {}.getType();
        List<ClashTeamMember> members = getFromApi("/lol/clash/v1/players/by-summoner/" + summoner.getId(), type);
        members.forEach(clashTeamMember -> clashTeamMember.setAPI(this));
        return members;
    }


    public ClashTeam getClashTeamById(int teamId) throws IOException, LeagueAPIException {
        ClashTeam team = getFromApi("/lol/clash/v1/teams/" + teamId, ClashTeam.class);
        team.setAPI(this);
        team.getPlayers().forEach(clashTeamMember -> clashTeamMember.setAPI(this));
        return team;
    }

    protected LeagueMatch getMatch(long matchId) throws IOException, LeagueAPIException {
        return getFromApi("/lol/match/v4/matches/" + matchId, LeagueMatch.class);
    }

    @Nullable
    protected SummonerQueueInfo getTFTQueueInfo(@Nonnull LeagueSummonerImpl summoner) throws IOException, LeagueAPIException {
        Type leagueQueueInfosListType = new TypeToken<List<SummonerQueueInfo>>() {}.getType();
        List<SummonerQueueInfo> infos = getFromApi("/tft/league/v1/entries/by-summoner/" + summoner.getId(), leagueQueueInfosListType);
        if (infos.size() == 0) return null;
        return infos.get(0);
    }


    private <T> T getFromApi(String urlPart, Class<T> clazz) throws IOException, LeagueAPIException {
        return gson.fromJson(new InputStreamReader(connectToRiotAPI(urlPart).getInputStream()), clazz);
    }

    private <T> T getFromApi(String urlPart, String arguments, Class<T> clazz) throws IOException, LeagueAPIException {
        return gson.fromJson(new InputStreamReader(connectToRiotAPI(urlPart, arguments).getInputStream()), clazz);
    }

    private <T> T getFromApi(String urlPart, Type type) throws IOException, LeagueAPIException {
        return gson.fromJson(new InputStreamReader(connectToRiotAPI(urlPart).getInputStream()), type);
    }

    private URL getAPIUrl(String urlPathString, String arguments) throws IOException {
        URL url;
        try {
            url = new URI("https", region.toString().toLowerCase() + ".api.riotgames.com", urlPathString, arguments, null).toURL();
        } catch (URISyntaxException e) {
            LOG.error("Invalid uri syntax:" + e.getMessage());
            throw new IOException("Could not create URI");
        }
        return url;
    }

    @Nonnull
    private HttpURLConnection connectToRiotAPI(String urlPathString) throws IOException, LeagueAPIException {
        if (disabled) throw new IOException("Api is disabled.");
        for (int i = 0; i < MAX_RETRY; i++) {
            HttpURLConnection connection = downloader.download(getAPIUrl(urlPathString, null));
            connection.setRequestProperty("X-Riot-Token", apiKey);
            try {
                connect(connection);
                checkResponse(connection);
                return connection;
            } catch (RateLimitException e) {
                if (i == MAX_RETRY - 1) throw e;
                try {
                    Thread.sleep(e.getRetryAfter() * 1000L + 200);
                } catch (InterruptedException ex) {
                    ex.printStackTrace();
                }
            }
        }
        throw new IllegalStateException("This code should not be reached: max retry without error");
    }

    @Nonnull
    private HttpURLConnection connectToRiotAPI(String urlPathString, String arguments) throws IOException, LeagueAPIException {
        if (disabled) throw new IOException("Api is disabled.");
        for (int i = 0; i < MAX_RETRY; i++) {
            HttpURLConnection connection = downloader.download(getAPIUrl(urlPathString, arguments));
            connection.setRequestProperty("X-Riot-Token", apiKey);
            try {
                connect(connection);
                checkResponse(connection);
                return connection;
            } catch (RateLimitException e) {
                if (i == MAX_RETRY - 1) throw e;
                try {
                    Thread.sleep(e.getRetryAfter() * 1000L + 200);
                } catch (InterruptedException ex) {
                    ex.printStackTrace();
                }
            }
        }
        throw new IllegalStateException("This code should not be reached: max retry without error");
    }

    private void connect(@Nonnull HttpURLConnection connection) throws IOException {
        connection.setRequestMethod("GET");
        connection.setConnectTimeout(5000);
        connection.setReadTimeout(5000);
        connection.setInstanceFollowRedirects(false);
        LOG.info("Connecting to League API: " + connection.getURL());
        connection.connect();
    }

    private void checkResponse(@Nonnull HttpURLConnection connection) throws LeagueAPIException, IOException {
        int response = connection.getResponseCode();
        if (response == HttpURLConnection.HTTP_OK) return;
        if (response == HttpURLConnection.HTTP_BAD_REQUEST) throw new BadRequestException();
        if (response == HttpURLConnection.HTTP_UNAUTHORIZED) {
            StringBuilder s = new StringBuilder(45);
            int c;
            while (-1 != (c = connection.getErrorStream().read())) {
                s.append(c);
            }
            System.out.println("Unauthorized Error:\n" + s);
            throw new NoAPIKeyException();
        }
        if (response == HttpURLConnection.HTTP_FORBIDDEN) {
            LOG.warn("API key is invalid, disabling");
            disabled = true;
            throw new InvalidAPIKeyException();
        }
        if (response == HttpURLConnection.HTTP_NOT_FOUND) throw new NotFoundException();
        if (response == RATE_LIMIT_EXCEEDED) {
            String retry_after = connection.getHeaderField("Retry-After");
            if (retry_after == null) {
                disabled = true;
                throw new IllegalArgumentException("The server returned 429 but no Retry-After header is set");
            }
            LOG.warn("Api rate limit exceeded, retry-after: " + retry_after);
            throw new RateLimitException(Integer.parseInt(retry_after));
        } else {
            throw new IllegalArgumentException("Invalid http response code");
        }
    }

    public DataDragonAPI getDataDragonAPI() {
        return dataDragonAPI;
    }
}
