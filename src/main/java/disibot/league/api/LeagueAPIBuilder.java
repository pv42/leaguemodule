package disibot.league.api;

import disibot.league.api.dataDragon.DataDragonAPI;
import disibot.league.api.dataDragon.Language;
import disibot.league.api.entities.Region;
import disibot.league.api.exceptions.LeagueAPIException;
import disibot.league.api.impl.HttpURLDownloader;
import disibot.league.api.impl.RiotAPIImpl;
import disibot.league.api.staticAPI.StaticAPI;

import javax.annotation.Nonnull;
import java.io.IOException;

/**
 * A league api builder is an object capable of setting up and creating a league api
 */
public class LeagueAPIBuilder {
    private boolean useStaticDiskCache = true;
    private boolean useDataDragonDiskCache = true;
    private boolean createStaticCacheOnBuild = true;
    private boolean createDataDragonCacheOnBuild = true;
    private String dataDragonVersionRegion = DataDragonAPI.DEFAULT_REGION;
    private String apiKey;
    private Region region = Region.EUW1;
    private Language language = Language.en_US;

    /**
     * creates a league api builder, capable to create a league api,
     * note that a api key is required to create a league api
     */
    public LeagueAPIBuilder() {
    }

    /**
     * creates a league api builder, capable to create a league api using the provided api key
     *
     * @param apiKey apikey to use
     */
    public LeagueAPIBuilder(@Nonnull String apiKey) {
        this();
        setApiKey(apiKey);
    }

    /**
     * Configures which internal league apis should use disk cache.
     * Disk cache allows the api to store data received from riots web api to be stored on the disk and loaded again if
     * the same request is made again. Per default this is enabled for all apis
     *
     * @param caches indicates which apis should use disk cache
     */
    public void setUseDiskCacheMode(RIOT_APIS caches) {
        useDataDragonDiskCache = caches == RIOT_APIS.ALL || caches == RIOT_APIS.ONLY_DATA_DRAGON;
        useStaticDiskCache = caches == RIOT_APIS.ALL || caches == RIOT_APIS.ONLY_STATIC;
    }

    /**
     * Configures which internal league apis should download commonly required data (e.g. information about Champions)
     * on initialisation. If disabled the data will be request on first use, increasing the time the function call
     * takes. If enabled the initialization will
     *
     * @param caches indicates which apis caches should be initialized
     */
    public void setCreateCacheOnBuild(RIOT_APIS caches) {
        createStaticCacheOnBuild = caches == RIOT_APIS.ALL || caches == RIOT_APIS.ONLY_DATA_DRAGON;
        createDataDragonCacheOnBuild = caches == RIOT_APIS.ALL || caches == RIOT_APIS.ONLY_STATIC;
    }

    /**
     * sets the region the data dragon should use to retrieve version information about it's sub apis, "na" is the
     * default value, note that other regions in most cases use the same data dragon api versions
     *
     * @param dataDragonVersionRegion region the data dragon api should use for version info, must be a string like
     *                                "euw", "eune", "br" or "na", ...
     */
    public void setDataDragonVersionRegion(@Nonnull String dataDragonVersionRegion) {
        this.dataDragonVersionRegion = dataDragonVersionRegion;
    }

    /**
     * sets the riot api key to use
     *
     * @param apiKey apikey to use
     */
    public void setApiKey(@Nonnull String apiKey) {
        this.apiKey = apiKey;
    }

    /**
     * sets the region, which should be used for dynamic api calls, default is EUW1
     *
     * @param region region to use
     */
    public void setRegion(Region region) {
        this.region = region;
    }

    /**
     * sets teh language, in which to responses should be, default is en_US
     *
     * @param language language to use
     */
    public void setLanguage(Language language) {
        this.language = language;
    }

    /**
     * creates a league api using the configuration of the builder
     *
     * @return the created lol api
     * @throws IOException           if there was an error reading from a web api or a file on disk
     * @throws LeagueAPIException    if an api endpoint answered with a none 200(OK) response code
     * @throws IllegalStateException if the api key or the data dragon api region are null
     */
    public RiotAPI build() throws IOException, LeagueAPIException {
        if (apiKey == null)
            throw new IllegalStateException("Can't create api without a valid api key");
        if (dataDragonVersionRegion == null)
            throw new IllegalStateException("Can't create api without a valid data dragon version region");
        return new RiotAPIImpl(apiKey,
                new StaticAPI(createStaticCacheOnBuild, useStaticDiskCache),
                new DataDragonAPI(dataDragonVersionRegion, language, createDataDragonCacheOnBuild, useDataDragonDiskCache),
                region,
                new HttpURLDownloader());
    }

    public enum RIOT_APIS {
        NONE,
        ONLY_STATIC,
        ONLY_DATA_DRAGON,
        ALL
    }
}
