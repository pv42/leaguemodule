package disibot.league.api.entities;

public enum Position {
    UNSELECTED, FILL, TOP, JUNGLE, MIDDLE, BOTTOM, UTILITY
}
