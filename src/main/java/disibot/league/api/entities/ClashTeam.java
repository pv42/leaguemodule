package disibot.league.api.entities;

import disibot.league.api.exceptions.LeagueAPIException;
import disibot.league.api.impl.LeagueAPIEntity;

import java.io.IOException;
import java.util.List;

public class ClashTeam extends LeagueAPIEntity {
    private int id;
    private int tournamentId;
    private String name;
    private int iconId;
    private int tier;
    private String captain;
    private String abbreviation;
    private List<ClashTeamMember> players;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getTier() {
        return tier;
    }

    public LeagueSummoner getCaptain() throws IOException, LeagueAPIException {
        return getAPI().getSummonerById(captain);
    }

    public String getAbbreviation() {
        return abbreviation;
    }

    public List<ClashTeamMember> getPlayers() {
        return players;
    }
}
