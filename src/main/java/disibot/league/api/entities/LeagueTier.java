package disibot.league.api.entities;

public enum LeagueTier {
    IRON("Iron", 0),
    BRONZE("Bronze", 1),
    SILVER("Silver", 2),
    GOLD("Gold", 3),
    PLATINUM("Platinum", 4),
    DIAMOND("Diamond", 5),
    MASTER("Master", 6),
    GRANDMASTER("Grandmaster", 7),
    CHALLENGER("Challenger", 8),
    UNRANKED("Unranked", -1);

    private final String displayName;
    private final int index;

    LeagueTier(String displayName, int index) {
        this.displayName = displayName;
        this.index = index;
    }

    public String getDisplayName() {
        return displayName;
    }

    public int getIndex() {
        return index;
    }
}
