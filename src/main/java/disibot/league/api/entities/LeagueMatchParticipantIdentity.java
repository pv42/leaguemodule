package disibot.league.api.entities;

public class LeagueMatchParticipantIdentity {
    private int participantId;
    private LeaguePlayer player;

    public LeaguePlayer getPlayer() {
        return player;
    }

    @Override
    public String toString() {
        return player.getSummonerName();
    }
}
