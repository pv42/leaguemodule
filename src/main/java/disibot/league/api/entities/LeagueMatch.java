package disibot.league.api.entities;

import disibot.league.api.staticAPI.LeagueQueue;
import disibot.league.api.staticAPI.LeagueSeason;

import java.util.List;

public class LeagueMatch {
    private LeagueSeason seasonId;
    private LeagueQueue queueId;
    private long gameId;
    private List<LeagueMatchParticipantIdentity> participantIdentities;
    private String gameVersion;
    private Region platformId;
    private String gameMode;
    private String mapId;
    private String gameType;
    private List<LeagueTeamStats> teams;
    private List<LeagueMatchParticipant> participants;

    public LeagueQueue getQueue() {
        return queueId;
    }

    public Region getRegion() {
        return platformId;
    }

    public List<LeagueMatchParticipant> getParticipants() {
        return participants;
    }

    @Override
    public String toString() {
        return "LeagueMatch{" +
                "season='" + seasonId + '\'' +
                ", queueId=" + queueId +
                ", gameId=" + gameId +
                ", participantIdentities=" + participantIdentities +
                ", gameVersion='" + gameVersion + '\'' +
                ", platformId='" + platformId + '\'' +
                ", gameMode='" + gameMode + '\'' +
                ", mapId='" + mapId + '\'' +
                ", gameType='" + gameType + '\'' +
                ", teams=" + teams +
                ", participants=" + participants +
                '}';
    }

    public long getId() {
        return gameId;
    }
}
