package disibot.league.api.entities;

import disibot.league.api.impl.MatchHistoryEntryImpl;

import java.util.List;

public class MatchHistory {
    private List<MatchHistoryEntryImpl> matches;
    private int totalGames;
    private int startIndex;
    private int endIndex;

    public List<MatchHistoryEntryImpl> getMatches() {
        return matches;
    }

    public int getTotalGames() {
        return totalGames;
    }
}
