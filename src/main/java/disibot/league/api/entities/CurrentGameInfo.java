package disibot.league.api.entities;

import disibot.league.api.impl.LeagueAPIEntity;

import java.time.LocalDateTime;
import java.util.List;

import static disibot.util.Util.ofEpochMillis;
import static java.time.ZoneOffset.UTC;

@SuppressWarnings("unused") // since it is initialized by deserialization
public class CurrentGameInfo extends LeagueAPIEntity {
    private long gameId;
    private long gameStartTime; // epoch ms
    private Region platformId;
    private String gameMode; // e.g. CLASSIC // todo
    private int mapId;
    private String gameType; // e.g. MATCHED_GAME // todo
    private long gameQueueConfigId;
    private LeagueObserver observers;
    private List<CurrentGameParticipant> participants;
    private List<LeagueBan> bannedChampions;
    private long gameLength;

    public long getId() {
        return gameId;
    }

    public List<CurrentGameParticipant> getParticipants() {
        if(participants.get(0).getAPI() == null) {
            for (CurrentGameParticipant participant: participants) {
                participant.setAPI(getAPI());
            }
        }
        return participants;
    }

    public LocalDateTime getStartTime() {
        return ofEpochMillis(gameStartTime);
    }

    public Region getRegion() {
        return platformId;
    }

    public String getGameMode() {
        return gameMode;
    }

    // todo map, gametype, qcfg, obs


    public String getGameType() {
        return gameType;
    }

    private static class LeagueObserver {
        private String encryptionKey;
    }
}
