package disibot.league.api.entities;

import disibot.league.api.dataDragon.entities.Champion;
import disibot.league.api.exceptions.LeagueAPIException;
import disibot.league.api.staticAPI.LeagueSeason;

import java.io.IOException;
import java.time.LocalDateTime;

public interface MatchHistoryEntry {
    Lane getLane();
    Region getRegion();
    Champion getChampion();
    LeagueMatch getGame() throws IOException, LeagueAPIException;
    LeagueSeason getSeason();
    LocalDateTime getTime();

    String getRole();
}
