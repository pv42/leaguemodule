package disibot.league.api.entities;

import disibot.league.api.dataDragon.entities.Champion;

import java.util.List;

public class FreeChampionInfo {
    private List<Champion> freeChampionIds;
    private List<Champion> freeChampionIdsForNewPlayers;
    private int maxNewPlayerLevel;

    public List<Champion> getFreeChampions() {
        return freeChampionIds;
    }

    public List<Champion> getFreeChampionsForNewPlayers() {
        return freeChampionIdsForNewPlayers;
    }

    public int getMaxNewPlayerLevel() {
        return maxNewPlayerLevel;
    }
}
