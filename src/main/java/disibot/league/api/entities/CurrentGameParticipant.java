package disibot.league.api.entities;

import disibot.league.api.dataDragon.entities.Champion;
import disibot.league.api.dataDragon.entities.ProfileIcon;
import disibot.league.api.exceptions.LeagueAPIException;
import disibot.league.api.impl.LeagueAPIEntity;

import java.io.IOException;
@SuppressWarnings("unused")
public class CurrentGameParticipant extends LeagueAPIEntity {
    private long profileIconId;
    private long championId;
    private String summonerName;
    //private List gameCustomizationObjects
    private boolean bot;
    //private Perks perks;
    private SummonerSpell spell1Id;
    private SummonerSpell spell2Id;
    private Team teamId;
    private String summonerId;

    public Champion getChampion() throws IOException, LeagueAPIException {
        return getAPI().getDataDragonAPI().getChampionById(championId);
    }

    public String getSummonerName() {
        return summonerName;
    }

    public boolean isBot() {
        return bot;
    }

    public LeagueSummoner getSummoner() throws IOException, LeagueAPIException {
        return getAPI().getSummonerById(summonerId);
    }

    public ProfileIcon getProfileIcon() throws IOException, LeagueAPIException {
        return getDataDragon().getProfileIcons().get((int)profileIconId);
    }

    public SummonerSpell getSummonerSpell1() {
        return spell1Id;
    }

    public SummonerSpell getSummonerSpell2() {
        return spell2Id;
    }

    public Team getTeam() {
        return teamId;
    }



    // todo perkz, gco,
}
