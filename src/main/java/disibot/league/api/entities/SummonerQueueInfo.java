package disibot.league.api.entities;

import disibot.league.api.exceptions.LeagueAPIException;
import disibot.league.api.impl.LeagueAPIEntity;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.IOException;

import static disibot.league.api.entities.LeagueTier.BRONZE;
import static disibot.league.api.entities.LeagueTier.CHALLENGER;
import static disibot.league.api.entities.LeagueTier.DIAMOND;
import static disibot.league.api.entities.LeagueTier.GOLD;
import static disibot.league.api.entities.LeagueTier.GRANDMASTER;
import static disibot.league.api.entities.LeagueTier.IRON;
import static disibot.league.api.entities.LeagueTier.MASTER;
import static disibot.league.api.entities.LeagueTier.PLATINUM;
import static disibot.league.api.entities.LeagueTier.SILVER;
import static disibot.league.api.entities.LeagueTier.UNRANKED;

@SuppressWarnings("unused")
public class SummonerQueueInfo extends LeagueAPIEntity implements Comparable<SummonerQueueInfo> {
    private String queueType;
    private String summonerName;
    private boolean hotStreak;
    private int wins;
    private int losses;
    private boolean veteran;
    private String rank;
    private String leagueId;
    private boolean inactive;
    private boolean freshBlood;
    private String tier;
    private String summonerId;
    private int leaguePoints;

    @Nonnull
    public static SummonerQueueInfo createEmptyQueueInfo(LeagueSummoner summoner) {
        SummonerQueueInfo info = new SummonerQueueInfo();
        info.summonerName = summoner.getName();
        info.tier = "UNRANKED";
        info.rank = "I";
        info.summonerId = summoner.getId();
        return info;
    }

    public int getDivisionInt() {
        switch (rank) {
            case "I":
                return 1;
            case "II":
                return 2;
            case "III":
                return 3;
            case "IV":
                return 4;
            case "V":
                return 5;
            default:
                throw new IllegalStateException("rank is not valid");
        }
    }

    public String getQueueType() {
        return queueType;
    }

    public String getSummonerName() {
        return summonerName;
    }

    public LeagueSummoner getSummoner() throws IOException, LeagueAPIException {
        return getAPI().getSummonerById(summonerId);
    }

    public String getDivision() {
        return rank;
    }

    public String getTierAndDivision() {
        if (getTier() == MASTER || getTier() == GRANDMASTER || getTier() == CHALLENGER || getTier() == UNRANKED)
            return getTier().getDisplayName();
        return getTier().getDisplayName() + " " + rank;
    }

    public LeagueTier getTier() {
        switch (tier) {
            case "IRON":
                return IRON;
            case "BRONZE":
                return BRONZE;
            case "SILVER":
                return SILVER;
            case "GOLD":
                return GOLD;
            case "PLATINUM":
                return PLATINUM;
            case "DIAMOND":
                return DIAMOND;
            case "MASTER":
                return MASTER;
            case "GRANDMASTER":
                return GRANDMASTER;
            case "CHALLENGER":
                return CHALLENGER;
            case "UNRANKED":
                return UNRANKED;
            default:
                throw new IllegalStateException("tier is not valid");
        }
    }

    public int getLeaguePoints() {
        return leaguePoints;
    }

    public int getWins() {
        return wins;
    }

    public int getLosses() {
        return losses;
    }

    @Override
    public int compareTo(@Nonnull SummonerQueueInfo other) {
        return compare(this, other);
    }

    public static int compare(@Nonnull SummonerQueueInfo first, @Nonnull SummonerQueueInfo second) {
        if (first.getTier().getIndex() > second.getTier().getIndex()) return 1;
        if (first.getTier().getIndex() < second.getTier().getIndex()) return -1;
        if (first.getDivisionInt() < second.getDivisionInt()) return 1;
        if (first.getDivisionInt() > second.getDivisionInt()) return -1;
        return Integer.compare(first.leaguePoints, second.leaguePoints);
    }

    public boolean onHotStreak() {
        return hotStreak;
    }

    public int getGameNumber() {
        return wins + losses;
    }
}
