package disibot.league.api.entities;

import disibot.league.api.dataDragon.entities.LeagueImageInfo;

import java.util.List;

public class SummonerSpell {
    private String id;
    private String name;
    private String description;
    private String tooltip;
    private int maxrank;
    private int[] cooldown;
    private String cooldownBurn; //?
    private int[] cost;
    // data values
    // effect
    // effectBurn
    // vars
    private long key;
    private List<String> modes;
    private String costType;
    private int maxammo;
    private int[] range;
    private String rangeBurn; // ?
    private LeagueImageInfo image;
    private String resource;

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public long getKey() {
        return key;
    }
}
