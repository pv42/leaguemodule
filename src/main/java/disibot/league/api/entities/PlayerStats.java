package disibot.league.api.entities;

import disibot.league.api.dataDragon.entities.LeagueItem;

import java.lang.reflect.Field;

public class PlayerStats {
    // meta //
    int participantId;
    // kda //
    int kills;
    int deaths;
    int assists;
    // multikills //
    int largestMultiKill;
    int doubleKills;
    int tripleKills;
    int quadraKills;
    int pentaKills;
    int unrealKills; // maybe multikills > 5 ?
    int killingSprees;
    int largestKillingSpree;
    // damage //
    long totalDamageDealt;
    long totalDamageDealtToChampions;
    long damageDealtToObjectives;
    long damageDealtToTurrets;
    long magicDamageDealt;
    long magicDamageDealtToChampions;
    long physicalDamageDealt;
    long physicalDamageDealtToChampions;
    long trueDamageDealt;
    long trueDamageDealtToChampions;
    long totalDamageTaken;
    long physicalDamageTaken;
    long magicalDamageTaken;
    long trueDamageTaken;
    long damageSelfMitigated;
    long totalHeal;
    // first* //
    boolean firstBloodKill;
    boolean firstBloodAssist;
    boolean firstInhibitorKill;
    boolean firstInhibitorAssist;
    boolean firstTowerKill;
    boolean firstTowerAssist;
    // vision //
    long visionScore;
    int wardsKilled;
    int visionWardsBoughtInGame;
    int wardsPlaced;
    long sightWardsBoughtInGame;
    // cc //
    int totalTimeCrowdControlDealt;
    long timeCCingOthers;
    // farm //
    int totalMinionsKilled;
    int neutralMinionsKilled;            // jungle cs
    int neutralMinionsKilledTeamJungle;
    int neutralMinionsKilledEnemyJungle;
    // objectives //
    int teamObjective;
    int turretKills;
    int inhibitorKills;
    // other game mode specific //
    int nodeNeutralize;
    int nodeNeutralizeAssist;
    int nodeCapture;
    int nodeCaptureAssist;
    int altarsNeutralized;
    int altarsCaptured;
    // player score //
    int totalScoreRank;
    int totalPlayerScore;
    int objectivePlayerScore;
    int combatPlayerScore;
    // gold //
    int goldSpent;
    int goldEarned;
    // other //
    int longestTimeSpentLiving;
    int totalUnitsHealed;
    int largestCriticalStrike;
    boolean win;
    int champLevel;
    // items //
    LeagueItem item0;
    LeagueItem item1;
    LeagueItem item2;
    LeagueItem item3;
    LeagueItem item4;
    LeagueItem item5;
    LeagueItem item6;
    // ?? playerScore //
    int playerScore0;
    int playerScore1;
    int playerScore2;
    int playerScore3;
    int playerScore4;
    int playerScore5;
    int playerScore6;
    int playerScore7;
    int playerScore8;
    int playerScore9;
    // runes //
    int perk0;              // 	Primary path keystone rune.
    int perk1;              // 	Primary path rune.
    int perk2;              //  Primary path rune.
    int perk3;              // 	Primary path rune.
    int perk4;              // 	Secondary path rune.
    int perk5;              //  Secondary path rune.
    int perkPrimaryStyle;   // 	Primary rune path
    int perkSubStyle;       //  Secondary rune path
    // rune stats //
    int perk0Var1;          //  Post game rune stats.
    int perk0Var2;          //  Post game rune stats.
    int perk0Var3;          //  Post game rune stats.
    int perk1Var1;          //  Post game rune stats.
    int perk1Var2;          //  Post game rune stats.
    int perk1Var3;          //  Post game rune stats.
    int perk2Var1;          //  Post game rune stats.
    int perk2Var2;          //	Post game rune stats.
    int perk2Var3;          //	Post game rune stats.
    int perk3Var1;          //  Post game rune stats.
    int perk3Var2;          //  Post game rune stats.
    int perk3Var3;          //  Post game rune stats.
    int perk4Var1;          //	Post game rune stats.
    int perk4Var2;          //	Post game rune stats.
    int perk4Var3;          //	Post game rune stats.
    int perk5Var1;          //	Post game rune stats.
    int perk5Var2;          //	Post game rune stats.
    int perk5Var3;          //	Post game rune stats.

    public LeagueItem[] getItems() {
        return new LeagueItem[]{item0, item1, item2, item3, item4, item5, item6};
    }

    @Override
    public String toString() {
        String str = "Stats{";
        for(Field field: this.getClass().getDeclaredFields()) {
            try {
                Object value = field.get(this);
                str += field.getName() + "=" + value + ", ";
            } catch (IllegalAccessException e) {
                e.printStackTrace();
                System.exit(-1);
            }
        }
        str = str.substring(0, str.length() - 1);
        str += "}";
        return str;
    }
}
