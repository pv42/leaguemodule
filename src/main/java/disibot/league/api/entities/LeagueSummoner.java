package disibot.league.api.entities;

import disibot.league.api.dataDragon.entities.Champion;
import disibot.league.api.dataDragon.entities.ProfileIcon;
import disibot.league.api.exceptions.LeagueAPIException;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;

public interface LeagueSummoner {
    String getId();

    String getName();

    String getAccountId();

    long getSummonerLevel();

    MatchHistory getMatchlist(@Nullable List<Champion> championFilter) throws IOException, LeagueAPIException;

    int getTotalChampionMastery() throws IOException, LeagueAPIException;

    List<SummonerChampionInfo> getAllChampionInfo() throws IOException, LeagueAPIException;

    List<SummonerQueueInfo> getQueueInfos() throws IOException, LeagueAPIException;

    @Nullable
    SummonerQueueInfo getTFTQueueInfo() throws IOException, LeagueAPIException;

    @Nullable
    default SummonerQueueInfo getSoloQueueInfo() throws IOException, LeagueAPIException {
        for (SummonerQueueInfo queueInfo : this.getQueueInfos()) {
            if (queueInfo.getQueueType().equals("RANKED_SOLO_5x5")) {
                return queueInfo;
            }
        }
        return null;
    }

    ProfileIcon getProfileIcon() throws IOException, LeagueAPIException;

    @Nullable
    CurrentGameInfo getCurrentGame() throws IOException, LeagueAPIException;

    @Nonnull
    String getPuuId();

    @Nonnull
    LocalDateTime getRevisionDate();

    List<ClashTeamMember> getClashTeams() throws IOException, LeagueAPIException;
}
