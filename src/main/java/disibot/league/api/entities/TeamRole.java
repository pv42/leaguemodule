package disibot.league.api.entities;

public enum TeamRole {
    CAPTAIN, MEMBER
}
