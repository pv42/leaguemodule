package disibot.league.api.entities;

public enum Team {
    RED,
    BLUE,
    INVALID;

    public static Team byId(long id) {
        switch ((int)id) {
            case 100: return BLUE;
            case 200: return RED;
            default: return INVALID;
        }
    }
}
