package disibot.league.api.entities;

import java.util.List;

public class LeagueTeamStats {
    private List<LeagueBan> bans;
    private boolean firstDragon;
    private boolean firstHerald;
    private boolean firstBaron;
    private boolean firstInhibitor;
    private boolean firstTower;
    private int dragonKills;
    private int riftHeraldKills;
    private int baronKills;
    private int inhibitorKills;
    private int towerKills;
    private boolean firstBlood;
    private Team teamId; //200=red, 100=blue
    private String win; // Fail|Win
}
