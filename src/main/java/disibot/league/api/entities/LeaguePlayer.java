package disibot.league.api.entities;

public class LeaguePlayer {
    private String currentPlatformId;
    private String summonerName;
    private String matchHistoryUri;
    private String platformId;
    private String currentAccountId;
    private int profileIcon;
    private String summonerId;
    private String accountId;

    @Override
    public String toString() {
        return "LeaguePlayer{" +
                "currentPlatformId='" + currentPlatformId + '\'' +
                ", summonerName='" + summonerName + '\'' +
                ", matchHistoryUri='" + matchHistoryUri + '\'' +
                ", platformId='" + platformId + '\'' +
                ", currentAccountId='" + currentAccountId + '\'' +
                ", profileIcon=" + profileIcon +
                ", summonerId='" + summonerId + '\'' +
                ", accountId='" + accountId + '\'' +
                '}';
    }

    public String getSummonerName() {
        return summonerName;
    }
}
