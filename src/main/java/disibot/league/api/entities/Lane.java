package disibot.league.api.entities;

public enum Lane {
    TOP,
    JUNGLE,
    MID,
    BOTTOM,
    SUPPORT,
    NONE
}
