package disibot.league.api.entities;

import disibot.league.api.dataDragon.entities.Champion;
import disibot.league.api.exceptions.LeagueAPIException;
import disibot.league.api.impl.LeagueAPIEntity;

import java.io.IOException;
import java.time.LocalDateTime;

import static disibot.util.Util.ofEpochMillis;

public class SummonerChampionInfo extends LeagueAPIEntity {
    private int championLevel;
    private boolean chestGranted;
    private int championPoints;
    private long championPointsSinceLastLevel;
    private long championPointsUntilNextLevel;
    private String summonerId;
    private int tokensEarned;
    private Champion championId;
    private long lastPlayTime; // ms unix time

    public int getChampionLevel() {
        return championLevel;
    }

    public boolean isChestGranted() {
        return chestGranted;
    }

    public int getChampionPoints() {
        return championPoints;
    }

    public long getChampionPointsSinceLastLevel() {
        return championPointsSinceLastLevel;
    }

    public long getChampionPointsUntilNextLevel() {
        return championPointsUntilNextLevel;
    }

    public int getTokensEarned() {
        return tokensEarned;
    }

    public Champion getChampion() {
        return championId;
    }

    public LeagueSummoner getSummoner() throws IOException, LeagueAPIException {
        return getAPI().getSummonerById(summonerId);
    }

    public LocalDateTime getLastTimePlayed() {
        return ofEpochMillis(lastPlayTime);
    }
}
