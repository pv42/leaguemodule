package disibot.league.api.entities;

import disibot.league.api.exceptions.LeagueAPIException;
import disibot.league.api.impl.LeagueAPIEntity;

import java.io.IOException;

public class ClashTeamMember extends LeagueAPIEntity {
    private String summonerId;
    private int teamId;
    private Position position;
    private TeamRole role;

    public Position getPosition() {
        return position;
    }

    public TeamRole getRole() {
        return role;
    }

    public ClashTeam getTeam() throws IOException, LeagueAPIException {
        return getAPI().getClashTeamById(teamId);
    }

    public LeagueSummoner getSummoner() throws IOException, LeagueAPIException {
        return getAPI().getSummonerById(summonerId);
    }
}
