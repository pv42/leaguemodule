package disibot.league.api.entities;

import disibot.league.api.dataDragon.entities.Champion;

import javax.annotation.Nullable;

public class LeagueMatchParticipant {
    private PlayerStats stats;
    private int participantId;
    // private timeline
    private Team teamId;
    private SummonerSpell spell1Id;
    private SummonerSpell spell2Id;
    @Nullable
    private String highestAchievedSeasonTier;
    private Champion championId;

    public Champion getChampion() {
        return championId;
    }

    public Team getTeam() {
        return teamId;
    }

    public PlayerStats getStats() {
        return stats;
    }

    @Override
    public String toString() {
        return "LeagueMatchParticipant{" +
                "participantId=" + participantId +
                ", teamId=" + teamId +
                ", spell1Id=" + spell1Id.getName() +
                ", spell2Id=" + spell2Id.getName() +
                ", highestAchievedSeasonTier='" + highestAchievedSeasonTier + '\'' +
                ", championId=" + championId.getName() +
             // ", stats=" + stats +
                '}';
    }
}
