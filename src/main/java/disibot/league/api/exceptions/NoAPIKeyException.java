package disibot.league.api.exceptions;

/**
 * The Exception thrown, when a league api endpoint answers with http response code 401 (Unauthorized).
 * This indicates, that no authorization via api key was provided
 */
public class NoAPIKeyException extends LeagueAPIException {
    public NoAPIKeyException() {
        super("There required authorization via API key was not provided.");
    }
}
