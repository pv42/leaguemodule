package disibot.league.api.exceptions;

/**
 * The Exception thrown, when a league api endpoint answers with http response code 403 (Forbidden).
 * This indicates, that the provided authorization was invalid. This may occur, when using an invalid or expired api key
 * is used or a endpoint is accessed, which does not accept authorization via api key.
 */
public class InvalidAPIKeyException extends LeagueAPIException {
    public InvalidAPIKeyException() {
        super("The api key provided is invalid, expired or not for the api endpoint requested. See https://developer.riotgames.com/");
    }
}
