package disibot.league.api.exceptions;


/**
 * The Exception thrown, when a league api endpoint answers with http response code 500 (Internal Server Error).
 * This indicates, that there was an unexpected error on the server while handling the request.
 */
public class InternalServerErrorException extends LeagueAPIException {
    public InternalServerErrorException() {
        super("The API server has encounterd an interal error");
    }
}
