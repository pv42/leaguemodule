package disibot.league.api.exceptions;

public class RateLimitException extends LeagueAPIException {
    private final int retryAfter;
    public RateLimitException(int retryAfter) {
        super("The requested object was not found");
        this.retryAfter = retryAfter;
    }

    public int getRetryAfter() {
        return retryAfter;
    }
}
