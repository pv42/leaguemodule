package disibot.league.api.exceptions;

public class LeagueAPIException extends Exception {
    public LeagueAPIException(String msg) {
        super(msg);
    }

    public LeagueAPIException() {
        super();
    }
}
