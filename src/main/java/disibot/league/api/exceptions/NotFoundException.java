package disibot.league.api.exceptions;

/**
 * The Exception thrown, when a league api endpoint answers with http response code 404 (Not found).
 * This indicates, that either the requested endpoint does not exists or a given parameter e.g. summonerId does not
 * exist.
 */
public class NotFoundException extends LeagueAPIException {
    public NotFoundException() {
        super("The requested object was not found");
    }
}
