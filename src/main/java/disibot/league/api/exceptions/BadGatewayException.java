package disibot.league.api.exceptions;

public class BadGatewayException extends LeagueAPIException {
    public BadGatewayException() {
        super("Bad Gateway");
    }
}
