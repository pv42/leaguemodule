package disibot.league.api.exceptions;

public class GatewayTimeOutException extends LeagueAPIException {
    public GatewayTimeOutException() {
        super("The gateway timed out");
    }
}
