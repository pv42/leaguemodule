package disibot.league.api.exceptions;

/**
 * The Exception thrown, when a league api endpoint answers with http response code 400 (Bad Request)
 */
public class BadRequestException extends LeagueAPIException {
    public BadRequestException() {
        super("Syntax error in the request");
    }
}
