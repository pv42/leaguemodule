package disibot.league.api.dataDragon;

import com.google.gson.Gson;
import disibot.league.api.impl.DataDragonAPIEntity;
import disibot.league.api.impl.ParameterizedGsonCachedURLDownloader;
import disibot.league.api.impl.URLGenerator;

import javax.annotation.Nullable;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.time.Duration;

public class DataDragonParameterizedDownloader<R extends DataDragonAPIEntity, P> extends ParameterizedGsonCachedURLDownloader<R, P> {
    private final DataDragonAPI dataDragonAPI;
    public DataDragonParameterizedDownloader(Duration validityDuration, URLGenerator<P> url, @Nullable String diskCachePathFormat,DataDragonAPI dataDragonAPI, Gson gson, Class<R> clazz) {
        super(validityDuration, url, diskCachePathFormat, gson, clazz);
        this.dataDragonAPI = dataDragonAPI;
    }

    public DataDragonParameterizedDownloader(Duration validityDuration, URLGenerator<P> url, @Nullable String diskCachePathFormat, DataDragonAPI dataDragonAPI, Gson gson, Type type) {
        super(validityDuration, url, diskCachePathFormat, gson, type);
        this.dataDragonAPI = dataDragonAPI;
    }

    @Override
    protected R deserialize(InputStream in) {
        R r = super.deserialize(in);
        r.setDataDragon(dataDragonAPI);
        return r;
    }
}
