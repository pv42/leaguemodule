package disibot.league.api.dataDragon.entities;

import disibot.league.api.impl.DataDragonAPIEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class LeagueChampionInfos extends DataDragonAPIEntity {
    private transient static final Logger LOG = LoggerFactory.getLogger(LeagueChampionInfos.class);
    private List<Champion> data;
    private String format;
    private Version version;
    private String type;

    private Map<Long, Integer> championKeyMap;

    public List<Champion> getList() {
        return data;
    }

    @Nullable
    public Champion getByNameId(String id) { // O(log n)
        int lwr = 0;
        int upr = data.size();
        while (upr - lwr > 0) {
            int mid = lwr + (upr - lwr) / 2;
            int compare = data.get(mid).getNameId().compareTo(id);
            if (compare == 0) return data.get(mid);
            if (compare > 0) {
                upr = mid;
            } else {
                lwr = mid;
            }
        }
        return null;
    }

    @Nullable
    public Champion getByName(String name) { // O(n)
        for (Champion info : data) {
            if (info.getName().equals(name)) return info;
        }
        return null;
    }

    @Nullable
    public Champion getByKey(long id) { // O(n)
        if (championKeyMap == null) {
            LOG.debug("creating champion key hash map");
            championKeyMap = new HashMap<>();
            for (int i = 0; i < data.size(); i++) {
                championKeyMap.put(data.get(i).getKey(), i);
            }
        }
        if (championKeyMap.containsKey(id)) return data.get(championKeyMap.get(id));
        return null; // this is common for bans, when no champ was banned
    }

    public Version getVersion() {
        return version;
    }
}
