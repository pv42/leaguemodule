package disibot.league.api.dataDragon.entities;

import disibot.league.api.dataDragon.ItemStatKey;
import disibot.league.api.dataDragon.ItemTag;
import disibot.league.api.exceptions.LeagueAPIException;
import disibot.league.api.impl.DataDragonAPIEntity;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@SuppressWarnings("unused")
public class LeagueItem extends DataDragonAPIEntity {

    private Long id;
    private String name;
    private String description;
    private String colloq; // = ; // ?
    private String plaintext;
    private List<Long> from; // builds from ...
    private List<Long> into; // builds into ...
    private LeagueImageInfo image;
    private PrizeInfo gold;
    private List<ItemTag> tags; // []
    private Map<ItemStatKey, Double> stats;
    private Map<Integer, Boolean> maps; //todo map ids
    //todo private effect


    public Long getId() {
        return id;
    }

    public void setId(long id) {
        if (this.id != null) throw new UnsupportedOperationException("Can't change id of item after it is set");
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public String getDescriptionText() {
        return plaintext;
    }

    public String getStatsDescription() {
        return description;
    }

    public LeagueImageInfo getImage() {
        return image;
    }

    public boolean isPurchasable() {
        return gold.purchasable;
    }

    public int getBasePrize() {
        return gold.base;
    }

    public int getTotalPrize() {
        return gold.total;
    }

    public int getSellPrize() {
        return gold.sell;
    }

    public List<ItemTag> getTags() {
        return tags;
    }


    public Map<ItemStatKey, Double> getStats() {
        return stats;
    }

    public Map<Integer, Boolean> getMaps() {
        return maps;
    }

    public List<LeagueItem> getComponents() throws IOException, LeagueAPIException {
        if(from == null) return null;
        List<LeagueItem> items = new ArrayList<>(from.size());
        for (long id : from) {
            items.add(getDataDragon().getItems().getItemById(id));
        }
        return items;
    }

    @Override
    public String toString() {
        return name;
    }

    private static class PrizeInfo {
        private int base;
        private boolean purchasable;
        private int total;
        private int sell;
    }
}
