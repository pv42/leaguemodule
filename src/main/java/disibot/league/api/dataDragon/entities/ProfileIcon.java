package disibot.league.api.dataDragon.entities;

import disibot.league.api.dataDragon.entities.DataDragonVersionInfo;
import disibot.league.api.dataDragon.entities.LeagueImageInfo;
import disibot.league.api.exceptions.LeagueAPIException;
import disibot.league.api.impl.DataDragonAPIEntity;

import java.io.IOException;

public class ProfileIcon extends DataDragonAPIEntity {
    private int id;
    private LeagueImageInfo image;

    public int getId() {
        return id;
    }

    public LeagueImageInfo getImage() {
        return image;
    }

    public String getUrl() throws IOException, LeagueAPIException {
        DataDragonVersionInfo version = getDataDragon().getVersionInfo();
        return version.getCdnUrl() + "/" + version.getProfileIcon().toShortenedString() + "/img/profileicon/" + id + ".png";
    }
}
