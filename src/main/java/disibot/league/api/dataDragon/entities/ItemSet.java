package disibot.league.api.dataDragon.entities;

import java.util.List;

public class ItemSet {
    private String champion;
    private String title;
    private String type;
    private String map;
    private String mode;
    private boolean priority;
    private List<ItemSetBlock> blocks;

    public String getChampion() {
        return champion;
    }

    public String getTitle() {
        return title;
    }

    public String getType() {
        return type;
    }

    public String getMap() {
        return map;
    }

    public String getMode() {
        return mode;
    }

    public boolean isPriority() {
        return priority;
    }

    public List<ItemSetBlock> getBlocks() {
        return blocks;
    }
}
