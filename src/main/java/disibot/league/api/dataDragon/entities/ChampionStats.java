package disibot.league.api.dataDragon.entities;

public class ChampionStats {
    private float hp; // shoutouts to amumu with 612.12 hp (???)
    private float hpperlevel;
    private float mp; // amumu 287.2
    private float mpperlevel;
    private float movespeed;
    private float armor;
    private float armorperlevel;
    private float spellblock;
    private float spellblockperlevel;
    private int attackrange;
    private float hpregen;
    private float hpregenperlevel;
    private float mpregen;
    private float mpregenperlevel;
    private int crit;
    private int critperlevel; // lul
    private float attackdamage;
    private float attackdamageperlevel;
    private float attackspeedperlevel;
    private float attackspeed;

    public float getHP() {
        return hp;
    }

    public float getHPGain() {
        return hpperlevel;
    }

    public float getMP() {
        return mp;
    }

    public float getMPGain() {
        return mpperlevel;
    }

    public float getMovementSpeed() {
        return movespeed;
    }

    public float getArmor() {
        return armor;
    }

    public float getArmorGain() {
        return armorperlevel;
    }

    public float getMR() {
        return spellblock;
    }

    public float getMRGain() {
        return spellblockperlevel;
    }

    public int getAttackrange() {
        return attackrange;
    }

    public float getHealthReg() {
        return hpregen;
    }

    public float getHealthRegGain() {
        return hpregenperlevel;
    }

    public float getManaReg() {
        return mpregen;
    }

    public float getManaRegGain() {
        return mpregenperlevel;
    }

    public int getCrit() {
        return crit;
    }

    public int getCritGain() {
        return critperlevel;
    }

    public float getAD() {
        return attackdamage;
    }

    public float getADGain() {
        return attackdamageperlevel;
    }


    public float getAttackspeed() {
        return attackspeed;
    }

    public float getAttackspeedGain() {
        return attackspeedperlevel;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ChampionStats that = (ChampionStats) o;

        if (hp != that.hp) return false;
        if (hpperlevel != that.hpperlevel) return false;
        if (mp != that.mp) return false;
        if (mpperlevel != that.mpperlevel) return false;
        if (movespeed != that.movespeed) return false;
        if (Float.compare(that.armor, armor) != 0) return false;
        if (Float.compare(that.armorperlevel, armorperlevel) != 0) return false;
        if (Float.compare(that.spellblock, spellblock) != 0) return false;
        if (Float.compare(that.spellblockperlevel, spellblockperlevel) != 0) return false;
        if (attackrange != that.attackrange) return false;
        if (Float.compare(that.hpregen, hpregen) != 0) return false;
        if (Float.compare(that.hpregenperlevel, hpregenperlevel) != 0) return false;
        if (Float.compare(that.mpregen, mpregen) != 0) return false;
        if (Float.compare(that.mpregenperlevel, mpregenperlevel) != 0) return false;
        if (crit != that.crit) return false;
        if (critperlevel != that.critperlevel) return false;
        if (Float.compare(that.attackdamage, attackdamage) != 0) return false;
        if (Float.compare(that.attackdamageperlevel, attackdamageperlevel) != 0) return false;
        if (Float.compare(that.attackspeedperlevel, attackspeedperlevel) != 0) return false;
        return Float.compare(that.attackspeed, attackspeed) == 0;
    }

    @Override
    public int hashCode() {
        int result = (hp != +0.0f ? Float.floatToIntBits(hp) : 0);
        result = 31 * result + (hpperlevel != +0.0f ? Float.floatToIntBits(hpperlevel) : 0);
        result = 31 * result + (mp != +0.0f ? Float.floatToIntBits(mp) : 0);
        result = 31 * result + (mpperlevel != +0.0f ? Float.floatToIntBits(mpperlevel) : 0);
        result = 31 * result + (movespeed != +0.0f ? Float.floatToIntBits(movespeed) : 0);
        result = 31 * result + (armor != +0.0f ? Float.floatToIntBits(armor) : 0);
        result = 31 * result + (armorperlevel != +0.0f ? Float.floatToIntBits(armorperlevel) : 0);
        result = 31 * result + (spellblock != +0.0f ? Float.floatToIntBits(spellblock) : 0);
        result = 31 * result + (spellblockperlevel != +0.0f ? Float.floatToIntBits(spellblockperlevel) : 0);
        result = 31 * result + attackrange;
        result = 31 * result + (hpregen != +0.0f ? Float.floatToIntBits(hpregen) : 0);
        result = 31 * result + (hpregenperlevel != +0.0f ? Float.floatToIntBits(hpregenperlevel) : 0);
        result = 31 * result + (mpregen != +0.0f ? Float.floatToIntBits(mpregen) : 0);
        result = 31 * result + (mpregenperlevel != +0.0f ? Float.floatToIntBits(mpregenperlevel) : 0);
        result = 31 * result + crit;
        result = 31 * result + critperlevel;
        result = 31 * result + (attackdamage != +0.0f ? Float.floatToIntBits(attackdamage) : 0);
        result = 31 * result + (attackdamageperlevel != +0.0f ? Float.floatToIntBits(attackdamageperlevel) : 0);
        result = 31 * result + (attackspeedperlevel != +0.0f ? Float.floatToIntBits(attackspeedperlevel) : 0);
        result = 31 * result + (attackspeed != +0.0f ? Float.floatToIntBits(attackspeed) : 0);
        return result;
    }
}
