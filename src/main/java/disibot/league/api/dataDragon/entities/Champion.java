package disibot.league.api.dataDragon.entities;

import disibot.league.api.exceptions.LeagueAPIException;
import disibot.league.api.impl.DataDragonAPIEntity;

import java.io.IOException;
import java.util.List;

@SuppressWarnings("unused")
public class Champion extends DataDragonAPIEntity {
    private long key; // String ??
    private String id; // name without special characters
    private String name;
    private Version version;
    private String title;
    private String blurb; // short lore
    private String partype; // secondary bar e.g. mana
    private List<String> tags; // e.g. Marksman, Fighter, ...
    private LeaguePlaystyleInfo info;
    private LeagueImageInfo image;
    private ChampionStats stats;

    protected Champion() {}

    // for tests
    Champion(long key, String id, String name, Version version, String title, String blurb, String partype, List<String> tags, LeaguePlaystyleInfo info, LeagueImageInfo image, ChampionStats stats) {
        this.key = key;
        this.id = id;
        this.name = name;
        this.version = version;
        this.title = title;
        this.blurb = blurb;
        this.partype = partype;
        this.tags = tags;
        this.info = info;
        this.image = image;
        this.stats = stats;
    }

    public String getNameId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public long getKey() {
        return key;
    }

    public Version getVersion() {
        return version;
    }

    public String getTitle() {
        return title;
    }

    public String getSecondaryBar() {
        return partype;
    }

    public List<String> getTags() {
        return tags;
    }

    public LeagueImageInfo getImage() {
        return image;
    }

    public String getLoreShort() {
        return blurb;
    }

    public LeaguePlaystyleInfo getPlaystyleInfo() {
        return info;
    }

    public ChampionStats getStats() {
        return stats;
    }

    public ExtendedChampionInfo getExtendedInfo() throws IOException, LeagueAPIException {
        return getDataDragon().getChampionInfoExtended(this);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Champion that = (Champion) o;
        if (key != that.key) return false;
        if (!id.equals(that.id)) return false;
        return version.equals(that.version);
    }

    @Override
    public int hashCode() {
        int result = (int) (key ^ (key >>> 32));
        result = 31 * result + id.hashCode();
        result = 31 * result + version.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return id;
    }

    //todo skins ....
}
