package disibot.league.api.dataDragon.entities;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ChampionSpell {
    private String id; // "ZyraQ"
    private String name; // Deadly Spines
    private String description; // without stats
    private String tooltip; // with stats and html markers
    private SpellLevelupToolTip leveltip;
    private int maxrank;
    private List<Float> cooldown; // for each level
    private List<Integer> cost; // for each level
    private String costBurn; // cost as singe string
    // private datavalues
    private List<List<Float>> effect; // data values for formatting, first element of each inner list is null
    private List<String> effectBurn; // data values for formatting as single strings, first element is null
    private List<SpellVariable> vars;
    private String costType; // e.g. "1 Seed" or {{ abilityresourcename }}
    private int maxammo; // may be -1, stored as String is always int ?
    private List<Integer> range; // for each level
    private LeagueImageInfo image;
    private String resource; // format string for spell cost e.g. "{{ cost }} {{ abilityresourcename }}"

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getTooltipRaw() {
        return tooltip;
    }

    public String getTooltipFormatted() {
        String tt = tooltip;
        Pattern pattern = Pattern.compile("\\{\\{ [aef][1-9] }}");
        Matcher matcher = pattern.matcher(tt);
        while (matcher.find()) {
            String key = matcher.group().substring(3, 5);
            char keyType = key.charAt(0);
            int keyNum = key.charAt(1) - '0';
            String replacement = null;
            switch (keyType) {
                case 'a':
                    for (SpellVariable var : vars) {
                        if(var.key.equals(key)) {
                            replacement = String.valueOf(var.coeff);
                            break;
                        }
                    }
                    break;
                case 'f':
                    replacement = "?";
                    break;
                case 'e':
                    replacement = effectBurn.get(keyNum);
            }
            if(replacement == null) replacement = "??";
            tt = tt.replaceFirst("\\{\\{ " + key + " }}", replacement);
        }
        return tt;
    }

    public SpellLevelupToolTip getLeveltipRaw() {
        return leveltip; //todo
    }

    // level usually 1..5
    public float getCooldownAtLevel(int level) {
        return cooldown.get(level - 1);
    }

    // level usually 1..5
    public int getCostAtLevel(int level) {
        return cost.get(level - 1);
    }

    // -1 for not ammo
    public int getMaxAmmo() {
        return maxammo;
    }

    public int getRangeAtLevel(int level) {
        return range.get(level);
    }

    public LeagueImageInfo getImage() {
        return image;
    }

    public String getResource() {
        return resource;
    }

    private static class SpellVariable {
        private String link; // e.g. "spelldamage"
        private float coeff;
        private String key; // e.g. "a1"
    }

    private static class SpellLevelupToolTip {
        private List<String> label;
        private List<String> effect;
    }
}
