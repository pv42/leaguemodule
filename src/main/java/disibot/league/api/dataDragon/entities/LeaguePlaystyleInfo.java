package disibot.league.api.dataDragon.entities;

public class LeaguePlaystyleInfo {
    private int difficulty;
    private int attack;
    private int magic;
    private int defense;

    LeaguePlaystyleInfo(int difficulty, int attack, int magic, int defense) {
        this.difficulty = difficulty;
        this.attack = attack;
        this.magic = magic;
        this.defense = defense;
    }

    public int getDifficulty() {
        return difficulty;
    }

    public int getAttack() {
        return attack;
    }

    public int getMagic() {
        return magic;
    }

    public int getDefense() {
        return defense;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LeaguePlaystyleInfo that = (LeaguePlaystyleInfo) o;
        if (difficulty != that.difficulty) return false;
        if (attack != that.attack) return false;
        if (magic != that.magic) return false;
        return defense == that.defense;
    }

    @Override
    public int hashCode() {
        int result = difficulty;
        result = 31 * result + attack;
        result = 31 * result + magic;
        result = 31 * result + defense;
        return result;
    }
}
