package disibot.league.api.dataDragon.entities;

import javax.annotation.Nonnull;

public class Version {
    private int mayor;
    private int minor;
    private long build;

    public Version(int mayor, int minor, long build) {
        this.mayor = mayor;
        this.minor = minor;
        this.build = build;
    }

    public Version(@Nonnull String versionString) {
        try {
            this.mayor = Integer.parseInt(versionString.split("\\.")[0]);
            this.minor = Integer.parseInt(versionString.split("\\.")[1]);
            this.build = Long.parseLong(versionString.split("\\.")[2]);
        } catch (NumberFormatException | ArrayIndexOutOfBoundsException e) {
            throw new IllegalArgumentException("Not a valid version string: '" + versionString + "'");
        }
    }

    public int getMayor() {
        return mayor;
    }

    public int getMinor() {
        return minor;
    }

    public long getBuild() {
        return build;
    }

    @Override
    public String toString() {
        return mayor + "." + minor + "." + build;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Version version = (Version) o;
        if (mayor != version.mayor) return false;
        if (minor != version.minor) return false;
        return build == version.build;
    }

    @Override
    public int hashCode() {
        int result = mayor;
        result = 1023 * result + minor;
        result = (int) (1023 * result + build);
        return result;
    }

    // after 10.10 the versions in reamls changed form 10.9.1 to 10.10.32161768608‬, but ddragon still requires shorter
    // versions like 10.10.3216176
    public String toShortenedString() {
        String buildString;
        if (build > 9999999) {
            buildString = Long.toString(build / 10000L); // e.g. 32161768608‬ -> 3216176
        } else {
            buildString = Long.toString(build);
        }
        return mayor + "." + minor + "." + buildString;
    }
}
