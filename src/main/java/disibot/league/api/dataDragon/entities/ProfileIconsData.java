package disibot.league.api.dataDragon.entities;

import disibot.league.api.impl.DataDragonAPIEntity;

import java.util.HashMap;
import java.util.Map;

public class ProfileIconsData extends DataDragonAPIEntity {
    private String type;
    private Version version;
    private Map<PlaceholdableInteger, ProfileIcon> data;
    private transient Map<Integer, ProfileIcon> cleanedData;

    public String getType() {
        return type;
    }

    public Version getVersion() {
        return version;
    }

    public Map<Integer, ProfileIcon> getData() {
        if (cleanedData == null) generateCleanData();
        return cleanedData;
    }

    private void generateCleanData() {
        cleanedData = new HashMap<>();
        for (PlaceholdableInteger pi : data.keySet()) {
            if(!pi.isPlaceholder()) cleanedData.put(pi.getValue(), data.get(pi));
        }
    }
}
