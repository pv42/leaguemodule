package disibot.league.api.dataDragon.entities;

import disibot.league.api.impl.DataDragonAPIEntity;

import java.util.Map;

public class LeagueItemInfos extends DataDragonAPIEntity {
    private String type; // item
    private Version version;
    //private base;
    private Map<Long, LeagueItem> data; // <id,data>
    //private groups;
    //private tree;

    public Map<Long, LeagueItem> getAllItems() {
        for (Long id : data.keySet()) {
            if (data.get(id).getId() == null) {
                data.get(id).setId(id);
                data.get(id).setDataDragon(getDataDragon());
            }
        }
        return data;
    }

    public LeagueItem getItemById(long id) {
        LeagueItem item = data.get(id);
        if (item != null && item.getId() == null) {
            item.setId(id);
            item.setDataDragon(getDataDragon());
        }
        return item;
    }

    public Version getVersion() {
        return version;
    }
}
