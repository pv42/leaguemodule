package disibot.league.api.dataDragon.entities;

public class DataDragonVersionInfo {
    private static class NewData { // exists for the purpose of easy gson adaptation
        private Version item;
        private Version rune;
        private Version mastery;
        private Version summoner;
        private Version champion;
        private Version profileicon;
        private Version map;
        private Version language;
        private Version sticker;
    }
    private NewData n;
    // end n
    private Version v;
    private String l;
    private String cdn;
    private Version dd; // data dragon
    private Version lg;
    private Version css;
    private int profileiconmax;

    public String getCdnUrl() {
        return cdn;
    }

    public Version getChampion() {
        return n.champion;
    }

    public Version getSummoner() {
        return n.summoner;
    }

    public Version getItem() {
        return n.item;
    }

    public Version getRune() {
        return n.rune;
    }

    public Version getMap() {
        return n.map;
    }

    public Version getProfileIcon() {
        return n.profileicon;
    }
}


/*
* {
* "n":{
*   "item":"10.1.1",
*   "rune":"7.23.1",
*   "mastery":"7.23.1",
*   "summoner":"10.1.1",
*   "champion":"10.1.1",
*   "profileicon":"10.1.1",
*   "map":"10.1.1",
*   "language":"10.1.1",
*   "sticker":"10.1.1"
* },
* "v":"10.1.1",
* "l":"en_US",
* "cdn":"https://ddragon.leagueoflegends.com/cdn",
* "dd":"10.1.1",
* "lg":"10.1.1",
* "css":"10.1.1",
* "profileiconmax":28,
* "store":null}
* */