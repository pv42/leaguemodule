package disibot.league.api.dataDragon.entities;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;

public class PlaceholdableIntegerAdapter extends TypeAdapter<PlaceholdableInteger> {
    @Override
    public void write(JsonWriter out, PlaceholdableInteger value) throws IOException {
        if(value.isPlaceholder()) out.value("placeholder");
        else out.value(value.getValue());
    }

    @Override
    public PlaceholdableInteger read(JsonReader in) throws IOException {
        String next = in.nextString();
        if(next.equals("placeholder")) return new PlaceholdableInteger(true,0);
        else return new PlaceholdableInteger(false, Integer.valueOf(next));
    }
}
