package disibot.league.api.dataDragon.entities;

import disibot.league.api.entities.SummonerSpell;
import disibot.league.api.impl.DataDragonAPIEntity;

import java.util.Collection;
import java.util.Map;

public class SummonerSpellsData extends DataDragonAPIEntity {
    private String type;
    private Version version;
    private Map<String,SummonerSpell> data;

    public String getType() {
        return type;
    }

    public Version getVersion() {
        return version;
    }

    public Collection<SummonerSpell> getData() {
        return data.values();
    }
}
