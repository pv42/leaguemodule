package disibot.league.api.dataDragon.entities;


import java.util.Objects;

public class LeagueImageInfo {
    private String full;
    private String sprite; // e.g. http://ddragon.leagueoflegends.com/cdn/10.1.1/img/sprite/champion4.png
    private String group; // e.g. champion
    private int x;
    private int y;
    private int w;
    private int h;

    public LeagueImageInfo(String full, String sprite, String group, int x, int y, int w, int h) {
        this.full = full;
        this.sprite = sprite;
        this.group = group;
        this.x = x;
        this.y = y;
        this.w = w;
        this.h = h;
    }

    public String getFull() {
        return full;
    }

    public String getSprite() {
        return sprite;
    }

    public int getSpriteOffsetX() {
        return x;
    }

    public int getSpriteOffsetY() {
        return y;
    }

    public int getSpriteWidth() {
        return w;
    }

    public int getSpriteHeight() {
        return h;
    }

    public String getGroup() {
        return group;
    }

    @Override
    public String toString() {
        return "Image: " + full + " sprite: " + sprite +"@px(" + x + "," + y + ") " + w + "x" + h;

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LeagueImageInfo that = (LeagueImageInfo) o;
        if (x != that.x) return false;
        if (y != that.y) return false;
        if (w != that.w) return false;
        if (h != that.h) return false;
        if (!Objects.equals(full, that.full)) return false;
        if (!Objects.equals(sprite, that.sprite)) return false;
        return Objects.equals(group, that.group);
    }

    @Override
    public int hashCode() {
        int result = full.hashCode();
        result = 31 * result + sprite.hashCode();
        result = 31 * result + group.hashCode();
        result = 31 * result + x;
        result = 31 * result + y;
        result = 31 * result + w;
        result = 31 * result + h;
        return result;
    }
}
