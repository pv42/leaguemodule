package disibot.league.api.dataDragon.entities;

public class ItemSetItem {
    private int count;
    private long id;

    public int getCount() {
        return count;
    }

    public long getItem() {
        return id;
    }
}
