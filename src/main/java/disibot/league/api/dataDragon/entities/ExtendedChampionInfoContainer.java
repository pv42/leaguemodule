package disibot.league.api.dataDragon.entities;

import disibot.league.api.impl.DataDragonAPIEntity;

import java.util.Map;

public class ExtendedChampionInfoContainer extends DataDragonAPIEntity {
    private String type;
    private String format;
    private Version version;
    private Map<String, ExtendedChampionInfo> data;


    public String getType() {
        return type;
    }

    public String getFormat() {
        return format;
    }

    public Version getVersion() {
        return version;
    }

    public ExtendedChampionInfo getData() {
        return data.get(data.keySet().iterator().next());
    }
}
