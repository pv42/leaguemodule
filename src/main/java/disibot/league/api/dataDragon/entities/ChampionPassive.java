package disibot.league.api.dataDragon.entities;

public class ChampionPassive {
    private String name;
    private String description;
    private LeagueImageInfo image;

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public LeagueImageInfo getImage() {
        return image;
    }
}
