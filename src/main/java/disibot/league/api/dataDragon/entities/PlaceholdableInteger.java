package disibot.league.api.dataDragon.entities;

/**
 * integer that can also be an place holder
 */
public class PlaceholdableInteger {
    private boolean isPlaceholder;
    private Integer value;

    public PlaceholdableInteger(boolean isPlaceholder, Integer value) {
        this.isPlaceholder = isPlaceholder;
        this.value = value;
    }

    public boolean isPlaceholder() {
        return isPlaceholder;
    }

    public Integer getValue() {
        return value;
    }
}
