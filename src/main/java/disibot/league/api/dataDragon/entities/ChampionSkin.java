package disibot.league.api.dataDragon.entities;

public class ChampionSkin {
    private long id; // global id
    private int num; // per champ id
    private String name;
    private boolean chromas;

    public long getId() {
        return id;
    }

    public int getNum() {
        return num;
    }

    public String getName() {
        return name;
    }

    public boolean hasChromas() {
        return chromas;
    }
}
