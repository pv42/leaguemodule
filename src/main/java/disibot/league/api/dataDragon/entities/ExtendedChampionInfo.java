package disibot.league.api.dataDragon.entities;

import java.util.List;

public class ExtendedChampionInfo extends Champion {
    /* LeagueChampionInfo:
    private long key; // String ??
    private String id; // name without special characters
    private String name;
    private Version version;
    private String title;
    private String blurb; // short lore
    private String partype; // secondary bar e.g. mana
    private List<String> tags; // e.g. Marksman, Fighter, ...
    private LeaguePlaystyleInfo info;
    private LeagueImageInfo image;
     */
    private List<ChampionSkin> skins;
    private String lore;
    private List<String> allytips;
    private List<String> enemytips;
    private List<ChampionSpell> spells;
    private ChampionPassive passive;
    private List<ItemSet> recommended;


    public List<ChampionSpell> getSpells() {
        return spells;
    }

    public List<ChampionSkin> getSkins() {
        return skins;
    }

    public ChampionPassive getPassive() {
        return passive;
    }

    public String getLore() {
        return lore;
    }

    public List<String> getAllyTips() {
        return allytips;
    }

    public List<String> getEnemyTips() {
        return enemytips;
    }
}
