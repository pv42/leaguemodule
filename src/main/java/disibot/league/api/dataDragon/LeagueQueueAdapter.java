package disibot.league.api.dataDragon;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import disibot.league.api.staticAPI.LeagueQueue;

import java.io.IOException;
import java.util.List;

public class LeagueQueueAdapter extends TypeAdapter<LeagueQueue> {
    private List<LeagueQueue> queues;
    public LeagueQueueAdapter(List<LeagueQueue> queues) {
        this.queues = queues;
    }

    @Override
    public void write(JsonWriter out, LeagueQueue value) throws IOException {
        out.value(value.getId());
    }

    @Override
    public LeagueQueue read(JsonReader in) throws IOException {
        int id = in.nextInt();
        for(LeagueQueue queue: queues) {
            if(queue.getId() == id) return queue;
        }
        throw new IllegalArgumentException("Invalid league queue id");
    }
}
