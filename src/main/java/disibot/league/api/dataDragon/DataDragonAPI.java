package disibot.league.api.dataDragon;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import disibot.league.api.dataDragon.entities.Champion;
import disibot.league.api.dataDragon.entities.DataDragonVersionInfo;
import disibot.league.api.dataDragon.entities.ExtendedChampionInfo;
import disibot.league.api.dataDragon.entities.ExtendedChampionInfoContainer;
import disibot.league.api.dataDragon.entities.LeagueChampionInfos;
import disibot.league.api.dataDragon.entities.LeagueItemInfos;
import disibot.league.api.dataDragon.entities.PlaceholdableInteger;
import disibot.league.api.dataDragon.entities.PlaceholdableIntegerAdapter;
import disibot.league.api.dataDragon.entities.ProfileIcon;
import disibot.league.api.dataDragon.entities.ProfileIconsData;
import disibot.league.api.dataDragon.entities.SummonerSpellsData;
import disibot.league.api.dataDragon.entities.Version;
import disibot.league.api.entities.SummonerSpell;
import disibot.league.api.exceptions.LeagueAPIException;
import disibot.league.api.impl.DataDragonAPIEntity;
import disibot.league.api.impl.HttpURLDownloader;
import disibot.league.api.impl.TimedGsonCachedURLDownloader;
import disibot.league.api.impl.URLGenerator;
import disibot.league.api.impl.VersionProvider;
import disibot.league.api.impl.WebAPI;
import org.jetbrains.annotations.Contract;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.IOException;
import java.lang.reflect.Type;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class DataDragonAPI extends WebAPI {
    public static final String DEFAULT_REGION = "na";
    private static final Logger LOG = LoggerFactory.getLogger(DataDragonAPI.class);
    private static final Type summonerSpellListType = new TypeToken<List<SummonerSpell>>() {}.getType();
    private static final Language DEFAULT_LANGUAGE = Language.en_US;
    private static Gson gson;
    private final Language language;
    private TimedGsonCachedURLDownloader<DataDragonVersionInfo> versionInfo;
    private DataDragonVersionedDownloader<SummonerSpellsData> summonerSpells;
    private DataDragonVersionedDownloader<LeagueChampionInfos> championInfos;
    private DataDragonVersionedDownloader<LeagueItemInfos> items;
    private DataDragonVersionedDownloader<ProfileIconsData> profileIcons;
    private DataDragonParameterizedDownloader<ExtendedChampionInfoContainer, Champion> extendedChampionInfo;
    private final boolean useDiskCache;


    public DataDragonAPI() throws IOException, LeagueAPIException {
        this(DEFAULT_REGION, DEFAULT_LANGUAGE, true, true);
    }

    public DataDragonAPI(String region, Language language, boolean cache, boolean useDiskCache) throws IOException, LeagueAPIException {
        this.language = language;
        this.useDiskCache = useDiskCache;
        initGson();
        initVersionInfo(region);
        initSummonerSpells();
        initChampionInfos();
        initItemCache();
        initProfileIcons();
        initExChampionInfos();
        if (cache) {
            versionInfo.get();
            summonerSpells.get();
            championInfos.get();
            items.get();
            profileIcons.get();
        }
    }

    private void initGson() {
        if (gson != null) return;
        GsonBuilder builder = new GsonBuilder();
        Type championInfoListType = new TypeToken<List<Champion>>() {}.getType();
        LeagueChampionInfoAdapter championInfoAdapter = new LeagueChampionInfoAdapter(this);
        builder.registerTypeAdapter(Version.class, new VersionAdapter());
        builder.registerTypeAdapter(summonerSpellListType, new SummonerSpellListAdapter());
        builder.registerTypeAdapter(championInfoListType, championInfoAdapter);
        builder.registerTypeAdapter(PlaceholdableInteger.class, new PlaceholdableIntegerAdapter());
        gson = builder.create();
        championInfoAdapter.setGson(gson);
    }

    @Nonnull
    @Contract("_, _, _, _ -> new")
    // getDataDragonVersionedCachedURLDownloader
    private <S extends DataDragonAPIEntity> DataDragonVersionedDownloader<S> getVersionedDownloader(String urlPath,
                                                                                                    VersionProvider versionProvider,
                                                                                                    Gson gson,
                                                                                                    Class<S> clazz) {
        return new DataDragonVersionedDownloader<>(
                getDataDragonURLGenerator(urlPath),
                versionProvider,
                useDiskCache ? "cache/datadragon" + urlPath : null,
                this,
                gson,
                clazz
        );
    }

    private void initVersionInfo(String region) throws MalformedURLException {
        URL url = new URL("https://ddragon.leagueoflegends.com/realms/" + region + ".json");
        versionInfo = new TimedGsonCachedURLDownloader<>(
                Duration.ofHours(1),
                url,
                useDiskCache ? "cache/datadragon/versions.json" : null,
                gson,
                DataDragonVersionInfo.class);
    }

    private void initExChampionInfos() {
        extendedChampionInfo = new DataDragonParameterizedDownloader<>(
                Duration.ofDays(1),
                champ -> getDataDragonURL(versionInfo.get().getChampion(),
                        "/champion/" + champ.getNameId() + ".json"),
                "cache/datadragon/champion/%s.json",
                this,
                gson,
                ExtendedChampionInfoContainer.class);

    }

    private URL getDataDragonURL(Version version, String path) throws IOException, LeagueAPIException {
        String versionString = version.toShortenedString();
        return new URL(versionInfo.get().getCdnUrl() + "/" + versionString + "/data/" + language + path);
    }

    private URLGenerator<Version> getDataDragonURLGenerator(String path) {
        return version -> getDataDragonURL(version, path);
    }

    private void initChampionInfos() {
        championInfos = getVersionedDownloader("/champion.json", () -> versionInfo.get().getChampion(), gson, LeagueChampionInfos.class);
    }

    private void initProfileIcons() {
        profileIcons = getVersionedDownloader("/profileicon.json", () -> versionInfo.get().getProfileIcon(), gson, ProfileIconsData.class);
    }

    private void initSummonerSpells() {
        summonerSpells = getVersionedDownloader("/summoner.json", () -> versionInfo.get().getSummoner(), gson, SummonerSpellsData.class);
    }


    private void initItemCache() {
        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapter(Version.class, new VersionAdapter());
        items = getVersionedDownloader("/item.json", () -> versionInfo.get().getItem(), builder.create(), LeagueItemInfos.class);
    }


    public Map<Integer, ProfileIcon> getProfileIcons() throws IOException, LeagueAPIException {
        Map<Integer, ProfileIcon> icons = profileIcons.get().getData();
        for (ProfileIcon icon : icons.values()) {
            if (icon.getDataDragon() == null) icon.setDataDragon(this);
        }
        return icons;
    }

    @Nullable
    public SummonerSpell getSummonerSpellByKey(long key) throws IOException, LeagueAPIException {
        List<SummonerSpell> summonerSpells = getSummonerSpells();
        for (SummonerSpell spell : summonerSpells) {
            if (spell.getKey() == key) return spell;
        }
        return null;
    }

    public Champion getChampionByName(String name) throws IOException, LeagueAPIException {
        return championInfos.get().getByName(name);
    }

    public Champion getChampionByNameId(String id) throws IOException, LeagueAPIException {
        return championInfos.get().getByNameId(id);
    }

    public Champion getChampionById(long key) throws IOException, LeagueAPIException {
        return championInfos.get().getByKey(key);
    }

    public ExtendedChampionInfo getChampionInfoExtended(Champion champion) throws IOException, LeagueAPIException {
        return extendedChampionInfo.get(champion).getData();
    }

    public List<SummonerSpell> getSummonerSpells() throws IOException, LeagueAPIException {
        return new ArrayList<>(summonerSpells.get().getData());
    }

    public LeagueItemInfos getItems() throws IOException, LeagueAPIException {
        return items.get();
    }

    public DataDragonVersionInfo getVersionInfo() throws IOException, LeagueAPIException {
        return versionInfo.get();
    }

    public LeagueChampionInfos getAllChampionInfos() throws IOException, LeagueAPIException {
        return championInfos.get();
    }

    @Override
    public void setDownloader(HttpURLDownloader downloader) {
        versionInfo.setDownloader(downloader);
        summonerSpells.setDownloader(downloader);
        championInfos.setDownloader(downloader);
        items.setDownloader(downloader);
        profileIcons.setDownloader(downloader);
        extendedChampionInfo.setDownloader(downloader);
    }
}
