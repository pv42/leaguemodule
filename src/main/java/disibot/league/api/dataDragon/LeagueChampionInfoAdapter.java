package disibot.league.api.dataDragon;

import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;
import disibot.league.api.dataDragon.entities.Champion;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

class LeagueChampionInfoAdapter extends TypeAdapter<List<Champion>> {
    private Gson gson;
    private final DataDragonAPI api;

    public LeagueChampionInfoAdapter(DataDragonAPI api) {
        this.api = api;
    }

    public void setGson(Gson gson) {
        this.gson = gson;
    }

    @Override
    public void write(JsonWriter out, List<Champion> value) throws IOException {
        for (Champion info : value) {
            out.name(info.getNameId());
            out.beginObject();
        }
    }

    @Override
    public List<Champion> read(JsonReader in) throws IOException {
        in.beginObject();
        JsonToken token = in.peek();
        List<Champion> championInfos = new ArrayList<>();
        while (token != JsonToken.END_OBJECT) {
            in.nextName();
            Champion champion = gson.fromJson(in, Champion.class);
            champion.setDataDragon(api);
            championInfos.add(champion);
            token = in.peek();
        }
        in.endObject();
        return championInfos;
    }
}
