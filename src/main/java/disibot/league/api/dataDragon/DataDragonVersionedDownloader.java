package disibot.league.api.dataDragon;

import com.google.gson.Gson;
import disibot.league.api.dataDragon.entities.Version;
import disibot.league.api.impl.DataDragonAPIEntity;
import disibot.league.api.impl.URLGenerator;
import disibot.league.api.impl.VersionProvider;
import disibot.league.api.impl.VersionedGsonCachedURLDownloader;

import javax.annotation.Nullable;
import java.io.InputStream;

public class DataDragonVersionedDownloader<T extends DataDragonAPIEntity> extends VersionedGsonCachedURLDownloader<T> {
    private final DataDragonAPI dataDragonAPI;


    public DataDragonVersionedDownloader(URLGenerator<Version> url, VersionProvider versionProvider, @Nullable String diskCachePath, DataDragonAPI dataDragonAPI, Gson gson, Class<T> clazz) {
        super(url, versionProvider, diskCachePath, gson, clazz);
        this.dataDragonAPI = dataDragonAPI;
    }

    @Override
    protected T deserialize(InputStream in) {
        T t = super.deserialize(in);
        if(t != null) {
            t.setDataDragon(dataDragonAPI);
        }
        return t;
    }
}
