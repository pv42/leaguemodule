package disibot.league.api.dataDragon;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import disibot.league.api.dataDragon.entities.Version;

import java.io.IOException;

class VersionAdapter extends TypeAdapter<Version> {
    @Override
    public void write(JsonWriter out, Version version) throws IOException {
        out.value(version.toString());
    }

    @Override
    public Version read(JsonReader in) throws IOException {
        return new Version(in.nextString());
    }
}
