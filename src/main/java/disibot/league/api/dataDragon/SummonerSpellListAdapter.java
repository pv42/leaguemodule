package disibot.league.api.dataDragon;

import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;
import disibot.league.api.entities.SummonerSpell;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

class SummonerSpellListAdapter extends TypeAdapter<List<SummonerSpell>> {
    private Gson gson;

    public SummonerSpellListAdapter() {
        this.gson = new Gson();
    }

    @Override
    public void write(JsonWriter out, List<SummonerSpell> value) throws IOException {
        throw new UnsupportedOperationException("Writing of summoner spells is not supported");
    }

    @Override
    public List<SummonerSpell> read(JsonReader in) throws IOException {
        in.beginObject();
        while (!in.nextName().equals("data")) {
            in.nextString();
        }
        in.beginObject();
        List<SummonerSpell> spells = new ArrayList<>();
        JsonToken token = in.peek();
        while (token != JsonToken.END_OBJECT) {
            in.nextName();
            spells.add(gson.fromJson(in, SummonerSpell.class));
            token = in.peek();
        }
        in.endObject();
        in.endObject();
        return spells;
    }
}
