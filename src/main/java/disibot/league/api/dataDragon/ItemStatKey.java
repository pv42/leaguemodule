package disibot.league.api.dataDragon;

public enum ItemStatKey {
    FlatSpellBlockMod,
    PercentAttackSpeedMod,
    FlatMovementSpeedMod,
    FlatMagicDamageMod,
    FlatArmorMod,
    FlatHPRegenMod,
    FlatMPPoolMod,
    PercentMovementSpeedMod,
    FlatHPPoolMod,
    FlatCritChanceMod,
    FlatPhysicalDamageMod,
    PercentLifeStealMod
}
