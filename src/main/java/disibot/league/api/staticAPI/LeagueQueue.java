package disibot.league.api.staticAPI;

public class LeagueQueue {
    private int queueId;
    private String map;
    private String description;
    private String notes;

    public int getId() {
        return queueId;
    }

    public String getMap() {
        return map;
    }

    @Override
    public String toString() {
        return map + ' ' + description;
    }
}
