package disibot.league.api.staticAPI;

@SuppressWarnings("unused")
public class LeagueSeasonImpl implements LeagueSeason {

    private int id;
    private String season;

    public String getName() {
        return season;
    }

    public int getId() {
        return id;
    }

    @Override
    public String toString() {
        return season;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LeagueSeasonImpl otherSeason = (LeagueSeasonImpl) o;
        return id == otherSeason.id;
    }

    @Override
    public int hashCode() {
        return id;
    }
}
