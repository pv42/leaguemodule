package disibot.league.api.staticAPI;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import disibot.league.api.exceptions.LeagueAPIException;
import disibot.league.api.impl.HttpURLDownloader;
import disibot.league.api.impl.TimedGsonCachedURLDownloader;
import disibot.league.api.impl.WebAPI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.lang.reflect.Type;
import java.net.URL;
import java.time.Duration;
import java.util.List;

public class StaticAPI extends WebAPI {
    private static final Logger LOG = LoggerFactory.getLogger(StaticAPI.class);
    private TimedGsonCachedURLDownloader<List<LeagueQueue>> queues;
    private TimedGsonCachedURLDownloader<List<LeagueSeasonImpl>> seasons;
    private boolean useDiskCache = true;

    /**
     * create an interface to the static league api, dont create cache now and dont use disk cache
     */
    public StaticAPI() throws IOException, LeagueAPIException {
        this(false, false);
    }

    /**
     * create an interface to the static league api
     *
     * @param createCacheNow if true (down-)load queue and season information now and store it, otherwise download on
     *                       first use
     */
    public StaticAPI(boolean createCacheNow, boolean useDiskCache) throws IOException, LeagueAPIException {
        this.useDiskCache = useDiskCache;
        Gson gson = new Gson();
        Type seasonsType = new TypeToken<List<LeagueSeasonImpl>>() {}.getType();
        seasons = new TimedGsonCachedURLDownloader<>(Duration.ofDays(1),
                new URL("http://static.developer.riotgames.com/docs/lol/seasons.json"),
                useDiskCache ? "cache/static/seasons.json" : null,
                gson,
                seasonsType
        );
        Type leagueQueueListType = new TypeToken<List<LeagueQueue>>() {}.getType();
        queues = new TimedGsonCachedURLDownloader<>(Duration.ofDays(1),
                new URL("http://static.developer.riotgames.com/docs/lol/queues.json"),
                useDiskCache ? "cache/static/queues.json" : null,
                gson,
                leagueQueueListType);
        if (createCacheNow) {
            queues.get();
            seasons.get();
        }
    }

    /**
     * get the game mode queues of league of legends, this will use the disk cache if enabled and present or send a
     * request to "static.developer.riotgames.com" to retrieve the queues
     *
     * @return a list of queues information that exist or existed in league of legends
     * @throws IOException        if there was a problem reading the response from the server or if diskcache is enabled from
     *                            the cache file
     * @throws LeagueAPIException if the league api answered with a response code other then 200
     */
    public List<LeagueQueue> getQueues() throws IOException, LeagueAPIException {
        return queues.get();
    }

    /**
     * get the ranked seasons of league of legends, this will use the disk cache if enabled and present or send a
     * request to "static.developer.riotgames.com" to retrieve the seasons
     *
     * @return a list of queues information that exist or existed in league of legends
     * @throws IOException        if there was a problem reading the response from the server or if diskcache is enabled from
     *                            the cache file
     * @throws LeagueAPIException if the league api answered with a response code other then 200
     */
    public List<LeagueSeasonImpl> getSeasons() throws IOException, LeagueAPIException {
        return seasons.get();
    }

    @Override
    public void setDownloader(HttpURLDownloader downloader) {
        queues.setDownloader(downloader);
        seasons.setDownloader(downloader);
    }
}
