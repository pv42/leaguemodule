package disibot.league.api.staticAPI;

public interface LeagueSeason {
    String getName();
    int getId();
}
