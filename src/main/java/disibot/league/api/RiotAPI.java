package disibot.league.api;

import disibot.league.api.dataDragon.DataDragonAPI;
import disibot.league.api.entities.ClashTeamMember;
import disibot.league.api.entities.FreeChampionInfo;
import disibot.league.api.entities.LeagueMatch;
import disibot.league.api.entities.LeagueSummoner;
import disibot.league.api.exceptions.LeagueAPIException;

import java.io.IOException;
import java.util.List;

public interface RiotAPI {
    LeagueSummoner getSummonerByName(String name) throws IOException, LeagueAPIException;

    LeagueSummoner getSummonerByAccountId(String accountId) throws IOException, LeagueAPIException;

    LeagueSummoner getSummonerById(String summonerId) throws IOException, LeagueAPIException;

    FreeChampionInfo getFreeChampions() throws IOException, LeagueAPIException;

    boolean isDisabled();

    LeagueMatch getMatchById(long id) throws IOException, LeagueAPIException;

    DataDragonAPI getDataDragonAPI();
}
