package disibot.league;

import disibot.DisiBot;
import disibot.context.ContextStorage;
import disibot.league.api.RiotAPI;
import disibot.league.api.entities.ClashTeamMember;
import disibot.league.api.entities.CurrentGameInfo;
import disibot.league.api.entities.CurrentGameParticipant;
import disibot.league.api.entities.LeagueSummoner;
import disibot.league.api.entities.LeagueTier;
import disibot.league.api.entities.Position;
import disibot.league.api.entities.SummonerQueueInfo;
import disibot.league.api.exceptions.InvalidAPIKeyException;
import disibot.league.api.exceptions.LeagueAPIException;
import disibot.league.api.exceptions.NotFoundException;
import disibot.newcommand.SlashCommandHandler;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;
import net.dv8tion.jda.api.interactions.commands.OptionMapping;
import net.dv8tion.jda.api.interactions.commands.OptionType;
import net.dv8tion.jda.api.interactions.commands.build.Commands;
import net.dv8tion.jda.api.interactions.commands.build.SlashCommandData;
import net.dv8tion.jda.api.interactions.commands.build.SubcommandData;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static disibot.league.api.entities.LeagueTier.CHALLENGER;
import static disibot.league.api.entities.LeagueTier.GRANDMASTER;
import static disibot.league.api.entities.LeagueTier.MASTER;
import static disibot.league.api.entities.Team.RED;
import static net.dv8tion.jda.api.entities.EmbedType.RICH;

public class LeagueSlashCommandHandler implements SlashCommandHandler {
    private static final Logger LOG = LoggerFactory.getLogger(LeagueSlashCommandHandler.class);
    private final ContextStorage contexts;
    private final RiotAPI riotAPI;
    private final SummonerRankedStorage rankedStorage;

    public LeagueSlashCommandHandler(ContextStorage contexts, JDA jda, RiotAPI riotAPI, SummonerRankedStorage rankedStorage) {
        this.contexts = contexts;
        this.riotAPI = riotAPI;
        this.rankedStorage = rankedStorage;
    }

    @Override
    public SlashCommandData getCommand() {
        SlashCommandData cd = Commands.slash("lol", "League of Legends commands");
        SubcommandData rankcd = new SubcommandData("rank", "shows soloQ ranking");
        rankcd.addOption(OptionType.STRING, "summoner-name", "name of player, empty to see own ranking");
        SubcommandData livecd = new SubcommandData("live", "shows live game information");
        livecd.addOption(OptionType.STRING, "summoner-name", "name of player, empty to see own live game");
        SubcommandData gccd = new SubcommandData("gamecount", "counts ranked game since last weekly toplist");
        gccd.addOption(OptionType.STRING, "summoner-name", "name of player, empty to see own game count");
        SubcommandData clashcd = new SubcommandData("clash", "shows clash information");
        clashcd.addOption(OptionType.STRING, "summoner-name", "name of player, empty to see own data");
        SubcommandData setsumcd = new SubcommandData("setsummoner", "sets own summoner name");
        setsumcd.addOption(OptionType.STRING, "summoner-name", "name of player, empty to see own data");
        cd.addSubcommands(rankcd, livecd, gccd, clashcd, setsumcd);
        return cd;
    }

    @Override
    public void handle(@NotNull SlashCommandInteractionEvent ev) {
        try {
            handleLOLVolatile(ev);
        } catch (NotFoundException e) {
            ev.reply(DisiBot.getString("league_summoner_name_not_found")).queue();
            LOG.error("Could not find summoner name because of " + e.getMessage());
        } catch (InvalidAPIKeyException e) {
            ev.reply(DisiBot.getString("league_api_key_expired")).queue();
            LOG.error("API key is expired:" + e.getMessage());
        } catch (LeagueAPIException | IOException e) {
            ev.reply(DisiBot.getString("league_api_error")).queue();
            LOG.error("There was an error accessing the league of legends api:" + e.getMessage());
            e.printStackTrace();
        }
    }

    public void handleLOLVolatile(@NotNull SlashCommandInteractionEvent ev) throws LeagueAPIException, IOException {
        String subcommandName = ev.getSubcommandName();
        if (subcommandName == null) {
            ev.reply("Internal Error").queue();
            return;
        }
        switch (subcommandName) {
            case "live":
                handleLOLLive(ev);
                break;
            case "rank":
                handleLOLRank(ev);
                break;
            case "setsummoner":
                handleLOLSetSummoner(ev);
                break;
            case "clash":
                handleLOLClash(ev);
                break;
            case "gamecount":
                handleLOLGameCount(ev);
                break;
        }
    }

    private void handleLOLClash(@NotNull SlashCommandInteractionEvent ev) throws IOException, LeagueAPIException {
        try {
            LeagueSummoner summoner = getSummoner(ev);
            if (summoner == null) return;
            List<ClashTeamMember> clashTeams = summoner.getClashTeams();
            if (clashTeams.size() == 0) {
                ev.reply("Player is not in a clash team.").queue(); // todo stringify
                return;
            }
            List<ClashTeamMember> players = clashTeams.get(0).getTeam().getPlayers();
            StringBuilder url = new StringBuilder("https://euw.op.gg/multi/query=");
            for (Position position : Position.values()) {
                for (ClashTeamMember player : players) {
                    if (position.equals(player.getPosition())) {
                        url.append(player.getSummoner().getName().replaceAll(" ", "%20")).append("%2C");
                    }
                }
            }
            ev.reply(url.toString()).queue();
        } catch (NotFoundException e) {
            ev.reply(DisiBot.getString("league_summoner_name_not_found")).queue();
            LOG.error("Could not find summoner name because of " + e.getMessage());
        }
    }

    private void handleLOLLive(@NotNull SlashCommandInteractionEvent ev) throws LeagueAPIException, IOException {
        try {
            LeagueSummoner summoner = getSummoner(ev);
            if (summoner == null) return;
            CurrentGameInfo currentGame = summoner.getCurrentGame();
            if (currentGame == null) {
                ev.reply(String.format(DisiBot.getString("league_not_in_game"), summoner.getName())).queue();
                return;
            }
            List<MessageEmbed.Field> team1 = new ArrayList<>();
            List<MessageEmbed.Field> team2 = new ArrayList<>();
            for (CurrentGameParticipant participant : currentGame.getParticipants()) {
                SummonerQueueInfo soloQ = participant.getSummoner().getSoloQueueInfo();
                String text = participant.getChampion().getName();
                if (soloQ != null) {
                    text += " - " + soloQ.getTier().getDisplayName().charAt(0);
                    if (soloQ.getTier().equals(GRANDMASTER)) text += 'M';
                    if (!Arrays.asList(new LeagueTier[]{MASTER, GRANDMASTER, CHALLENGER}).contains(soloQ.getTier()))
                        text += soloQ.getDivisionInt();
                }
                MessageEmbed.Field field = new MessageEmbed.Field(participant.getSummonerName(), text, true);
                if (participant.getTeam() == RED) {
                    team1.add(field);
                } else {
                    team2.add(field);
                }
            }
            List<MessageEmbed.Field> fields = new ArrayList<>(team1);
            fields.add(new MessageEmbed.Field("", "", false)); //
            fields.addAll(team2);
            MessageEmbed embed = new MessageEmbed("https://euw.op.gg/summoner/userName=" + summoner.getName(),
                    DisiBot.getString("league_live_game"), null, RICH, currentGame.getStartTime().atOffset(ZoneOffset.UTC), 0x5383e8, null, null,
                    null, null,
                    new MessageEmbed.Footer(currentGame.getGameType() + " " + currentGame.getGameMode() + " " + currentGame.getRegion().name(), null, null),
                    null, fields);
            ev.replyEmbeds(embed).queue();
        } catch (NotFoundException e) {
            ev.reply(DisiBot.getString("league_summoner_name_not_found")).queue();
            LOG.error("Could not find summoner name because of " + e.getMessage());
        }
    }

    private void handleLOLSetSummoner(@NotNull SlashCommandInteractionEvent ev) {
        OptionMapping om = ev.getOption("summoner-name");

        if (om == null || om.getAsString().equals("")) {
            rankedStorage.removeUsers(ev.getUser());
            ev.reply(String.format(DisiBot.getString("league_unlinked_summoner"), ev.getUser().getName())).queue();
            return;
        }
        String summonerName = om.getAsString();
        LeagueSummoner summoner;
        try {
            summoner = riotAPI.getSummonerByName(summonerName);
        } catch (IOException | LeagueAPIException e) {
            ev.reply(String.format(DisiBot.getString("league_summoner_name_not_found_named"), summonerName)).queue();
            LOG.error("Could not find summoner name:" + summonerName + " because of " + e.getMessage());
            return;
        }
        rankedStorage.putUsersSummoner(ev.getUser(), summoner);
        ev.reply(String.format(DisiBot.getString("league_linked_summoner"), ev.getUser().getName(), summoner.getName())).queue();
    }

    private void handleLOLGameCount(@NotNull SlashCommandInteractionEvent ev) throws IOException, LeagueAPIException {
        LeagueSummoner summoner = getSummoner(ev);
        if (summoner == null) return;
        SummonerQueueInfo newInfo = summoner.getSoloQueueInfo();
        if (newInfo == null) {
            ev.reply(String.format(DisiBot.getString("league_summoner_not_ranked_named"), summoner.getName())).queue();
            return;
        }
        List<SummonerQueueInfo> oldInfos = rankedStorage.getSnapshotData();
        LocalDateTime lastTime = rankedStorage.getLastSnapshotTime();
        SummonerQueueInfo oldInfo = null;
        for (SummonerQueueInfo queueInfo : oldInfos) {
            if (queueInfo == null) continue;
            if (summoner.equals(queueInfo.getSummoner())) {
                oldInfo = queueInfo;
                break;
            }
        }
        if (oldInfo == null) {
            ev.reply(DisiBot.getString("league_summoner_games_not_counted")).queue();
            return;
        }
        int games = newInfo.getGameNumber() - oldInfo.getGameNumber();
        String lastTimeStr = "the big bang";
        if (lastTime != null)
            lastTimeStr = lastTime.format(DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM));
        ev.reply(String.format("%s has played %d games since %s", summoner.getName(), games, lastTimeStr)).queue();
        //ev.reply(String.format(DisiBot.getString("league_games_since_last_thursday"), summoner.getName(), games)).queue();
    }

    private void handleLOLRank(@NotNull SlashCommandInteractionEvent ev) throws IOException, LeagueAPIException {
        LeagueSummoner summoner = getSummoner(ev);
        if (summoner == null) {
            ev.reply("Please specify a summoner name or link it with `/lol setsummoner`").queue();
            return;
        }
        SummonerQueueInfo info = summoner.getSoloQueueInfo();
        if (info == null) {
            ev.reply(String.format(DisiBot.getString("league_summoner_not_ranked_named"), summoner.getName())).queue();
        } else {
            ev.reply(String.format(DisiBot.getString("league_rank"), summoner.getName(), info.getTierAndDivision(), info.getLeaguePoints())).queue();
        }
    }

    @Nullable
    private LeagueSummoner getSummoner(@NotNull SlashCommandInteractionEvent ev) throws IOException, LeagueAPIException {
        String summonerName;
        OptionMapping om = ev.getOption("summoner-name");
        if (om == null || om.getAsString().trim().equals("")) {
            if (rankedStorage.getUserSummonerMap().containsKey(ev.getUser())) {
                summonerName = rankedStorage.getUserSummonerMap().get(ev.getUser()).getName();
            } else {
                ev.reply(DisiBot.getString("league_no_summoner_name")).queue();
                return null;
            }
        } else {
            summonerName = om.getAsString();
        }
        return riotAPI.getSummonerByName(summonerName);
    }

}
