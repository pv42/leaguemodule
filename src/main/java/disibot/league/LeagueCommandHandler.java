package disibot.league;

import disibot.DisiBot;
import disibot.command.CommandHandler;
import disibot.context.ContextStorage;
import disibot.league.api.RiotAPI;
import disibot.league.api.entities.ClashTeamMember;
import disibot.league.api.entities.CurrentGameInfo;
import disibot.league.api.entities.CurrentGameParticipant;
import disibot.league.api.entities.LeagueSummoner;
import disibot.league.api.entities.LeagueTier;
import disibot.league.api.entities.Position;
import disibot.league.api.entities.SummonerQueueInfo;
import disibot.league.api.exceptions.InvalidAPIKeyException;
import disibot.league.api.exceptions.LeagueAPIException;
import disibot.league.api.exceptions.NotFoundException;
import disibot.util.Command;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.IOException;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static disibot.league.api.entities.LeagueTier.CHALLENGER;
import static disibot.league.api.entities.LeagueTier.GRANDMASTER;
import static disibot.league.api.entities.LeagueTier.MASTER;
import static disibot.league.api.entities.Team.RED;
import static net.dv8tion.jda.api.entities.EmbedType.RICH;


/*
DisiBot isn’t endorsed by Riot Games and doesn’t reflect the views or opinions of Riot Games
or anyone officially involved in producing or managing League of Legends. League of Legends and Riot Games are
trademarks or registered trademarks of Riot Games, Inc. League of Legends © Riot Games, Inc.
 */

public class LeagueCommandHandler extends CommandHandler {
    private static final Logger LOG = LoggerFactory.getLogger(LeagueCommandHandler.class);
    private static boolean TFT_DISABLED = true; // not final to allow test to change it to true
    private final RiotAPI riotAPI;
    private final SummonerRankedStorage rankedStorage;

    /**
     * Creates league a command handler
     *
     * @param contexts private message server contexts
     */
    public LeagueCommandHandler(ContextStorage contexts, JDA jda, RiotAPI riotAPI, SummonerRankedStorage rankedStorage) {
        super(contexts);
        this.riotAPI = riotAPI;
        this.rankedStorage = rankedStorage;
    }

    @Override
    public boolean canHandleCommand(@Nonnull Command command, @Nonnull MessageReceivedEvent ev) {
        return command.peekString().equals("lol") || command.peekString().equals("opgg") || command.peekString().equals("tft");
    }

    @Override
    protected void handle(@Nonnull Command command) {
        switch (command.nextString()) {
            case "opgg":
                handleOPGG(command);
                break;
            case "lol":
                handleLOL(command);
                break;
            case "tft":
                if (TFT_DISABLED) {
                    reply(DisiBot.getString("league_tft_is_disabled"));
                } else {
                    handleTFT(command);
                }
                break;
        }
    }

    private void handleOPGG(@Nonnull Command command) {
        if (command.hasNext()) {
            reply("https://euw.op.gg/summoner/userName=" + command.nextString());
        } else {
            if (rankedStorage.getUserSummonerMap().containsKey(getSenderAsUser())) {
                reply("https://euw.op.gg/summoner/userName=" + rankedStorage.getUserSummonerMap().get(getSenderAsUser()).getName());
            } else {
                reply(DisiBot.getString("league_opgg_usage"));
            }
        }
    }

    @Nullable
    private LeagueSummoner getSummoner(@Nonnull Command command) throws IOException, LeagueAPIException {
        String summonerName = command.nextString();
        if (summonerName.equals("")) {
            if (rankedStorage.getUserSummonerMap().containsKey(getSenderAsUser())) {
                summonerName = rankedStorage.getUserSummonerMap().get(getSenderAsUser()).getName();
            } else {
                reply(DisiBot.getString("league_no_summoner_name"));
                return null;
            }
        }
        return riotAPI.getSummonerByName(summonerName);
    }

    private void handleTFT(@Nonnull Command command) {
        String sub = command.nextString();
        if (sub.equals("rank")) {
            try {
                LeagueSummoner summoner = getSummoner(command);
                if (summoner == null) return;
                SummonerQueueInfo info = summoner.getTFTQueueInfo();
                if (info == null) {
                    reply(DisiBot.getString("league_no_tft_rank"));
                } else {
                    reply(String.format(DisiBot.getString("league_rank"), summoner.getName(), info.getTierAndDivision(), info.getLeaguePoints()));
                }
            } catch (NotFoundException e) {
                reply(DisiBot.getString("league_summoner_name_not_found"));
                LOG.error("Could not find summoner name  because of " + e.getMessage());
            } catch (InvalidAPIKeyException e) {
                reply(DisiBot.getString("league_api_key_expired"));
                LOG.error("API key is expired:" + e.getMessage());
            } catch (LeagueAPIException | IOException e) {
                reply(DisiBot.getString("league_api_error"));
                LOG.error("There was an error accessing the league of legends api:" + e.getMessage());
                e.printStackTrace();
            }
        } else {
            reply(DisiBot.getString("league_tft_usage"));
        }
    }

    private void handleLOL(@Nonnull Command command) {
        try {
            handleLOLVolatile(command);
        } catch (NotFoundException e) {
            reply(DisiBot.getString("league_summoner_name_not_found"));
            LOG.error("Could not find summoner name because of " + e.getMessage());
        } catch (InvalidAPIKeyException e) {
            reply(DisiBot.getString("league_api_key_expired"));
            LOG.error("API key is expired:" + e.getMessage());
        } catch (LeagueAPIException | IOException e) {
            reply(DisiBot.getString("league_api_error"));
            LOG.error("There was an error accessing the league of legends api:" + e.getMessage());
            e.printStackTrace();
        }
    }

    private void handleLOLVolatile(@Nonnull Command command) throws IOException, LeagueAPIException {
        String sub = command.nextString();
        if (sub.equals("riot")) {
            reply(DisiBot.getString("league_riot_legal"));
        } else if (sub.equals("help")) {
            reply(DisiBot.getString("league_lol_help"));
        } else if (sub.equalsIgnoreCase("setsummoner")) {
            handleLOLSetSummoner(command);
        } else if (sub.equals("rank")) {
            reply("Use `/lol rank` instead");
            //handleLOLRank(command);
        } else if (sub.equals("games")) {
            handleLOLGameCount(command);
        } else if (sub.equals("live")) {
            reply("Use `/lol live` instead");
            //handleLOLLive(command);
        } else if (sub.equals("clash")) {
            reply("Use `/lol clash` instead");
            //handleLOLClash(command);
        } else {
            reply(DisiBot.getString("league_lol_syntax"));
        }
    }

    private void handleLOLClash(Command command) throws IOException, LeagueAPIException {
        try {
            LeagueSummoner summoner = getSummoner(command);
            if (summoner == null) return;
            List<ClashTeamMember> clashTeams = summoner.getClashTeams();
            if(clashTeams.size() == 0) {
                reply("Player is not in a clash team."); // todo stringify
                return;
            }
            List<ClashTeamMember> players = clashTeams.get(0).getTeam().getPlayers();
            StringBuilder url = new StringBuilder("https://euw.op.gg/multi/query=");
            for(Position position: Position.values()) {
                for(ClashTeamMember player: players) {
                    if(position.equals(player.getPosition())) {
                        url.append(player.getSummoner().getName().replaceAll(" ","%20")).append("%2C");
                    }
                }
            }
            reply(url.toString());
        } catch (NotFoundException e) {
            reply(DisiBot.getString("league_summoner_name_not_found"));
            LOG.error("Could not find summoner name because of " + e.getMessage());
        }
    }


    private void handleLOLLive(Command command) throws LeagueAPIException, IOException {
        try {
            LeagueSummoner summoner = getSummoner(command);
            if (summoner == null) return;
            CurrentGameInfo currentGame = summoner.getCurrentGame();
            if (currentGame == null) {
                reply(String.format(DisiBot.getString("league_not_in_game"), summoner.getName()));
                return;
            }
            List<MessageEmbed.Field> team1 = new ArrayList<>();
            List<MessageEmbed.Field> team2 = new ArrayList<>();
            for (CurrentGameParticipant participant : currentGame.getParticipants()) {
                SummonerQueueInfo soloQ = participant.getSummoner().getSoloQueueInfo();
                String text = participant.getChampion().getName();
                if (soloQ != null) {
                    text += " - " + soloQ.getTier().getDisplayName().charAt(0);
                    if (soloQ.getTier().equals(GRANDMASTER)) text += 'M';
                    if (!Arrays.asList(new LeagueTier[]{MASTER, GRANDMASTER, CHALLENGER}).contains(soloQ.getTier()))
                        text += soloQ.getDivisionInt();
                }
                MessageEmbed.Field field = new MessageEmbed.Field(participant.getSummonerName(), text, true);
                if (participant.getTeam() == RED) {
                    team1.add(field);
                } else {
                    team2.add(field);
                }
            }
            List<MessageEmbed.Field> fields = new ArrayList<>(team1);
            fields.add(new MessageEmbed.Field("", "", false)); //
            fields.addAll(team2);
            MessageEmbed embed = new MessageEmbed("https://euw.op.gg/summoner/userName=" + summoner.getName(),
                    DisiBot.getString("league_live_game"), null, RICH, currentGame.getStartTime().atOffset(ZoneOffset.UTC), 0x5383e8, null, null,
                    null, null,
                    new MessageEmbed.Footer(currentGame.getGameType() + " " + currentGame.getGameMode() + " " + currentGame.getRegion().name(), null, null),
                    null, fields);
            reply(embed);
        } catch (NotFoundException e) {
            reply(DisiBot.getString("league_summoner_name_not_found"));
            LOG.error("Could not find summoner name because of " + e.getMessage());
        }
    }

    private void handleLOLSetSummoner(@Nonnull Command command) {
        LeagueSummoner summoner;
        if (!command.hasNext()) {
            rankedStorage.removeUsers(getSenderAsUser());
            reply(String.format(DisiBot.getString("league_unlinked_summoner"),getSenderAsUser().getName()));
            return;
        }
        String summonerName = command.nextString();
        try {
            summoner = riotAPI.getSummonerByName(summonerName);
        } catch (IOException | LeagueAPIException e) {
            reply(String.format(DisiBot.getString("league_summoner_name_not_found_named"), summonerName));
            LOG.error("Could not find summoner name:" + summonerName + " because of " + e.getMessage());
            return;
        }
        rankedStorage.putUsersSummoner(getSenderAsUser(), summoner);
        reply(String.format(DisiBot.getString("league_linked_summoner"), getSenderAsUser().getName(), summoner.getName()));
    }

    private void handleLOLGameCount(Command command) throws IOException, LeagueAPIException {
        LeagueSummoner summoner = getSummoner(command);
        if (summoner == null) return;
        SummonerQueueInfo newInfo = summoner.getSoloQueueInfo();
        if (newInfo == null) {
            reply(String.format(DisiBot.getString("league_summoner_not_ranked_named"), summoner.getName()));
            return;
        }
        List<SummonerQueueInfo> oldInfos = rankedStorage.getSnapshotData();
        SummonerQueueInfo oldInfo = null;
        for (SummonerQueueInfo queueInfo : oldInfos) {
            if (queueInfo == null) continue;
            if (summoner.equals(queueInfo.getSummoner())) {
                oldInfo = queueInfo;
                break;
            }
        }
        if (oldInfo == null) {
            reply(DisiBot.getString("league_summoner_games_not_counted"));
            return;
        }
        int games = newInfo.getGameNumber() - oldInfo.getGameNumber();
        reply(String.format(DisiBot.getString("league_games_since_last_thursday"),summoner.getName(),games));
    }

    private void handleLOLRank(Command command) throws IOException, LeagueAPIException {
        LeagueSummoner summoner = getSummoner(command);
        if (summoner == null) return;
        SummonerQueueInfo info = summoner.getSoloQueueInfo();
        if (info == null) {
            reply(String.format(DisiBot.getString("league_summoner_not_ranked_named"), summoner.getName()));
        } else {
            reply(String.format(DisiBot.getString("league_rank"), summoner.getName(), info.getTierAndDivision(), info.getLeaguePoints()));
        }
    }
}
