package disibot.league;

import disibot.context.ContextStorage;
import disibot.league.api.LeagueAPIBuilder;
import disibot.league.api.RiotAPI;
import disibot.league.api.exceptions.LeagueAPIException;
import disibot.modules.DisiBotModuleInterface;
import disibot.modules.DisiBotModule;
import net.dv8tion.jda.api.JDA;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import java.io.FileNotFoundException;
import java.io.IOException;

import static disibot.util.Util.readTokenFile;

public class LeagueModule extends DisiBotModule {
    private static final Logger LOG = LoggerFactory.getLogger(LeagueModule.class);
    private static final String ERR_API_KEY_FILE_NOT_FOUND = "Could not find api key file.";
    private static final String ERR_API_KEY_FILE_READ_ERROR = "Could not read api key file.";
    private final LeagueCommandHandler commandHandler;
    private final LeagueSlashCommandHandler slashCommandHandler;
    private final WeeklyTopListManager wtlManager;
    private LeagueConfig config;

    public LeagueModule(ContextStorage contextStorage, JDA jda) {
        RiotAPI riotAPI = createRiotApi();
        SummonerRankedStorage rankedStorage = SummonerRankedStorage.fromDisk(riotAPI, jda);
        commandHandler = new LeagueCommandHandler(contextStorage, jda, riotAPI, rankedStorage);
        wtlManager = new WeeklyTopListManager(rankedStorage, jda, this);
        slashCommandHandler = new LeagueSlashCommandHandler(contextStorage, jda, riotAPI, rankedStorage);
    }

    @Override
    public void load(@NotNull DisiBotModuleInterface moduleInterface) {
        moduleInterface.addCommandHandler(this, commandHandler);
        moduleInterface.addSlashCommandHandler(this, slashCommandHandler);
        wtlManager.setModuleInterface(moduleInterface);
        moduleInterface.addTask(this, wtlManager.createTask());
    }

    private static RiotAPI createRiotApi() {
        try {
            String apiKey = loadApiKey();
            if (apiKey == null) return null;
            return new LeagueAPIBuilder(apiKey).build();
        } catch (IOException e) {
            LOG.error("IOException while creating league api: " + e.getMessage());
        } catch (LeagueAPIException e) {
            LOG.error("League API could not be created: " + e.getMessage());
        }
        return null;
    }

    @Nullable
    private static String loadApiKey() {
        String API_KEY_FILE = "lol_api_key";
        String api_key;
        try {
            api_key = readTokenFile(API_KEY_FILE);
        } catch (FileNotFoundException e) {
            LOG.error(ERR_API_KEY_FILE_NOT_FOUND);
            return null;
        } catch (IOException e) {
            LOG.error(ERR_API_KEY_FILE_READ_ERROR);
            return null;
        }
        api_key = api_key.replaceFirst("\n", ""); //remove eventual newline at eof
        return api_key;
    }
}
