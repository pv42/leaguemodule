package disibot.league;

import disibot.config.ConfigSection;
import disibot.league.api.entities.LeagueSummoner;
import disibot.league.api.entities.SummonerQueueInfo;
import disibot.league.api.exceptions.LeagueAPIException;
import disibot.modules.DisiBotModule;
import disibot.modules.DisiBotModuleInterface;
import disibot.tasks.Task;
import disibot.util.SVGSender;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.entities.User;
import org.apache.batik.transcoder.TranscoderException;
import org.jetbrains.annotations.Contract;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.time.Clock;
import java.time.DayOfWeek;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.time.temporal.WeekFields;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class WeeklyTopListManager {
    private static final Logger LOG = LoggerFactory.getLogger(WeeklyTopListManager.class);
    private final JDA jda;
    private final SummonerRankedStorage storage;
    private final Clock clock;

    private DisiBotModuleInterface moduleInterface;
    private final DisiBotModule thisModule;

    public WeeklyTopListManager(@Nonnull SummonerRankedStorage storage, JDA jda, DisiBotModule thisModule) {
        this.storage = storage;
        this.jda = jda;
        this.thisModule = thisModule;
        clock = Clock.systemDefaultZone();
    }

    @Nonnull
    @Contract(pure = true)
    private static String getArrowAngle(int compareValue) {
        if (compareValue < 0) return "45";  // /^
        if (compareValue > 0) return "135"; // \v
        return "90"; // ->
    }

    @Nonnull
    @Contract(pure = true)
    private static String getArrowColor(int compareValue) {
        if (compareValue < 0) return "#4bb44b"; // green
        if (compareValue > 0) return "#9c3213"; // red
        return "#aaaaaa"; //grey
    }

    @Nonnull
    private static String getBadge(@Nonnull SummonerQueueInfo info) {
        return "file:///" + System.getProperty("user.dir").replaceAll("\\\\", "/") + "/res/Emblem_" + info.getTier().getDisplayName() + ".png";
    }

    @Nonnull
    @Contract("_ -> new")
    private static String loadSVGFile(String file) throws IOException {
        File svgFile = new File(file);
        FileInputStream in = new FileInputStream(svgFile);
        byte[] data = new byte[(int) svgFile.length()];
        int bytesRead = in.read(data);
        if (bytesRead != data.length) throw new IOException("Could not read whole file. (" + file + ")");
        return new String(data);
    }

    private LeagueConfig getConfig() {
        ConfigSection rawConfig = moduleInterface.getConfig(thisModule, LeagueConfig::new);
        return new LeagueConfig(rawConfig);
    }

    public Task createTask() {
        LeagueConfig config = getConfig();
        LocalDateTime phase = nextDayOfWeekWithHour(config.getWtlDay(), config.getWtlHour());
        return new Task("WeeklyTopListSenderTask", Period.ofWeeks(1), phase,
                this::sendWeeklySoloQRankings);
    }

    private LocalDateTime nextDayOfWeekWithHour(@Nonnull DayOfWeek dayOfWeek, int hour) {
        LocalDateTime now = LocalDateTime.now(clock);
        LocalDateTime dateTime = now.plus(Period.ofDays((7 + dayOfWeek.getValue() - now.getDayOfWeek().getValue()) % 7));
        if (now.getHour() < hour || now.getDayOfWeek() != dayOfWeek) {
            // if not on matching day and after time then subtract one extra week so the first deadline is less the 1
            // week in the future
            dateTime = dateTime.minus(Period.ofWeeks(1));
        }
        dateTime = dateTime.plus(Duration.ofHours(hour - dateTime.getHour())); // adjust hour
        dateTime = dateTime.minus(Duration.ofMinutes(dateTime.getMinute())); // set mins to 0
        dateTime = dateTime.minus(Duration.ofSeconds(dateTime.getSecond())); // set secs to 0
        return dateTime;
    }

    private void sendWeeklySoloQRankings() {
        for (Guild guild : jda.getGuilds()) {
            sendWeeklySoloQRanking(guild);
        }
    }

    public void sendWeeklySoloQRanking(Guild guild) {
        try {
            String mainSvg = loadSVGFile("res/weekly_ranking.svg");
            String partSvg = loadSVGFile("res/weekly_ranking_player.svg");
            sendWeeklySoloQRankingVolatile(guild, mainSvg, partSvg);
        } catch (IOException e) {
            LOG.error("IOException while sending a podium svg for server " + guild.getName() + ": " + e.getMessage());
        } catch (LeagueAPIException e) {
            LOG.error("Failed to create weekly soloQ ranking svg for server " + guild.getName() + " because of a league api error: " + e.getMessage());
        } catch (TranscoderException e) {
            LOG.error("Failed to encode svg for podium for server " + guild.getName() + ": " + e.getMessage());
        }
    }

    private void sendWeeklySoloQRankingVolatile(@Nonnull Guild guild, String mainSvgRaw, String partSvgRaw) throws TranscoderException, IOException, LeagueAPIException {
        LOG.info("weekly soloQ ranking for " + guild.getName());
        String svgFormatted;
        try {
            svgFormatted = formatSVG(mainSvgRaw, partSvgRaw, guild);
        } catch (EmptyEntryListException e) {
            LOG.info("weekly ranking for " + guild.getName() + " omitted. Reason: no entries");
            return;
        }
        MessageChannel channel = getWTLSendChannel(guild);
        if (channel != null) {
            SVGSender.send(svgFormatted, channel);
            LOG.info("weekly ranking for " + guild.getName() + " sent.");
        } else {
            LOG.info("Server " + guild.getName() + "does not seem to have a text channel with MESSAGE_WRITE permission, skipping emote podium");
        }
    }

    @Nullable
    private MessageChannel getWTLSendChannel(@Nonnull Guild guild) {
        MessageChannel channel = guild.getDefaultChannel();
        if (channel != null) return channel;
        LOG.info("Server " + guild.getName() + " does not have a default text channel, continue searching for valid channel.");
        for (TextChannel tc : guild.getTextChannels()) {
            Member member = guild.getMember(jda.getSelfUser());
            if (member == null) {
                LOG.warn("Bot was kicked from " + guild.getName() + " during wtl creation");
                continue;
            }
            if (member.hasPermission(tc, Permission.MESSAGE_SEND)) return tc;
        }
        return null;
    }

    @Nonnull
    private String formatSVG(String svg, String part, Guild guild) throws IOException, LeagueAPIException, EmptyEntryListException {
        LocalDateTime now = LocalDateTime.now(clock);
        WeekFields weekFields = WeekFields.of(Locale.getDefault());
        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("dd.MM.yyyy", Locale.getDefault());
        svg = svg.replaceFirst("\\[week]", String.valueOf(now.get(weekFields.weekOfYear())));
        svg = svg.replaceFirst("\\[year]", String.valueOf(now.getYear()));
        svg = svg.replaceFirst("\\[end_date]", dateFormatter.format(now));
        svg = svg.replaceFirst("\\[entries]", formatPlayerSvgs(part, guild));
        svg = svg.replaceFirst("\\[start_date]", dateFormatter.format(now.minusWeeks(1)));
        return svg;
    }

    @Nonnull
    private String formatPlayerSvgs(@Nonnull String part, @Nonnull Guild guild) throws IOException, LeagueAPIException, EmptyEntryListException {
        LeagueConfig config = getConfig();
        StringBuilder result = new StringBuilder();
        List<SummonerEntryData> entries = getSummonerEntryData(guild);
        if (entries.size() == 0 && !config.doSendWhenEmpty()) {
            throw new EmptyEntryListException();
        }
        for (int i = 0; i < entries.size(); i++) {
            result.append(formatPlayerSvg(part, entries.get(i), i));
        }
        storage.createSnapshot();
        return result.toString();
    }

    @Nonnull
    private List<SummonerEntryData> getSummonerEntryData(Guild guild) throws IOException, LeagueAPIException {
        LeagueConfig config = getConfig();
        List<SummonerQueueInfo> oldQueueInfos = storage.getSnapshotData();
        List<SummonerEntryData> entries = new ArrayList<>();
        for (User discordUser : storage.getUserSummonerMap().keySet()) {
            if (!guild.isMember(discordUser)) continue;
            LeagueSummoner summoner = storage.getUserSummonerMap().get(discordUser);
            if (summoner == null) {
                LOG.warn("User " + discordUser.getName() + " has an invalid lol summoner id assigned");
                continue;
            }
            SummonerQueueInfo newQueueInfo = summoner.getSoloQueueInfo();
            if (newQueueInfo == null) {
                LOG.warn("No soloQ info found for summoner " + summoner.getName());
                continue;
            }
            SummonerQueueInfo oldQueueInfo = null;
            for (SummonerQueueInfo queueInfo : oldQueueInfos) {
                if (queueInfo == null) continue;
                if (queueInfo.getSummoner().equals(summoner)) {
                    oldQueueInfo = queueInfo;
                    break;
                }
            }
            if (oldQueueInfo == null) {
                oldQueueInfo = SummonerQueueInfo.createEmptyQueueInfo(summoner);
            }
            if (!config.isHideInactivePlayers() || oldQueueInfo.getGameNumber() != newQueueInfo.getGameNumber()) {
                entries.add(new SummonerEntryData(summoner, discordUser, newQueueInfo, oldQueueInfo));
            }
        }
        entries.sort((t0, t1) -> {
            int score0 = t0.newQueueInfo.getWins() - t0.oldQueueInfo.getWins() - t0.newQueueInfo.getLosses() + t0.oldQueueInfo.getLosses();
            int score1 = t1.newQueueInfo.getWins() - t1.oldQueueInfo.getWins() - t1.newQueueInfo.getLosses() + t1.oldQueueInfo.getLosses();
            return -Integer.compare(score0, score1);
        });
        return entries;
    }

    @Nonnull
    private String formatPlayerSvg(String format, @Nonnull SummonerEntryData summoner, int y_index) throws IOException, LeagueAPIException {
        String entry = format;
        int wins = summoner.newQueueInfo.getWins() - summoner.oldQueueInfo.getWins();
        int losses = summoner.newQueueInfo.getLosses() - summoner.oldQueueInfo.getLosses();
        entry = entry.replaceFirst("\\[y_offset]", String.valueOf(85 + 45 * y_index)); //85,130 (85+45)
        entry = entry.replaceFirst("\\[summoner_icon]", summoner.summoner.getProfileIcon().getUrl());
        entry = entry.replaceFirst("\\[summoner_name]", summoner.summoner.getName());
        entry = entry.replaceFirst("\\[discord_handle]", summoner.discordUser.getAsTag());
        entry = entry.replaceFirst("\\[wins]", String.valueOf(wins));
        entry = entry.replaceFirst("\\[losses]", String.valueOf(losses));
        entry = entry.replaceFirst("\\[win_percent]", String.valueOf(Math.round(100f * (float) wins / (float) (wins + losses))));
        entry = entry.replaceFirst("\\[old_rank]", summoner.oldQueueInfo.getTierAndDivision());
        entry = entry.replaceFirst("\\[old_badge]", getBadge(summoner.oldQueueInfo));
        entry = entry.replaceFirst("\\[old_lp]", String.valueOf(summoner.oldQueueInfo.getLeaguePoints()));
        entry = entry.replaceFirst("\\[arrow_rotation]", getArrowAngle(summoner.oldQueueInfo.compareTo(summoner.newQueueInfo)));
        entry = entry.replaceFirst("\\[arrow_color]", getArrowColor(summoner.oldQueueInfo.compareTo(summoner.newQueueInfo)));
        entry = entry.replaceFirst("\\[new_rank]", summoner.newQueueInfo.getTierAndDivision());
        entry = entry.replaceFirst("\\[new_badge]", getBadge(summoner.newQueueInfo));
        entry = entry.replaceFirst("\\[new_lp]", String.valueOf(summoner.newQueueInfo.getLeaguePoints()));
        return entry;
    }

    public void setModuleInterface(DisiBotModuleInterface moduleInterface) {
        this.moduleInterface = moduleInterface;
    }

    private static class SummonerEntryData {
        LeagueSummoner summoner;
        User discordUser;
        SummonerQueueInfo newQueueInfo;
        SummonerQueueInfo oldQueueInfo;

        public SummonerEntryData(LeagueSummoner summoner, User discordUser, SummonerQueueInfo newQueueInfo, SummonerQueueInfo oldQueueInfo) {
            this.summoner = summoner;
            this.discordUser = discordUser;
            this.newQueueInfo = newQueueInfo;
            this.oldQueueInfo = oldQueueInfo;
        }
    }

    private static class EmptyEntryListException extends Exception {}
}
