package disibot.league;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import disibot.league.api.RiotAPI;
import disibot.league.api.entities.LeagueSummoner;
import disibot.league.api.exceptions.LeagueAPIException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class LeagueSummonerAdapter extends TypeAdapter<LeagueSummoner> {
    private static final Logger LOG = LoggerFactory.getLogger(LeagueSummonerAdapter.class);
    private final RiotAPI api;

    public LeagueSummonerAdapter(RiotAPI api) {
        this.api = api;
    }

    @Override
    public void write(JsonWriter out, LeagueSummoner value) throws IOException {
        out.value(value.getId());
    }

    @Override
    public LeagueSummoner read(JsonReader in) throws IOException {
        try {
            return api.getSummonerById(in.nextString());
        } catch (LeagueAPIException e) {
            LOG.warn("Could not resolve summoner: " +  e.getMessage());
            return null;
        }
    }
}
