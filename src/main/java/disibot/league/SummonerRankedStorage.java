package disibot.league;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import disibot.league.api.RiotAPI;
import disibot.league.api.entities.LeagueSummoner;
import disibot.league.api.entities.SummonerQueueInfo;
import disibot.league.api.exceptions.LeagueAPIException;
import disibot.league.api.impl.RiotAPIImpl;
import disibot.util.UserAdapter;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.entities.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SummonerRankedStorage {
    private static final Logger LOG = LoggerFactory.getLogger(SummonerRankedStorage.class);
    private static final String fileName = "summoner_ranked_storage.json";
    private final List<SummonerRankedSnapshot> snapshots;
    private final Map<User, LeagueSummoner> userSummonerMap;
    private transient RiotAPI riotApi;
    private transient JDA jda;

    private SummonerRankedStorage(RiotAPI riotAPI, JDA jda) {
        this.riotApi = riotAPI;
        this.jda = jda;
        userSummonerMap = new HashMap<>();
        snapshots = new ArrayList<>();
    }

    @Nonnull
    public static SummonerRankedStorage fromDisk(RiotAPI riotApi, JDA jda) {
        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapter(LeagueSummoner.class, new LeagueSummonerAdapter(riotApi));
        builder.registerTypeHierarchyAdapter(User.class, new UserAdapter(jda));
        builder.enableComplexMapKeySerialization();
        Gson gson = builder.create();
        try {
            SummonerRankedStorage storage = gson.fromJson(new FileReader(fileName), SummonerRankedStorage.class);
            for (SummonerRankedSnapshot snapshot : storage.snapshots) {
                if (snapshot != null) {
                    for (SummonerQueueInfo info : snapshot.summonerQueueInfos) {
                        if (info != null) info.setAPI((RiotAPIImpl) riotApi); // todo
                    }
                } else {
                    LOG.warn("Snapshot is null");
                }
            }
            storage.jda = jda;
            storage.riotApi = riotApi;
            return storage;
        } catch (FileNotFoundException e) {
            LOG.info("ranked storage does not exist yet");
            return new SummonerRankedStorage(riotApi, jda);
        } catch (JsonSyntaxException e) {
            LOG.info("could not access api");
            return new SummonerRankedStorage(riotApi, jda);
        } catch (NumberFormatException | NullPointerException e) {
            LOG.error("Could not read storage file: " + e.getMessage());
            e.printStackTrace();
            return new SummonerRankedStorage(riotApi, jda);
        }
    }

    public void writeToDisk() {
        if (riotApi.isDisabled()) return;
        if (userSummonerMap.size() == 0 && snapshots.size() == 0) return;
        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapter(LeagueSummoner.class, new LeagueSummonerAdapter(riotApi));
        builder.registerTypeHierarchyAdapter(User.class, new UserAdapter(jda));
        builder.enableComplexMapKeySerialization();
        Gson gson = builder.create();
        try {
            FileWriter fw = new FileWriter(fileName);
            gson.toJson(this, fw);
            fw.close();
        } catch (IOException e) {
            LOG.error("ranked storage could not be written to disk");
        }
    }

    @Nonnull
    public List<SummonerQueueInfo> getSnapshotData() {
        SummonerRankedSnapshot snapshot = getLastSnapshot();
        if(snapshot == null) return new ArrayList<>();
        return snapshot.summonerQueueInfos;
    }

    @Nullable
    public LocalDateTime getLastSnapshotTime() {
        SummonerRankedSnapshot snapshot = getLastSnapshot();
        if(snapshot == null) return null;
        return snapshot.time;
    }

    @Nullable
    private SummonerRankedSnapshot getLastSnapshot() {
        LocalDateTime oneWeekAgo = LocalDateTime.now().minusWeeks(1);
        int beforeIndex = snapshots.size() - 1;
        if (beforeIndex == -1) return null; // no data
        while (beforeIndex > 0 && snapshots.get(beforeIndex).time.isAfter(oneWeekAgo)) {
            beforeIndex--;
        }
        int afterIndex = beforeIndex + 1;
        int index;
        if (afterIndex < snapshots.size()) {
            Duration beforeDuration = Duration.between(snapshots.get(beforeIndex).time, oneWeekAgo);
            Duration afterDuration = Duration.between(oneWeekAgo, snapshots.get(afterIndex).time);
            if (beforeDuration.getSeconds() < afterDuration.getSeconds()) {
                index = beforeIndex;
            } else {
                index = afterIndex;
            }
        } else {
            index = beforeIndex;
        }
        return snapshots.get(index);
    }

    public void createSnapshot() {
        SummonerRankedSnapshot snapshot = new SummonerRankedSnapshot();
        snapshot.summonerQueueInfos = new ArrayList<>();
        try {
            for (LeagueSummoner summoner : userSummonerMap.values()) {
                snapshot.summonerQueueInfos.add(summoner.getSoloQueueInfo());
            }
        } catch (IOException | LeagueAPIException e) {
            LOG.error("Could not create snapshot: " + e.getMessage());
            e.printStackTrace();
        }
        snapshot.time = LocalDateTime.now();
        LOG.info("Created snapshots with " + userSummonerMap.size() + " summoners");
        snapshots.add(snapshot);
        writeToDisk();
    }

    public Map<User, LeagueSummoner> getUserSummonerMap() {
        return userSummonerMap;
    }

    public void putUsersSummoner(User user, LeagueSummoner summoner) {
        userSummonerMap.put(user, summoner);
        createSnapshot();
    }

    public void removeUsers(User user) {
        userSummonerMap.remove(user);
        writeToDisk();
    }

    static class SummonerRankedSnapshot {
        private List<SummonerQueueInfo> summonerQueueInfos;
        private LocalDateTime time;
    }
}
