package disibot.league;

import disibot.league.api.RiotAPI;
import disibot.league.api.entities.CurrentGameInfo;
import disibot.league.api.entities.CurrentGameParticipant;
import disibot.league.api.entities.LeagueSummoner;
import disibot.league.api.exceptions.LeagueAPIException;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.OnlineStatus;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

public class DamageGraphSender extends Thread {
    public boolean stop;
    private JDA jda;
    private SummonerRankedStorage rankedStorage;
    private Set<CurrentGameInfo> watchedGames = new HashSet<>();
    private RiotAPI api;

    public DamageGraphSender(JDA jda, RiotAPI api) {
        this.jda = jda;
        this.api = api;
    }

    @Override
    public void run() {
        int i = 0;
        while (!stop) {
            if(i % 10 == 0) lookForGames();
            checkWatchedGames();
            i++;
        }
    }

    private void checkWatchedGames() {
        for(CurrentGameInfo gameInfo: watchedGames) {
            try {
                if(!gameInfo.equals(gameInfo.getParticipants().get(0).getSummoner().getCurrentGame())) {
                    int knownSummonerCount = 0;
                    for(CurrentGameParticipant participant: gameInfo.getParticipants()) {
                        if(rankedStorage.getUserSummonerMap().containsValue(participant.getSummoner())) {
                            knownSummonerCount ++;
                        }
                    }
                    if(knownSummonerCount >= 2) {
                        // TODO send image
                    }
                    watchedGames.remove(gameInfo);
                }
            } catch (IOException | LeagueAPIException e) {
                e.printStackTrace();
                watchedGames.remove(gameInfo);
            }
        }
    }

    private void lookForGames() {
        for (Guild guild : jda.getGuilds()) {
            for (Member member : guild.getMembers()) {
                CurrentGameInfo info = getGameFromUser(member);
                if(info!=null) watchedGames.add(info);
            }
        }
    }

    private CurrentGameInfo getGameFromUser(Member member) {
        if (member.getOnlineStatus().equals(OnlineStatus.ONLINE)) {
            LeagueSummoner summoner = rankedStorage.getUserSummonerMap().get(member.getUser());
            if (summoner != null) {
                CurrentGameInfo game;
                try {
                    game = summoner.getCurrentGame();
                } catch (IOException | LeagueAPIException e) {
                    game = null;
                }
                return game;
            }
        }
        return null;
    }

    public void shutdown() {
        stop = true;
    }
}
