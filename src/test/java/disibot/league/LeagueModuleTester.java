package disibot.league;

import disibot.modules.DisiBotModuleInterface;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class LeagueModuleTester {
    @Test
    void testConstructorAndLoad() throws IOException {
        // create dummy api key file
        File f = new File("lol_api_key");
        FileOutputStream fout = new FileOutputStream(f);
        fout.write("test_api_key".getBytes());
        fout.close();
        // test
        LeagueModule module = new LeagueModule(null, null);
        DisiBotModuleInterface moduleInterface = mock(DisiBotModuleInterface.class);
        final boolean[] ok = {false, false};
        doAnswer(invocation -> {
            ok[0] = true;
            return null;
        }).when(moduleInterface).addCommandHandler(eq(module), any(LeagueCommandHandler.class));
        doAnswer(invocation -> {
            ok[1] = true;
            return null;
        }).when(moduleInterface).addTask(eq(module), any());
            // when(moduleInterface).addCommandHandler(any(), any());
        when(moduleInterface.getConfig(eq(module), any())).thenReturn(new LeagueConfig());
        module.load(moduleInterface);
        assertTrue(ok[0]);
        assertTrue(ok[1]);
        // del api key
        Files.delete(Path.of("lol_api_key"));
    }
}
