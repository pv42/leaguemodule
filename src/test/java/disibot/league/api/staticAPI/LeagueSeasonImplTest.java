package disibot.league.api.staticAPI;

import org.junit.jupiter.api.Test;

import java.lang.reflect.Field;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

public class LeagueSeasonImplTest {
    @Test
    void testEquals() throws NoSuchFieldException, IllegalAccessException {
        LeagueSeasonImpl season = new LeagueSeasonImpl();
        LeagueSeasonImpl seasonSame = new LeagueSeasonImpl();
        LeagueSeasonImpl seasonDiff = new LeagueSeasonImpl();
        assertEquals(season, season);
        assertNotEquals(season, null);
        assertNotEquals(season, "a random not season object");
        Field idField = LeagueSeasonImpl.class.getDeclaredField("id");
        idField.setAccessible(true);
        idField.set(season,10);
        idField.set(seasonSame,10);
        idField.set(seasonDiff,5);
        assertEquals(season, seasonSame);
        assertNotEquals(season, seasonDiff);
    }
}
