package disibot.league.api;

import disibot.league.api.exceptions.LeagueAPIException;
import disibot.league.api.staticAPI.LeagueQueue;
import disibot.league.api.staticAPI.LeagueSeason;
import disibot.league.api.staticAPI.LeagueSeasonImpl;
import disibot.league.api.staticAPI.StaticAPI;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

public class StaticAPITest {
    private StaticAPI api;

    @BeforeEach
    void prepare() throws IOException, LeagueAPIException {
        api = new StaticAPI(false,false);
    }

    @Test
    void testGetQueue() {
        try {
            List<LeagueQueue> queues = api.getQueues();
            assertTrue(79 <= queues.size());
            assertTrue(100 >= queues.size());
            LeagueQueue q3 = queues.get(3);
            assertEquals(6, q3.getId());
            assertEquals("Summoner's Rift", q3.getMap());
        } catch (IOException | LeagueAPIException e) {
            fail(e);
        }
    }

    @Test
    void testGetSeasons() {
        try {
            List<LeagueSeasonImpl> seasons = api.getSeasons();
            assertEquals(14, seasons.size());
            LeagueSeason s13 = seasons.get(13);
            assertEquals("SEASON 2019", s13.getName());
            assertEquals(13, s13.getId());
        } catch (IOException | LeagueAPIException e) {
            fail(e);
        }
    }
}
