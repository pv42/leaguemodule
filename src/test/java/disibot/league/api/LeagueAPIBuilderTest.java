package disibot.league.api;

import disibot.league.api.exceptions.LeagueAPIException;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertThrows;

public class LeagueAPIBuilderTest {
    @Test
    void testNoAPIKey() throws IOException, LeagueAPIException {
        LeagueAPIBuilder builder = new LeagueAPIBuilder();
        assertThrows(IllegalStateException.class, builder::build);
    }

    @Test
    void testNoDDVR() throws IOException, LeagueAPIException {
        LeagueAPIBuilder builder = new LeagueAPIBuilder();
        builder.setDataDragonVersionRegion(null);
        assertThrows(IllegalStateException.class, builder::build);
    }
}
