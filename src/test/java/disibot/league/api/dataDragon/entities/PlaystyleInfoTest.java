package disibot.league.api.dataDragon.entities;

import disibot.league.api.dataDragon.entities.LeaguePlaystyleInfo;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class PlaystyleInfoTest {
    private static LeaguePlaystyleInfo info = new LeaguePlaystyleInfo(1,2,3,4);

    @Test
    void testGetters() {
        assertEquals(1, info.getDifficulty());
        assertEquals(2, info.getAttack());
        assertEquals(3, info.getMagic());
        assertEquals(4, info.getDefense());
    }

    @Test
    void testEquals() {
        assertEquals(info, info);
        //noinspection ConstantConditions,SimplifiableJUnitAssertion
        assertFalse(info.equals(null));
        assertNotEquals(info, new Object());
        assertEquals(info, new LeaguePlaystyleInfo(1,2,3,4));
        assertNotEquals(info, new LeaguePlaystyleInfo(2,2,3,4));
        assertNotEquals(info, new LeaguePlaystyleInfo(1,10,3,4));
        assertNotEquals(info, new LeaguePlaystyleInfo(1,2,1,4));
        assertNotEquals(info, new LeaguePlaystyleInfo(1,2,3,2));
    }

    @Test
    void testHashCode() {
        assertEquals(31810, info.hashCode());
    }

}
