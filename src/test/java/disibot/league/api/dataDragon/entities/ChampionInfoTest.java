package disibot.league.api.dataDragon.entities;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class ChampionInfoTest {
    private static List<String> tags = Arrays.asList("Mage","Flower");
    private static ChampionStats stats = new ChampionStats();
    private static LeaguePlaystyleInfo playstyle = new LeaguePlaystyleInfo(2,3,4,2);
    private static LeagueImageInfo imageInfo = new LeagueImageInfo("a","b", "c",0,1,2,3);
    private static Champion championInfo = getChampionInfo();

    private static Champion getChampionInfo() {
        return new Champion(99, "Zyra", "Zy'ra", new Version("10.2.1"),"The throns embrace", "some lore ...", "Mana", tags,playstyle,imageInfo,stats);
    }

    // functions with one attribute modified
    private static Champion getChampionInfoMKey() {
        return new Champion(100, "Zyra", "Zy'ra", new Version("10.2.1"),"The throns embrace", "some lore ...", "Mana", tags,playstyle,imageInfo,stats);
    }

    private static Champion getChampionInfoMId() {
        return new Champion(99, "Zy'ra", "Zy'ra", new Version("10.2.1"),"The throns embrace", "some lore ...", "Mana", tags,playstyle,imageInfo,stats);
    }

    private static Champion getChampionInfoMVersion() {
        return new Champion(99, "Zyra", "Zy'ra", new Version("10.2.2"),"The throns embrace", "some lore ...", "Mana", tags,playstyle,imageInfo,stats);
    }



    @Test
    void testGetters() {
        assertEquals(imageInfo, championInfo.getImage());
        assertEquals(99, championInfo.getKey());
        assertEquals("some lore ...", championInfo.getLoreShort());
        assertEquals("Zy'ra", championInfo.getName());
        assertEquals("Zyra", championInfo.getNameId());
        assertEquals(playstyle, championInfo.getPlaystyleInfo());
        assertEquals("Mana", championInfo.getSecondaryBar());
        assertEquals(stats, championInfo.getStats());
        assertEquals(tags, championInfo.getTags());
        assertEquals("The throns embrace", championInfo.getTitle());
        assertEquals(new Version("10.2.1"), championInfo.getVersion());
    }

    @Test
    void testEquals() {
        assertEquals(championInfo, championInfo);
        //noinspection SimplifiableJUnitAssertion,ConstantConditions
        assertFalse(championInfo.equals(null));
        assertNotEquals(championInfo, new Object());
        assertEquals(championInfo, getChampionInfo());
        assertNotEquals(getChampionInfoMId(), championInfo);
        assertNotEquals(getChampionInfoMKey(), championInfo);
        assertNotEquals(getChampionInfoMVersion(), championInfo);
    }

    @Test
    void testHashCode() {
        assertEquals(97396638, championInfo.hashCode());
    }
}
