package disibot.league.api.dataDragon.entities;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import static org.junit.jupiter.api.Assertions.assertThrows;

public class LeagueItemTest {
    @Test
    void testIdOverride() {
        LeagueItem item = new LeagueItem();
        item.setId(0);
        assertThrows(UnsupportedOperationException.class, new Executable() {
            @Override
            public void execute() throws Throwable {
                item.setId(9);
            }
        });
    }
}
