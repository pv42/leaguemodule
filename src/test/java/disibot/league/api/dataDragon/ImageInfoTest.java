package disibot.league.api.dataDragon;

import disibot.league.api.dataDragon.entities.LeagueImageInfo;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ImageInfoTest {
    private static LeagueImageInfo imageInfo;

    @BeforeAll
    static void setUp() {
        imageInfo = new LeagueImageInfo("Zyra.png", "champion8.png", "champion", 0, 48, 48, 48);
    }

    @Test
    void testEquals() {
        LeagueImageInfo other = new LeagueImageInfo("Zyra.png", "champion8.png", "champion", 0, 48, 48, 48);
        //noinspection ConstantConditions,SimplifiableJUnitAssertion
        assertFalse(imageInfo.equals(null));
        assertNotEquals(imageInfo, new Object());
        assertEquals(imageInfo, imageInfo);
        assertEquals(imageInfo, other);
        other = new LeagueImageInfo("Xayah.png", "champion8.png", "champion", 0, 48, 48, 48);
        assertNotEquals(other, imageInfo);
        other = new LeagueImageInfo("Zyra.png", "champion9.png", "champion", 0, 48, 48, 48);
        assertNotEquals(other, imageInfo);
        other = new LeagueImageInfo("Zyra.png", "champion8.png", "champions", 0, 48, 48, 48);
        assertNotEquals(other, imageInfo);
        other = new LeagueImageInfo("Zyra.png", "champion8.png", "champion", 48, 48, 48, 48);
        assertNotEquals(other, imageInfo);
        other = new LeagueImageInfo("Zyra.png", "champion8.png", "champion", 0, 0, 48, 48);
        assertNotEquals(other, imageInfo);
        other = new LeagueImageInfo("Zyra.png", "champion8.png", "champion", 0, 48, 32, 48);
        assertNotEquals(other, imageInfo);
        other = new LeagueImageInfo("Zyra.png", "champion8.png", "champion", 0, 48, 48, 32);
        assertNotEquals(other, imageInfo);
    }

    @Test
    void testHashCode() {
        assertEquals(619577080, imageInfo.hashCode());
    }

    @Test
    void testGetters() {
        assertEquals("Zyra.png", imageInfo.getFull());
        assertEquals("champion8.png", imageInfo.getSprite());
        assertEquals("champion", imageInfo.getGroup());
        assertEquals(0, imageInfo.getSpriteOffsetX());
        assertEquals(48, imageInfo.getSpriteOffsetY());
        assertEquals(48, imageInfo.getSpriteWidth());
        assertEquals(48, imageInfo.getSpriteHeight());
    }

    @Test
    void testToString() {
        assertEquals("Image: Zyra.png sprite: champion8.png@px(0,48) 48x48", imageInfo.toString());
    }
}
