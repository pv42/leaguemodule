package disibot.league.api.dataDragon;

import disibot.league.api.dataDragon.entities.Version;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class VersionTest {
    @Test
    void testConstructor() {
        Version version = new Version("10.5.399");
        assertEquals(10, version.getMayor());
        assertEquals(5, version.getMinor());
        assertEquals(399, version.getBuild());
        assertEquals(new Version(10,5,399), version);
        assertThrows(IllegalArgumentException.class, () -> new Version("a.b.c"));
        assertThrows(IllegalArgumentException.class, () -> new Version("1.2"));
    }

    @Test
    void testEquals() {
        Version version = new Version(10,2,3);
        assertEquals(version, version);
        Version other = new Version(10,2,3);
        assertEquals(other, version);
        other = new Version(10,2,5);
        assertNotEquals(other, version);
        other = new Version(10,22,3);
        assertNotEquals(other, version);
        other = new Version(1,2,3);
        assertNotEquals(other, version);
        //noinspection SimplifiableJUnitAssertion,ConstantConditions
        assertFalse(version.equals(null));
        assertNotEquals(version, new Object());
    }

    @Test
    void testToString() {
        assertEquals("1.10.399", new Version(1,10,399).toString());
        assertEquals("999.0.0", new Version(999,0,0).toString());
    }

    @Test
    void testHashCode() {
        assertEquals(1051657, new Version(1,5,13).hashCode());
    }
}
