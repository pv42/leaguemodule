package disibot.league.api.dataDragon;

import disibot.league.api.dataDragon.entities.Champion;
import disibot.league.api.dataDragon.entities.ChampionPassive;
import disibot.league.api.dataDragon.entities.ChampionSkin;
import disibot.league.api.dataDragon.entities.ChampionSpell;
import disibot.league.api.dataDragon.entities.ChampionStats;
import disibot.league.api.dataDragon.entities.ExtendedChampionInfo;
import disibot.league.api.dataDragon.entities.LeagueItem;
import disibot.league.api.dataDragon.entities.LeagueItemInfos;
import disibot.league.api.exceptions.LeagueAPIException;
import disibot.league.api.impl.HttpURLDownloader;
import disibot.util.Pair;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.stubbing.Answer;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static disibot.league.api.dataDragon.ItemStatKey.FlatMPPoolMod;
import static disibot.league.api.dataDragon.ItemStatKey.FlatMagicDamageMod;
import static disibot.league.api.dataDragon.ItemTag.Active;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class DataDragonAPITest {
    private static DataDragonAPI api;
    private Map<URL, Pair<String, Integer>> responses = new HashMap<>(); // stream supposed to be return from url, but mocked


    @BeforeEach
    void setup() throws IOException, LeagueAPIException {
        api = new DataDragonAPI("na", Language.en_US, false, false);
        HttpURLDownloader mock = new MockUrlDownloader();
        api.setDownloader(mock);
        setUpMockResponses();
    }

    private void setUpMockResponses() throws MalformedURLException {
        setInputToFile("realms/na.json", "realms_na.json");
        setInputToFile("cdn/10.2.1/data/en_US/item.json", "items.json");
        setInputToFile("cdn/10.2.1/data/en_US/champion.json", "champions.json");
        setInputToFile("cdn/10.2.1/data/en_US/champion/Zyra.json", "zyra.json");
    }

    void setInputToFile(String urlPart, String filename) throws MalformedURLException {
        responses.put(new URL("https://ddragon.leagueoflegends.com/" + urlPart), new Pair<>("testres/dd_responses/" + filename, 200));
    }

    void setResponseCode(int responseCode, String urlPart) throws MalformedURLException {
        responses.put(new URL(urlPart), new Pair<>(null, responseCode));
    }

    @Test
    void testGetZyra() throws IOException, LeagueAPIException {
        Champion zyra = api.getChampionByName("Zyra");
        Champion zyra2 = api.getChampionByNameId("Zyra");
        assertNotNull(zyra);
        assertEquals(zyra, zyra2);
        assertEquals("Zyra", zyra.getName());
        assertEquals("Zyra", zyra.getNameId());
        assertEquals("Mana", zyra.getSecondaryBar());
        assertEquals(Arrays.toString(new String[]{"Mage", "Support"}), Arrays.toString(zyra.getTags().toArray()));
        assertEquals("Image: Zyra.png sprite: champion4.png@px(336,96) 48x48", zyra.getImage().toString());
        assertEquals("Rise of the Thorns", zyra.getTitle());
        assertEquals("Born in an ancient, sorcerous catastrophe, Zyra is the wrath of nature given form—an a" +
                "lluring hybrid of plant and human, kindling new life with every step. She views the many mortals of " +
                "Valoran as little more than prey for her seeded progeny, and thinks...", zyra.getLoreShort());
        assertEquals(4, zyra.getPlaystyleInfo().getAttack());
        assertEquals(3, zyra.getPlaystyleInfo().getDefense());
        assertEquals(8, zyra.getPlaystyleInfo().getMagic());
        assertEquals(7, zyra.getPlaystyleInfo().getDifficulty());
        testZyraStats(zyra.getStats());
    }

    private void testZyraStats(ChampionStats stats) {
        assertEquals(504f, stats.getHP());
        assertEquals(79, stats.getHPGain());
        assertEquals(418, stats.getMP());
        assertEquals(25, stats.getMPGain());
        assertEquals(340, stats.getMovementSpeed());
        assertEquals(29, stats.getArmor());
        assertEquals(3, stats.getArmorGain());
        assertEquals(30, stats.getMR());
        assertEquals(0.5, stats.getMRGain());
        assertEquals(575, stats.getAttackrange());
        assertEquals(5.5, stats.getHealthReg());
        assertEquals(0.5, stats.getHealthRegGain());
        assertEquals(13, stats.getManaReg());
        assertEquals(0.4f, stats.getManaRegGain());
        assertEquals(0, stats.getCrit());
        assertEquals(0, stats.getCritGain());
        assertEquals(53.376f, stats.getAD());
        assertEquals(3.2f, stats.getADGain());
        assertEquals(0.625, stats.getAttackspeed());
        assertEquals(2.11f, stats.getAttackspeedGain());
        assertEquals(stats, stats);
        assertEquals(-1578212682, stats.hashCode());
    }

    @Test
    void testZyraExtended() throws IOException, LeagueAPIException {
        Champion zyra = api.getChampionByName("Zyra");
        assertNotNull(zyra);
        ExtendedChampionInfo zyrapp = api.getChampionInfoExtended(zyra);
        assertNotNull(zyrapp);
        assertEquals(4, zyrapp.getSpells().size());
        assertEquals("Thick vines spread through the ground and explode into spines, dealing 60/95/130/165/" +
                        "200 <scaleAP>(+0.6)</scaleAP> magic damage to enemies within the area. <br /><br /><span class=\"" +
                        "colorDDDD77\">Garden of Thorns: </span>If Deadly Spines is cast near a seed, a Thorn Spitter grows, " +
                        "dealing ? <scaleAP>(+0.15)</scaleAP> magic damage. A Thorn Spitter has 575 range and lasts ? seconds.",
                zyrapp.getSpells().get(0).getTooltipFormatted());
        assertEquals(zyra.getPlaystyleInfo(), zyrapp.getPlaystyleInfo());
        assertEquals(zyra.getLoreShort(), zyrapp.getLoreShort());
        assertEquals(zyra.getImage(), zyrapp.getImage());
        assertEquals(zyra.getKey(), zyrapp.getKey());
        assertEquals(zyra.getName(), zyrapp.getName());
        assertEquals(zyra.getSecondaryBar(), zyrapp.getSecondaryBar());
        assertEquals(zyra.getTags(), zyrapp.getTags());
        assertEquals(zyra.getTitle(), zyrapp.getTitle());
        assertEquals(zyra.getStats(), zyrapp.getStats());
        ChampionSpell stranglethorns = zyrapp.getSpells().get(3);
        testZyraR(stranglethorns);
        testWildfireSkin(zyrapp.getSkins().get(1));
        testPassive(zyrapp.getPassive());
        assertEquals("Born in an ancient, sorcerous catastrophe, Zyra is the wrath of nature given form—an " +
                "alluring hybrid of plant and human, kindling new life with every step. She views the many mortals of" +
                " Valoran as little more than prey for her seeded progeny, and thinks nothing of slaying them with " +
                "flurries of deadly spines. Though her true purpose has not been revealed, Zyra wanders the world, " +
                "indulging her most primal urges to colonize, and strangle all other life from it.", zyrapp.getLore());
        testZyraTips(zyrapp);

    }

    private void testZyraTips(ExtendedChampionInfo zyrapp) {
        assertEquals(Arrays.asList("Placing a seed in your spell's path after you have cast it gives you" +
                        " the best chance of having a seed in the proper spot.", "Seeds grant vision to your team.", "Zyra is a" +
                        " great ambusher - look for opportunities to set a trap of seeds in brush, then lure enemies in."),
                zyrapp.getAllyTips());
        assertEquals(Arrays.asList("Zyra's seeds can be destroyed by stepping on them. If she tries to grow them as " +
                "you do, dodge back at the last moment. ", "Moving closer to Zyra can be a smart move if she places " +
                "plants too far away from herself.", "Plants take fixed damage from attacks and effects, similar to " +
                "wards. They also expire quickly on their own.", "Moving the fight to a different area will deprive " +
                "Zyra of accumulated seeds.", "Seeds placed by Zyra's W are larger, a different color, and grant a " +
                "small amount of vision to her team."), zyrapp.getEnemyTips());
    }

    private void testZyraR(ChampionSpell stranglethorns) {
        assertEquals("Stranglethorns", stranglethorns.getName());
        assertEquals(90, stranglethorns.getCooldownAtLevel(3));
        assertEquals(100, stranglethorns.getCostAtLevel(3));
        assertEquals(700, stranglethorns.getRangeAtLevel(2));
        assertEquals("Zyra summons a twisted thicket at her target location, dealing damage to enemies as it" +
                        " expands and knocking them airborne as it contracts. Plants within the thicket are enraged.",
                stranglethorns.getDescription());
        assertEquals(-1, stranglethorns.getMaxAmmo());
        assertEquals("ZyraR", stranglethorns.getId());
        assertEquals("{{ cost }} {{ abilityresourcename }}", stranglethorns.getResource());
    }

    private void testWildfireSkin(ChampionSkin skin) {
        assertEquals(143001, skin.getId());
        assertEquals(1, skin.getNum());
        assertEquals("Wildfire Zyra", skin.getName());
        assertFalse(skin.hasChromas());
    }

    private void testPassive(ChampionPassive passive) {
        assertEquals("Garden of Thorns", passive.getName());
        assertEquals("Seeds spawn around Zyra periodically, becoming faster with level and lasting 30 seconds. If an enemy Champion steps on a seed, it dies. <br><br>Zyra can cast spells near seeds to grow plants. Extra plants striking the same target deal reduced damage.", passive.getDescription());
        assertEquals("Image: ZyraP.png sprite: passive4.png@px(336,96) 48x48", passive.getImage().toString());
    }

    @Test
    void testGetAatrox() throws IOException, LeagueAPIException {
        Champion aatrox = api.getChampionByName("Aatrox");
        Champion aatrox2 = api.getChampionByNameId("Aatrox");
        assertNotNull(aatrox);
        assertEquals(aatrox, aatrox2);
        assertEquals("Aatrox", aatrox.getName());
        assertEquals("Aatrox", aatrox.getNameId());
        assertEquals("Blood Well", aatrox.getSecondaryBar());
    }

    @Test
    void testItems() throws IOException, LeagueAPIException {
        LeagueItemInfos items = api.getItems();
        for (LeagueItem item : items.getAllItems().values()) {
            assertNotNull(item);
        }
        LeagueItem embrace = items.getItemById(3040);
        assertEquals(3200, embrace.getBasePrize());
        assertEquals(2240, embrace.getSellPrize());
        assertEquals(3200, embrace.getTotalPrize());
        assertEquals("Seraph's Embrace", embrace.getName());
        assertEquals(3040, embrace.getId());
        assertFalse(embrace.isPurchasable());
        assertEquals("<stats>+50 Ability Power<br><mana>+1400 Mana</mana><br>+10% Cooldown Reduction</stats><br><br><unique>UNIQUE Passive - Haste:</unique> This item gains an additional 10% Cooldown Reduction.<br><mana><unique>UNIQUE Passive - Awe:</unique> Grants Ability Power equal to 3% of maximum Mana. Refunds 25% of Mana spent.</mana><br><active>UNIQUE Active - Mana Shield:</active> Consumes 15% of current Mana to grant a shield for 2 seconds that absorbs damage equal to 150 plus the amount of Mana consumed (120 second cooldown).<br><br><groupLimit>Limited to 1 Tear item.</groupLimit>", embrace.getStatsDescription());
        assertEquals("", embrace.getDescriptionText());
        assertEquals(1, embrace.getTags().size());
        assertEquals(Active, embrace.getTags().get(0));
        assertEquals(2, embrace.getStats().size());
        assertEquals(1400, embrace.getStats().get(FlatMPPoolMod));
        assertEquals(50, embrace.getStats().get(FlatMagicDamageMod));
        assertEquals(true, embrace.getMaps().get(10));
        assertEquals(true, embrace.getMaps().get(11));
        assertEquals(true, embrace.getMaps().get(12));
        assertEquals(false, embrace.getMaps().get(22));
        assertEquals("Seraph's Embrace", embrace.toString());
        LeagueItem domi = items.getItemById(3036);
        assertEquals(2, domi.getComponents().size());
        assertEquals(3035, domi.getComponents().get(0).getId());
        assertEquals(1037, domi.getComponents().get(1).getId());
        assertTrue(domi.isPurchasable());
        assertEquals(384, domi.getImage().getSpriteOffsetX());
    }

    private class MockUrlDownloader extends HttpURLDownloader {
        public HttpURLConnection download(URL url) throws IOException {
            HttpURLConnection conn = mock(HttpURLConnection.class);
            if (!responses.containsKey(url))
                throw new IllegalArgumentException("requested unknown url:" + url);
            when(conn.getInputStream()).then((Answer<InputStream>) invocation -> {
                System.out.println("Mocking " + url);
                return new FileInputStream(responses.get(url).getKey());
            });
            when(conn.getResponseCode()).then((Answer<Integer>) invocation -> responses.get(url).getValue());
            return conn;
        }
    }
}
