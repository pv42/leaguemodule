package disibot.league.api;

import disibot.league.api.dataDragon.entities.Champion;
import disibot.league.api.entities.ClashTeam;
import disibot.league.api.entities.ClashTeamMember;
import disibot.league.api.entities.CurrentGameInfo;
import disibot.league.api.entities.CurrentGameParticipant;
import disibot.league.api.entities.FreeChampionInfo;
import disibot.league.api.entities.Lane;
import disibot.league.api.entities.LeagueMatch;
import disibot.league.api.entities.LeagueMatchParticipant;
import disibot.league.api.entities.LeagueSummoner;
import disibot.league.api.entities.MatchHistory;
import disibot.league.api.entities.MatchHistoryEntry;
import disibot.league.api.entities.Position;
import disibot.league.api.entities.SummonerChampionInfo;
import disibot.league.api.entities.SummonerQueueInfo;
import disibot.league.api.entities.Team;
import disibot.league.api.entities.TeamRole;
import disibot.league.api.exceptions.BadRequestException;
import disibot.league.api.exceptions.InvalidAPIKeyException;
import disibot.league.api.exceptions.LeagueAPIException;
import disibot.league.api.exceptions.NoAPIKeyException;
import disibot.league.api.exceptions.NotFoundException;
import disibot.league.api.impl.HttpURLDownloader;
import disibot.league.api.impl.RiotAPIImpl;
import disibot.util.Pair;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.stubbing.Answer;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static disibot.league.api.LeagueAPIBuilder.RIOT_APIS.ALL;
import static disibot.league.api.dataDragon.Language.en_US;
import static disibot.league.api.entities.Lane.MID;
import static disibot.league.api.entities.LeagueTier.CHALLENGER;
import static disibot.league.api.entities.LeagueTier.PLATINUM;
import static disibot.league.api.entities.Region.EUW1;
import static java.time.ZoneOffset.UTC;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class RiotAPITest {
    private static final String API_KEY = "";
    private static RiotAPIImpl api;
    private Map<URL, Pair<String, Integer>> responses = new HashMap<>(); // stream supposed to be return from url, but mocked

    // also test the builder
    @BeforeEach
    void setup() {
        try {
            LeagueAPIBuilder builder = new LeagueAPIBuilder();
            builder.setApiKey(API_KEY);
            builder.setCreateCacheOnBuild(ALL);
            builder.setUseDiskCacheMode(ALL);
            builder.setDataDragonVersionRegion("euw");
            builder.setLanguage(en_US);
            builder.setRegion(EUW1);
            api = (RiotAPIImpl) builder.build();
            setupMockito();
        } catch (IOException | LeagueAPIException | NoSuchFieldException | IllegalAccessException e) {
            fail(e);
        }
    }

    void setupMockito() throws NoSuchFieldException, IllegalAccessException, MalformedURLException, FileNotFoundException {
        MockUrlDownloader downloader = new MockUrlDownloader();
        api.setDownloader(downloader);

        setInputToFile("lol/champion-mastery/v4/champion-masteries/by-summoner/Uh2MFNYS030H2909B7rplMdGL-CvarKuyB6ZxGNm3xWuF7g", "champion_mastery_all.json");
        setInputToFile("lol/platform/v3/champion-rotations", "free_champions.json");
        setInputToFile("lol/spectator/v4/active-games/by-summoner/Uh2MFNYS030H2909B7rplMdGL-CvarKuyB6ZxGNm3xWuF7g", "active_game_by_summoner.json");
        setInputToFile("lol/summoner/v4/summoners/KXN5Dg0seB1BCLxNjDICNMdgGAOPDwpiYmp9agBb9KueUmI", "summoner_by_id.json");
        setInputToFile("lol/summoner/v4/summoners/by-account/0PfA_uQYWcnxR0SAFazu6pbVl0EVqL0LEJhI8vgZ6XRXFA", "summoner_by_account.json");
        setInputToFile("lol/summoner/v4/summoners/by-name/Relayted", "summoner_by_name.json");
        setInputToFile("lol/summoner/v4/summoners/Uh2MFNYS030H2909B7rplMdGL-CvarKuyB6ZxGNm3xWuF7g", "summoner_by_name.json");
        setInputToFile("lol/league/v4/entries/by-summoner/Uh2MFNYS030H2909B7rplMdGL-CvarKuyB6ZxGNm3xWuF7g", "queues.json");
        setInputToFile("lol/champion-mastery/v4/scores/by-summoner/Uh2MFNYS030H2909B7rplMdGL-CvarKuyB6ZxGNm3xWuF7g", "total_mastery");
        setInputToFile("lol/match/v4/matchlists/by-account/LVKZb0Z9ELjnu9JqsALsqKJdpLGhX-6F2yAGAUI4kOr94g", "matchlist.json");
        setInputToFile("lol/match/v4/matches/4399087828", "match.json");
        setInputToFile("tft/league/v1/entries/by-summoner/Uh2MFNYS030H2909B7rplMdGL-CvarKuyB6ZxGNm3xWuF7g", "tft_rank.json");
        setInputToFile("lol/match/v4/matchlists/by-account/LVKZb0Z9ELjnu9JqsALsqKJdpLGhX-6F2yAGAUI4kOr94g?champion=131&champion=142&champion=246", "matchlist_filtered_by_champ.json");
        setInputToFile("lol/summoner/v4/summoners/by-name/RelaytedTCM400","summoner_by_name_tcm400.json");
        setInputToFile("lol/summoner/v4/summoners/by-name/RelaytedTCM401","summoner_by_name_tcm401.json");
        setInputToFile("lol/summoner/v4/summoners/by-name/RelaytedTCM403","summoner_by_name_tcm403.json");
        setInputToFile("lol/summoner/v4/summoners/by-name/RelaytedTCM404","summoner_by_name_tcm404.json");
        setInputToFile("lol/clash/v1/players/by-summoner/Uh2MFNYS030H2909B7rplMdGL-CvarKuyB6ZxGNm3xWuF7g", "clash_team_by_summoner.json");
        setInputToFile("lol/clash/v1/teams/1471682","clash_team_by_id.json");
        setResponseCode(400, "lol/summoner/v4/summoners/by-name/Relayted400");
        setResponseCode(401, "lol/summoner/v4/summoners/by-name/Relayted401");
        setResponseCode(403, "lol/summoner/v4/summoners/by-name/Relayted403");
        setResponseCode(404, "lol/summoner/v4/summoners/by-name/Relayted404");
        setResponseCode(400, "lol/champion-mastery/v4/scores/by-summoner/id_of_tcm400");
        setResponseCode(401, "lol/champion-mastery/v4/scores/by-summoner/id_of_tcm401");
        setResponseCode(403, "lol/champion-mastery/v4/scores/by-summoner/id_of_tcm403");
        setResponseCode(404, "lol/champion-mastery/v4/scores/by-summoner/id_of_tcm404");
    }

    void setInputToFile(String urlPart, String filename) throws MalformedURLException {
        responses.put(new URL("https://euw1.api.riotgames.com/" + urlPart), new Pair<>("testres/api_responses/" + filename, 200));
    }

    void setResponseCode(int responseCode, String urlPart) throws MalformedURLException {
        responses.put(new URL("https://euw1.api.riotgames.com/" + urlPart), new Pair<>(null, responseCode));
    }

    @Test
    public void testGetSummonerByName() throws IOException, LeagueAPIException {
        LeagueSummoner summoner = getSummonerByName();
        assertEquals("Relayted", summoner.getName());
        assertEquals("Uh2MFNYS030H2909B7rplMdGL-CvarKuyB6ZxGNm3xWuF7g", summoner.getId());
        assertEquals(232, summoner.getSummonerLevel());
        assertEquals("LVKZb0Z9ELjnu9JqsALsqKJdpLGhX-6F2yAGAUI4kOr94g", summoner.getAccountId());
        assertEquals(4214, summoner.getProfileIcon().getId());
        assertEquals("IRwiKdfTwr1kDArVF9d_Jxpvx1Tjn8haNPEVZ4JQRvi01DAEM2NEN8SIEFiFwjqcCnhPu45gfmmIYA", summoner.getPuuId());
        assertEquals(LocalDateTime.ofEpochSecond(1580944491, 0, UTC), summoner.getRevisionDate());
    }

    @Test
    public void testSummonerById() throws IOException, LeagueAPIException {
        LeagueSummoner summoner = api.getSummonerById("KXN5Dg0seB1BCLxNjDICNMdgGAOPDwpiYmp9agBb9KueUmI");
        assertEquals("Hide on Brush", summoner.getName());
        assertEquals("KXN5Dg0seB1BCLxNjDICNMdgGAOPDwpiYmp9agBb9KueUmI", summoner.getId());
        assertEquals(30, summoner.getSummonerLevel());
        assertEquals("pjYuC7pqWyRmx5j03xeYkdg8pFLRZpjBYx9_Ck9On9jkCBc", summoner.getAccountId());
        assertEquals(1666, summoner.getProfileIcon().getId());
        assertEquals("NTIY8y5ACefESpR0n_2MFCb-BO4Y5AVUOnLoZ1NJuUS6ry2Ls9lrSDwmYyIfC4gXRp32N3XUtwrNxg", summoner.getPuuId());
        assertEquals(LocalDateTime.ofEpochSecond(1520539793, 0, UTC), summoner.getRevisionDate());
    }

    @Test
    public void testSummonerByAccount() throws IOException, LeagueAPIException {
        LeagueSummoner summoner = api.getSummonerByAccountId("0PfA_uQYWcnxR0SAFazu6pbVl0EVqL0LEJhI8vgZ6XRXFA");
        assertEquals("S04 Forg1ven", summoner.getName());
        assertEquals("kqXFwYV-fwwEA5dmmZS1anQn0vk6EQuo1DYobLjjOJP27CQ", summoner.getId());
        assertEquals(59, summoner.getSummonerLevel());
        assertEquals("0PfA_uQYWcnxR0SAFazu6pbVl0EVqL0LEJhI8vgZ6XRXFA", summoner.getAccountId());
        assertEquals(1665, summoner.getProfileIcon().getId());
        assertEquals("xXNqGjRAYgR2Dxa2lCWvyLHpCOTheinuGxz0hlkBEcurjQKQioXJkt5n3NePRfzFdbK5FP57xLNcKA", summoner.getPuuId());
        assertEquals(LocalDateTime.ofEpochSecond(1581249165, 0, UTC), summoner.getRevisionDate());

    }

    private LeagueSummoner getSummonerByName() throws IOException, LeagueAPIException {
        return api.getSummonerByName("Relayted");
    }

    @Test
    void test400() {
        assertThrows(BadRequestException.class, () -> api.getSummonerByName("Relayted400"));
        assertThrows(BadRequestException.class, () -> api.getSummonerByName("RelaytedTCM400").getTotalChampionMastery());
    }

    @Test
    void test401() {
        assertThrows(NoAPIKeyException.class, () -> api.getSummonerByName("Relayted401"));
        assertThrows(NoAPIKeyException.class, () -> api.getSummonerByName("RelaytedTCM401").getTotalChampionMastery());
    }

    @Test
    void test403() {
        assertThrows(InvalidAPIKeyException.class, () -> api.getSummonerByName("Relayted403"));
        assertThrows(InvalidAPIKeyException.class, () -> api.getSummonerByName("RelaytedTCM403").getTotalChampionMastery());
    }

    @Test
    void test404() {
        assertThrows(NotFoundException.class, () -> api.getSummonerByName("Relayted404"));
        assertThrows(NotFoundException.class, () -> api.getSummonerByName("RelaytedTCM404").getTotalChampionMastery());
    }

    @Test
    void testMatchList() throws IOException, LeagueAPIException {
        LeagueSummoner summoner = getSummonerByName();
        MatchHistory matchlist = summoner.getMatchlist(null);
        MatchHistoryEntry entry = matchlist.getMatches().get(8);
//        entry.getSeason();
        LeagueMatch match = entry.getGame();
        for (LeagueMatchParticipant participant : match.getParticipants()) {
            assertNotNull(participant.getStats());
        }
        LeagueMatchParticipant participant = match.getParticipants().get(0);
        //todo
    }

    @Test
    void testFilteredMatchList() throws IOException, LeagueAPIException {
        LeagueSummoner summoner = getSummonerByName();
        List<Champion> filter = new ArrayList<>();
        filter.add(api.getDataDragonAPI().getChampionByName("Diana"));
        filter.add(api.getDataDragonAPI().getChampionByName("Zoe"));
        filter.add(api.getDataDragonAPI().getChampionByName("Qiyana"));
        MatchHistory matchlist = summoner.getMatchlist(filter);
        assertEquals(54, matchlist.getMatches().size());
        assertEquals("Diana", matchlist.getMatches().get(0).getChampion().getName());
        assertEquals("Qiyana", matchlist.getMatches().get(4).getChampion().getName());
        assertEquals("Zoe", matchlist.getMatches().get(23).getChampion().getName());
        MatchHistoryEntry m53 = matchlist.getMatches().get(53);
        assertEquals(Lane.MID, m53.getLane());
        assertEquals(EUW1, m53.getRegion());
        assertEquals(11, m53.getSeason().getId());
        assertEquals(LocalDateTime.ofEpochSecond(1524915097, 966000000, UTC), m53.getTime());
        assertEquals("SOLO", m53.getRole());
    }


    @Test
    void testGetFreeChampions() {
        try {
            FreeChampionInfo free = api.getFreeChampions();
            assertTrue(free.getFreeChampions().size() > 0);
            assertTrue(free.getFreeChampionsForNewPlayers().size() > 0);
            assertTrue(free.getMaxNewPlayerLevel() > 0);
        } catch (IOException | LeagueAPIException e) {
            fail(e);
        }
    }

    @Test
    void testTotalChampionMaster() throws IOException, LeagueAPIException {
        LeagueSummoner summoner = getSummonerByName();
        assertEquals(421, summoner.getTotalChampionMastery());
    }

    @Test
    void testQueueInfos() throws IOException, LeagueAPIException {
        LeagueSummoner summoner = getSummonerByName();
        SummonerQueueInfo soloQ = summoner.getSoloQueueInfo();
        assertNotNull(soloQ);
        assertEquals(2, soloQ.getDivisionInt());
        assertEquals("RANKED_SOLO_5x5", soloQ.getQueueType());
        assertEquals(summoner.getName(), soloQ.getSummonerName());
        assertTrue(soloQ.onHotStreak());
        assertEquals(7, soloQ.getWins());
        assertEquals(70, soloQ.getLeaguePoints());
        assertEquals(PLATINUM, soloQ.getTier());
        assertEquals(7, soloQ.getLosses());
    }

    @Test
    void testTFTQueueInfos() throws IOException, LeagueAPIException {
        LeagueSummoner summoner = getSummonerByName();
        SummonerQueueInfo tft = summoner.getTFTQueueInfo();
        assertNotNull(tft);
        assertEquals(1, tft.getDivisionInt());
        assertEquals(CHALLENGER, tft.getTier());
        assertEquals(413, tft.getLeaguePoints());
        assertEquals(51, tft.getWins());
        assertEquals(324, tft.getLosses());
        assertEquals("RANKED_TFT", tft.getQueueType());
        assertEquals(summoner.getName(), tft.getSummonerName());
        setInputToFile("tft/league/v1/entries/by-summoner/Uh2MFNYS030H2909B7rplMdGL-CvarKuyB6ZxGNm3xWuF7g", "tft_rank_none.json");
        assertNull(summoner.getTFTQueueInfo());
    }

    @Test
    void testGetAllChampionInfo() throws IOException, LeagueAPIException {
        LeagueSummoner summoner = getSummonerByName();
        List<SummonerChampionInfo> infos = summoner.getAllChampionInfo();
        assertEquals(142, infos.size());
        SummonerChampionInfo i2 = infos.get(2);
        assertEquals(7, i2.getChampionLevel());
        assertFalse(i2.isChestGranted());
        assertEquals(95475, i2.getChampionPoints());
        assertEquals(0, i2.getChampionPointsUntilNextLevel());
        assertEquals(73875, i2.getChampionPointsSinceLastLevel());
        assertEquals(summoner, i2.getSummoner());
        assertEquals(0, i2.getTokensEarned());
        assertEquals("Miss Fortune", i2.getChampion().getName());
        assertEquals(LocalDateTime.ofEpochSecond(1581277614, 0, UTC), i2.getLastTimePlayed());
    }

    @Test
    void testCurrentGame() throws IOException, LeagueAPIException {
        LeagueSummoner summoner = getSummonerByName();
        CurrentGameInfo cg = summoner.getCurrentGame();
        assertNotNull(cg);
        assertEquals(4409333499L, cg.getId());
        assertEquals(LocalDateTime.ofEpochSecond(1581527085, 644000000, UTC), cg.getStartTime());
        assertEquals(EUW1, cg.getRegion());
        assertEquals("ARAM", cg.getGameMode());
        assertEquals(10, cg.getParticipants().size());
        CurrentGameParticipant p9 = cg.getParticipants().get(9);
        assertEquals(4404, p9.getProfileIcon().getId());
        assertEquals("Xerath", p9.getChampion().getName());
        assertEquals("AlphaBienchen", p9.getSummonerName());
        assertFalse(p9.isBot());
        assertEquals("Heal", p9.getSummonerSpell1().getName());
        assertEquals("Flash", p9.getSummonerSpell2().getName());
        assertEquals(Team.RED, p9.getTeam());
    }

    @Test
    void testClash() throws IOException, LeagueAPIException {
        LeagueSummoner summoner = getSummonerByName();
        List<ClashTeamMember> memberships = summoner.getClashTeams();
        assertEquals(1, memberships.size());
        assertEquals(Position.JUNGLE, memberships.get(0).getPosition());
        assertEquals(TeamRole.CAPTAIN, memberships.get(0).getRole());
        assertEquals(summoner, memberships.get(0).getSummoner());
        ClashTeam team = memberships.get(0).getTeam();
        assertEquals("Zyra FTW", team.getName());
        assertEquals("ZFW", team.getAbbreviation());
        assertEquals(summoner, team.getPlayers().get(0).getSummoner());
    }


    private class MockUrlDownloader extends HttpURLDownloader {
        public HttpURLConnection download(URL url) throws IOException {
            HttpURLConnection conn = mock(HttpURLConnection.class);
            if (!responses.containsKey(url))
                throw new IllegalArgumentException("requested unknown url:" + url);
            when(conn.getInputStream()).then((Answer<InputStream>) invocation -> new FileInputStream(responses.get(url).getKey()));
            when(conn.getResponseCode()).then((Answer<Integer>) invocation -> responses.get(url).getValue());
            when(conn.getErrorStream()).then((Answer<InputStream>) invocation -> new InputStream() {
                @Override
                public int read() {
                    return -1;
                }
            });
            when(conn.getURL()).then((Answer<URL>) invocation -> url);
            return conn;
        }
    }

}
