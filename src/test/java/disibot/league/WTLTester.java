package disibot.league;

import disibot.league.api.dataDragon.entities.ProfileIcon;
import disibot.league.api.entities.LeagueSummoner;
import disibot.league.api.entities.LeagueTier;
import disibot.league.api.entities.SummonerQueueInfo;
import disibot.league.api.exceptions.LeagueAPIException;
import disibot.league.api.impl.LeagueSummonerImpl;
import disibot.modules.DisiBotModuleInterface;
import fakejda.FakeGuild;
import fakejda.FakeUser;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.User;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.time.Clock;
import java.time.Instant;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class WTLTester {
    private static final String res_path = "file:///" + System.getProperty("user.dir").replaceAll("\\\\","/");
            //"file:///C:/Users/pv42/Documents/IdeaProjects/DisiBot";
    private static final String expectedFullXML = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>\n" +
            "<svg\n" +
            "        xmlns=\"http://www.w3.org/2000/svg\"\n" +
            "        xmlns:xlink=\"http://www.w3.org/1999/xlink\"\n" +
            "        version=\"1.1\"\n" +
            "        x=\"0px\"\n" +
            "        y=\"0px\"\n" +
            "        width=\"560\"\n" +
            "        height=\"400\"\n" +
            "        id=\"svg2961\">\n" +
            "    <defs>\n" +
            "        <clipPath id=\"circle\">\n" +
            "            <circle cx=\"150\" cy=\"150\" r=\"150\"/>\n" +
            "        </clipPath>\n" +
            "        <style type=\"text/css\"><![CDATA[\n" +
            "\t\t\t#podium rect {\n" +
            "\t\t\t\tstroke: #fff;\n" +
            "\t\t\t\tstroke-width: 0;\n" +
            "\t\t\t\tfill: #36393F;\n" +
            "\t\t\t}\n" +
            "\t\t\ttext {\n" +
            "\t\t\t    background-color: #f00;\n" +
            "\t\t\t    fill: #fff;\n" +
            "\t\t\t    font-size: 14;\n" +
            "\t\t\t    font-family: Arial;\n" +
            "\t\t\t}\n" +
            "\t\t\t#table-header {\n" +
            "\t\t\t    font-weight: bold;\n" +
            "\t\t\t}\n" +
            "\t\t\t#title {\n" +
            "\t\t\t    font-weight: bold;\n" +
            "\t\t\t}\n" +
            "\t\t\t#bg {\n" +
            "\t\t\t    fill:#2F3136;\n" +
            "\t\t\t}\n" +
            "\t\t\t#profile-icon-border {\n" +
            "\t\t\t    fill:none;\n" +
            "\t\t\t    stroke-width:12;\n" +
            "\t\t\t    stroke:#AC8F23;\n" +
            "\t\t\t    transform:scale(10);\n" +
            "\t\t\t}\n" +
            "\t\t]]></style>\n" +
            "    </defs>\n" +
            "    <rect width=\"100%\" height=\"100%\" id=\"bg\"/>\n" +
            "    <g id=\"title\">\n" +
            "        <text x=\"290\" y=\"30\" text-anchor=\"middle\" style=\"font-size:20\">Weekly SoloQ Ranking 1/1970</text>\n" +
            "        <text x=\"290\" y=\"45\" text-anchor=\"middle\" style=\"font-size:14\">25.12.1969 - 01.01.1970</text>\n" +
            "    </g>\n" +
            "    <g id=\"table\">\n" +
            "        <g id=\"table-header\">\n" +
            "            <text x=\"4\" y=\"65\">Player</text>\n" +
            "            <text x=\"170\" y=\"65\">Wins/Losses</text>\n" +
            "            <text x=\"270\" y=\"65\">Rank</text>\n" +
            "        </g>\n" +
            "        <!--suppress XmlUnboundNsPrefix -->\n" +
            "<g class=\"player\" transform=\"translate(0,85)\">\n" +
            "    <g class=\"profileIcon\" transform=\"translate(4,-11),scale(0.1)\">\n" +
            "        <image width=\"300\" height=\"300\" clip-path=\"url(#circle)\"\n" +
            "               xlink:href=\"protocol://server/path-to-icon\"/>\n" +
            "        <circle id=\"profile-icon-border\" cx=\"150\" cy=\"150\" r=\"150\"/>\n" +
            "    </g>\n" +
            "    <text x=\"40\" y=\"0\">TestSummoner</text>\n" +
            "    <text x=\"40\" y=\"13\" style=\"font-size:11; font-weight:bold\">@FakeUser</text>\n" +
            "    <text x=\"170\">31/0 (100%)</text>\n" +
            "    <g class=\"ranked-info\">\n" +
            "        <text x=\"310\">Bronze 5</text>\n" +
            "        <text x=\"310\" y=\"13\" style=\"font-size:11; font-weight:bold\">0LP</text>\n" +
            "        <image x=\"265\" y=\"-18\" width=\"40\" height=\"45\"\n" +
            "               xlink:href=\""+res_path+"/res/Emblem_Bronze.png\"/>\n" +
            "    </g>\n" +
            "    <path transform=\"translate(405,2),scale(0.3),rotate(45)\"\n" +
            "          d=\"M0 -50 L-50 0 L-25 0 L-25 50 L25 50 L25 0 L50 0 Z\" style=\"fill:#4bb44b\"/> <!-- #4bb44b-->\n" +
            "    <g class=\"ranked-info\" transform=\"translate(160,0)\">\n" +
            "        <text x=\"310\">Diamond 1</text>\n" +
            "        <text x=\"310\" y=\"13\" style=\"font-size:11; font-weight:bold\">100LP</text>\n" +
            "        <image x=\"265\" y=\"-18\" width=\"40\" height=\"45\"\n" +
            "               xlink:href=\""+res_path+"/res/Emblem_Diamond.png\"/>\n" +
            "    </g>\n" +
            "</g><!--suppress XmlUnboundNsPrefix -->\n" +
            "<g class=\"player\" transform=\"translate(0,130)\">\n" +
            "    <g class=\"profileIcon\" transform=\"translate(4,-11),scale(0.1)\">\n" +
            "        <image width=\"300\" height=\"300\" clip-path=\"url(#circle)\"\n" +
            "               xlink:href=\"protocol://server/path-to-icon\"/>\n" +
            "        <circle id=\"profile-icon-border\" cx=\"150\" cy=\"150\" r=\"150\"/>\n" +
            "    </g>\n" +
            "    <text x=\"40\" y=\"0\">TestSummoner</text>\n" +
            "    <text x=\"40\" y=\"13\" style=\"font-size:11; font-weight:bold\">@FakeUser</text>\n" +
            "    <text x=\"170\">3/2 (60%)</text>\n" +
            "    <g class=\"ranked-info\">\n" +
            "        <text x=\"310\">GOLD 2</text>\n" +
            "        <text x=\"310\" y=\"13\" style=\"font-size:11; font-weight:bold\">78LP</text>\n" +
            "        <image x=\"265\" y=\"-18\" width=\"40\" height=\"45\"\n" +
            "               xlink:href=\""+res_path+"/res/Emblem_Gold.png\"/>\n" +
            "    </g>\n" +
            "    <path transform=\"translate(405,2),scale(0.3),rotate(45)\"\n" +
            "          d=\"M0 -50 L-50 0 L-25 0 L-25 50 L25 50 L25 0 L50 0 Z\" style=\"fill:#4bb44b\"/> <!-- #4bb44b-->\n" +
            "    <g class=\"ranked-info\" transform=\"translate(160,0)\">\n" +
            "        <text x=\"310\">Gold 1</text>\n" +
            "        <text x=\"310\" y=\"13\" style=\"font-size:11; font-weight:bold\">23LP</text>\n" +
            "        <image x=\"265\" y=\"-18\" width=\"40\" height=\"45\"\n" +
            "               xlink:href=\""+res_path+"/res/Emblem_Gold.png\"/>\n" +
            "    </g>\n" +
            "</g><!--suppress XmlUnboundNsPrefix -->\n" +
            "<g class=\"player\" transform=\"translate(0,175)\">\n" +
            "    <g class=\"profileIcon\" transform=\"translate(4,-11),scale(0.1)\">\n" +
            "        <image width=\"300\" height=\"300\" clip-path=\"url(#circle)\"\n" +
            "               xlink:href=\"protocol://server/path-to-icon\"/>\n" +
            "        <circle id=\"profile-icon-border\" cx=\"150\" cy=\"150\" r=\"150\"/>\n" +
            "    </g>\n" +
            "    <text x=\"40\" y=\"0\">TestSummoner</text>\n" +
            "    <text x=\"40\" y=\"13\" style=\"font-size:11; font-weight:bold\">@FakeUser</text>\n" +
            "    <text x=\"170\">0/0 (0%)</text>\n" +
            "    <g class=\"ranked-info\">\n" +
            "        <text x=\"310\">Diamond 4</text>\n" +
            "        <text x=\"310\" y=\"13\" style=\"font-size:11; font-weight:bold\">100LP</text>\n" +
            "        <image x=\"265\" y=\"-18\" width=\"40\" height=\"45\"\n" +
            "               xlink:href=\""+res_path+"/res/Emblem_Diamond.png\"/>\n" +
            "    </g>\n" +
            "    <path transform=\"translate(405,2),scale(0.3),rotate(135)\"\n" +
            "          d=\"M0 -50 L-50 0 L-25 0 L-25 50 L25 50 L25 0 L50 0 Z\" style=\"fill:#9c3213\"/> <!-- #4bb44b-->\n" +
            "    <g class=\"ranked-info\" transform=\"translate(160,0)\">\n" +
            "        <text x=\"310\">Platinum 2</text>\n" +
            "        <text x=\"310\" y=\"13\" style=\"font-size:11; font-weight:bold\">0LP</text>\n" +
            "        <image x=\"265\" y=\"-18\" width=\"40\" height=\"45\"\n" +
            "               xlink:href=\""+res_path+"/res/Emblem_Platinum.png\"/>\n" +
            "    </g>\n" +
            "</g>\n" +
            "    </g>\n" +
            "</svg>".replaceAll("\\n", "\\r\\n");

    @Test
    void testFormatSVG() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException, IOException, LeagueAPIException, NoSuchFieldException {
        // String formatSVG(String svg, String part, Guild guild) throws IOException, LeagueAPIException
        Method formatMethod = WeeklyTopListManager.class.getDeclaredMethod("formatSVG", String.class, String.class, Guild.class);
        formatMethod.setAccessible(true);
        Method loadFileMethod = WeeklyTopListManager.class.getDeclaredMethod("loadSVGFile", String.class);
        loadFileMethod.setAccessible(true);
        Field clockField = WeeklyTopListManager.class.getDeclaredField("clock");
        clockField.setAccessible(true);
        String mainSvg = (String) loadFileMethod.invoke(null, "res/weekly_ranking.svg");
        String partSvg = (String) loadFileMethod.invoke(null, "res/weekly_ranking_player.svg");
        FakeGuild guild = new FakeGuild("wtltestguild");
        FakeUser user = new FakeUser(101);
        FakeUser user2 = new FakeUser(102);
        FakeUser userWOSummoner = new FakeUser(103);
        FakeUser user3 = new FakeUser(105);
        guild.addMember(user);
        guild.addMember(user2);
        guild.addMember(user3);
        SummonerRankedStorage storage = mock(SummonerRankedStorage.class);
        LeagueSummoner climbingSummoner = createClimbingSummoner();
        LeagueSummoner slowlyClimbingSummoner = createSlowlyClimbingSummoner();
        LeagueSummoner decayingSummoner = createDecayingSummoner();
        List<SummonerQueueInfo> oldList = new ArrayList<>();
        oldList.add(createClimbingOldSQI(climbingSummoner));
        oldList.add(createSlowlyClimbingOldSQI(slowlyClimbingSummoner));
        oldList.add(createDecayingClimbingOldSQI(decayingSummoner));
        Map<User, LeagueSummoner> userSummonerMap = new HashMap<>();
        userSummonerMap.put(user, climbingSummoner);
        userSummonerMap.put(userWOSummoner, null);
        userSummonerMap.put(user2, slowlyClimbingSummoner);
        userSummonerMap.put(user3, decayingSummoner);
        when(storage.getSnapshotData()).thenReturn(oldList);
        when(storage.getUserSummonerMap()).thenReturn(userSummonerMap);
        WeeklyTopListManager wtl = new WeeklyTopListManager(storage, null, null);
        DisiBotModuleInterface moduleInterface = mock(DisiBotModuleInterface.class);
        when(moduleInterface.getConfig(any(), any())).thenReturn(new LeagueConfig());
        wtl.setModuleInterface(moduleInterface);
        clockField.set(wtl, Clock.fixed(Instant.EPOCH, ZoneId.of("GMT+0")));
        String result = (String) formatMethod.invoke(wtl, mainSvg, partSvg, guild);
        assertEquals(expectedFullXML, result.replaceAll("\\r", ""));
    }

    private LeagueSummoner createClimbingSummoner() throws IOException, LeagueAPIException {
        LeagueSummoner climbingSummoner = mock(LeagueSummonerImpl.class);
        ProfileIcon icon = mock(ProfileIcon.class);
        when(icon.getUrl()).thenReturn("protocol://server/path-to-icon");
        when(climbingSummoner.getProfileIcon()).thenReturn(icon);
        when(climbingSummoner.getName()).thenReturn("TestSummoner");
        SummonerQueueInfo newsqi = mock(SummonerQueueInfo.class);
        when(newsqi.getSummoner()).thenReturn(climbingSummoner);
        when(newsqi.getTierAndDivision()).thenReturn("Diamond 1");
        when(newsqi.getTier()).thenReturn(LeagueTier.DIAMOND);
        when(newsqi.getDivisionInt()).thenReturn(1);
        when(newsqi.getLeaguePoints()).thenReturn(100);
        when(newsqi.getGameNumber()).thenReturn(31);
        when(newsqi.getWins()).thenReturn(31);
        when(climbingSummoner.getSoloQueueInfo()).thenReturn(newsqi);
        return climbingSummoner;
    }

    private SummonerQueueInfo createClimbingOldSQI(LeagueSummoner climbingSummoner) throws IOException, LeagueAPIException {
        SummonerQueueInfo oldsqi = mock(SummonerQueueInfo.class);
        when(oldsqi.getSummoner()).thenReturn(climbingSummoner);
        when(oldsqi.getTierAndDivision()).thenReturn("Bronze 5");
        when(oldsqi.getTier()).thenReturn(LeagueTier.BRONZE);
        when(oldsqi.getDivisionInt()).thenReturn(5);
        when(oldsqi.getLeaguePoints()).thenReturn(0);
        when(oldsqi.compareTo(any())).thenReturn(-1);
        return oldsqi;
    }

    private LeagueSummoner createSlowlyClimbingSummoner() throws IOException, LeagueAPIException {
        LeagueSummoner climbingSummoner = mock(LeagueSummonerImpl.class);
        ProfileIcon icon = mock(ProfileIcon.class);
        when(icon.getUrl()).thenReturn("protocol://server/path-to-icon");
        when(climbingSummoner.getProfileIcon()).thenReturn(icon);
        when(climbingSummoner.getName()).thenReturn("TestSummoner");
        SummonerQueueInfo newsqi = mock(SummonerQueueInfo.class);
        when(newsqi.getSummoner()).thenReturn(climbingSummoner);
        when(newsqi.getTierAndDivision()).thenReturn("Gold 1");
        when(newsqi.getTier()).thenReturn(LeagueTier.GOLD);
        when(newsqi.getDivisionInt()).thenReturn(1);
        when(newsqi.getLeaguePoints()).thenReturn(23);
        when(newsqi.getGameNumber()).thenReturn(31);
        when(newsqi.getWins()).thenReturn(21);
        when(newsqi.getLosses()).thenReturn(10);
        when(climbingSummoner.getSoloQueueInfo()).thenReturn(newsqi);
        return climbingSummoner;
    }

    private SummonerQueueInfo createSlowlyClimbingOldSQI(LeagueSummoner climbingSummoner) throws IOException, LeagueAPIException {
        SummonerQueueInfo oldsqi = mock(SummonerQueueInfo.class);
        when(oldsqi.getSummoner()).thenReturn(climbingSummoner);
        when(oldsqi.getTierAndDivision()).thenReturn("GOLD 2");
        when(oldsqi.getTier()).thenReturn(LeagueTier.GOLD);
        when(oldsqi.getDivisionInt()).thenReturn(2);
        when(oldsqi.getLeaguePoints()).thenReturn(78);
        when(oldsqi.getGameNumber()).thenReturn(26);
        when(oldsqi.getWins()).thenReturn(18);
        when(oldsqi.getLosses()).thenReturn(8);
        when(oldsqi.compareTo(any())).thenReturn(-1);
        return oldsqi;
    }

    private LeagueSummoner createDecayingSummoner() throws IOException, LeagueAPIException {
        LeagueSummoner climbingSummoner = mock(LeagueSummonerImpl.class);
        ProfileIcon icon = mock(ProfileIcon.class);
        when(icon.getUrl()).thenReturn("protocol://server/path-to-icon");
        when(climbingSummoner.getProfileIcon()).thenReturn(icon);
        when(climbingSummoner.getName()).thenReturn("TestSummoner");
        SummonerQueueInfo newsqi = mock(SummonerQueueInfo.class);
        when(newsqi.getSummoner()).thenReturn(climbingSummoner);
        when(newsqi.getTierAndDivision()).thenReturn("Platinum 2");
        when(newsqi.getTier()).thenReturn(LeagueTier.PLATINUM);
        when(newsqi.getDivisionInt()).thenReturn(2);
        when(newsqi.getLeaguePoints()).thenReturn(0);
        when(newsqi.getGameNumber()).thenReturn(10);
        when(newsqi.getWins()).thenReturn(5);
        when(newsqi.getLosses()).thenReturn(5);
        when(climbingSummoner.getSoloQueueInfo()).thenReturn(newsqi);
        return climbingSummoner;
    }

    private SummonerQueueInfo createDecayingClimbingOldSQI(LeagueSummoner climbingSummoner) throws IOException, LeagueAPIException {
        SummonerQueueInfo oldsqi = mock(SummonerQueueInfo.class);
        when(oldsqi.getSummoner()).thenReturn(climbingSummoner);
        when(oldsqi.getTierAndDivision()).thenReturn("Diamond 4");
        when(oldsqi.getTier()).thenReturn(LeagueTier.DIAMOND);
        when(oldsqi.getDivisionInt()).thenReturn(4);
        when(oldsqi.getLeaguePoints()).thenReturn(100);
        when(oldsqi.getGameNumber()).thenReturn(10);
        when(oldsqi.getWins()).thenReturn(5);
        when(oldsqi.getLosses()).thenReturn(5);
        return oldsqi;
    }
}
