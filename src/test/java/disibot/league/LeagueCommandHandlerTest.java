package disibot.league;

import disibot.league.api.RiotAPI;
import disibot.league.api.dataDragon.entities.Champion;
import disibot.league.api.entities.ClashTeam;
import disibot.league.api.entities.ClashTeamMember;
import disibot.league.api.entities.CurrentGameInfo;
import disibot.league.api.entities.CurrentGameParticipant;
import disibot.league.api.entities.LeagueSummoner;
import disibot.league.api.entities.Position;
import disibot.league.api.entities.Region;
import disibot.league.api.entities.SummonerQueueInfo;
import disibot.league.api.entities.Team;
import disibot.league.api.exceptions.InternalServerErrorException;
import disibot.league.api.exceptions.InvalidAPIKeyException;
import disibot.league.api.exceptions.LeagueAPIException;
import disibot.league.api.exceptions.NotFoundException;
import disibot.league.api.impl.LeagueSummonerImpl;
import disibot.league.api.impl.RiotAPIImpl;
import disibot.util.Command;
import disibot.util.Util;
import fakejda.FakeGuild;
import fakejda.FakeJDA;
import fakejda.FakeMember;
import fakejda.FakeMessage;
import fakejda.FakeMessageChannel;
import fakejda.FakeMessageEvent;
import fakejda.FakeTextChannel;
import fakejda.FakeUser;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.stubbing.Answer;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.lang.reflect.Field;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class LeagueCommandHandlerTest {

    LeagueCommandHandler handler;
    FakeTextChannel channel;
    FakeGuild guild = new FakeGuild("FG7");
    FakeUser user = new FakeUser(7777);
    FakeMember member = new FakeMember(guild, user);
    LeagueSummoner goldSummoner;
    LeagueSummoner gmSummoner;
    RiotAPI riotAPI;

    private static void assertLastMessage(String expectedLastMessage, @Nonnull FakeMessageChannel channel) {
        assertTrue(channel.getMessages().size() > 0);
        assertEquals(expectedLastMessage, channel.getMessages().get(channel.getMessages().size() - 1).getContentRaw());
    }

    @BeforeAll
    static void prepare() throws IOException {
        if (Files.exists(Path.of("summoner_ranked_storage.json"))) {
            Files.delete(Path.of("summoner_ranked_storage.json"));
        }
    }

    @BeforeEach
    void setUp() throws IOException, LeagueAPIException, NoSuchFieldException, IllegalAccessException {
        riotAPI = mock(RiotAPIImpl.class);
        LeagueSummoner unrankedSummoner = mock(LeagueSummonerImpl.class);
        when(unrankedSummoner.getName()).thenReturn("TestUnrankedSummoner");
        when(riotAPI.getSummonerByName("TestUnrankedSummoner")).thenReturn(unrankedSummoner);
        when(riotAPI.getSummonerByName("NotASummoner")).then((Answer<LeagueSummoner>) invocation -> {
            throw new NotFoundException();
        });
        when(riotAPI.getSummonerByName("ISESummoner")).then((Answer<LeagueSummoner>) invocation -> {
            throw new InternalServerErrorException();
        });
        when(riotAPI.getSummonerByName("ApiKeyExpSummoner")).then((Answer<LeagueSummoner>) invocation -> {
            throw new InvalidAPIKeyException();
        });
        setupGoldSummoner(riotAPI);
        setupGMSummoner(riotAPI);
        when(riotAPI.getSummonerByName("TestGoldSummoner")).thenReturn(goldSummoner);
        when(riotAPI.getSummonerById(goldSummoner.getId())).thenReturn(goldSummoner);
        when(riotAPI.getSummonerByName("TestGMSummoner")).thenReturn(gmSummoner);
        when(riotAPI.getSummonerById(gmSummoner.getId())).thenReturn(gmSummoner);
        setupCurrentGame(goldSummoner, unrankedSummoner, gmSummoner);
        FakeJDA jda = new FakeJDA();
        jda.addUser(user);

        handler = new LeagueCommandHandler(null, jda, riotAPI, SummonerRankedStorage.fromDisk(riotAPI, jda));
        channel = new FakeTextChannel();
    }

    private void setupGoldSummoner(RiotAPI api) throws IOException, LeagueAPIException, NoSuchFieldException, IllegalAccessException {
        goldSummoner = mock(LeagueSummonerImpl.class);
        when(goldSummoner.getName()).thenReturn("TestGoldSummoner");
        when(goldSummoner.getId()).thenReturn("gold-summoner-id");
        SummonerQueueInfo gold = createGold(goldSummoner, riotAPI);
        when(goldSummoner.getSoloQueueInfo()).thenReturn(gold);
        when(goldSummoner.getTFTQueueInfo()).thenReturn(gold);
        List<ClashTeamMember> teamMembers = new ArrayList<>();
        when(goldSummoner.getClashTeams()).then((Answer<List<ClashTeamMember>>) invocation -> teamMembers);
    }

    private void setupGMSummoner(RiotAPI api) throws IOException, LeagueAPIException, NoSuchFieldException, IllegalAccessException {
        gmSummoner = mock(LeagueSummonerImpl.class);
        when(gmSummoner.getName()).thenReturn("TestGMSummoner");
        when(gmSummoner.getId()).thenReturn("gm-sum-id");
        SummonerQueueInfo gm = createGM(gmSummoner, api);
        when(gmSummoner.getSoloQueueInfo()).thenReturn(gm);
        List<ClashTeamMember> teamMembers = new ArrayList<>();
        ClashTeamMember member = mock(ClashTeamMember.class);
        when(member.getPosition()).thenReturn(Position.MIDDLE);
        when(member.getSummoner()).thenReturn(gmSummoner);
        ClashTeam team = mock(ClashTeam.class);
        when(member.getTeam()).thenReturn(team);
        teamMembers.add(member);
        when(team.getPlayers()).thenReturn(teamMembers);
        when(gmSummoner.getClashTeams()).then((Answer<List<ClashTeamMember>>) invocation -> teamMembers);
    }

    private void setupCurrentGame(LeagueSummoner goldSummoner, LeagueSummoner unrankedSummoner, LeagueSummoner gmSummoner) throws IOException, LeagueAPIException {
        CurrentGameInfo gameInfo = mock(CurrentGameInfo.class);
        when(gameInfo.getStartTime()).then((Answer<LocalDateTime>) invocation -> Util.ofEpochMillis(1005416548L));
        when(gameInfo.getRegion()).then((Answer<Region>) invocation -> Region.EUW1);
        when(gameInfo.getGameMode()).then((Answer<String>) invocation -> "CLASSIC");
        when(gameInfo.getGameType()).then((Answer<String>) invocation -> "MATCHED_GAME");
        List<CurrentGameParticipant> participants = new ArrayList<>();
        CurrentGameParticipant p0 = mock(CurrentGameParticipant.class);
        CurrentGameParticipant p1 = mock(CurrentGameParticipant.class);
        CurrentGameParticipant p2 = mock(CurrentGameParticipant.class);
        when(p0.getSummoner()).thenReturn(goldSummoner);
        when(p1.getSummoner()).thenReturn(unrankedSummoner);
        when(p2.getSummoner()).thenReturn(gmSummoner);
        when(p0.getSummonerName()).then((Answer<String>) invocation -> goldSummoner.getName());
        when(p1.getSummonerName()).then((Answer<String>) invocation -> unrankedSummoner.getName());
        when(p2.getSummonerName()).then((Answer<String>) invocation -> gmSummoner.getName());
        when(p0.getTeam()).then((Answer<Team>) invocation -> Team.BLUE);
        when(p1.getTeam()).then((Answer<Team>) invocation -> Team.RED);
        when(p2.getTeam()).then((Answer<Team>) invocation -> Team.RED);
        Champion champion = mock(Champion.class);
        when(champion.getName()).then((Answer<String>) invocation -> "TestChamp");
        when(p0.getChampion()).then((Answer<Champion>) invocation -> champion);
        when(p1.getChampion()).then((Answer<Champion>) invocation -> champion);
        when(p2.getChampion()).then((Answer<Champion>) invocation -> champion);
        participants.add(p0);
        participants.add(p1);
        participants.add(p2);
        when(gameInfo.getParticipants()).then((Answer<List<CurrentGameParticipant>>) invocation -> participants);
        when(goldSummoner.getCurrentGame()).then((Answer<CurrentGameInfo>) invocation -> gameInfo);
    }

    private SummonerQueueInfo createGold(LeagueSummoner summoner, RiotAPI api) throws IllegalAccessException, IOException, LeagueAPIException, NoSuchFieldException {
        SummonerQueueInfo info = SummonerQueueInfo.createEmptyQueueInfo(summoner);
        Field tierField = SummonerQueueInfo.class.getDeclaredField("tier");
        tierField.setAccessible(true);
        tierField.set(info, "GOLD");
        Field summonerIdField = SummonerQueueInfo.class.getDeclaredField("summonerId");
        summonerIdField.setAccessible(true);
        summonerIdField.set(info, summoner.getId());
        info.setAPI((RiotAPIImpl) api);
        when(summoner.getSoloQueueInfo()).then((Answer<SummonerQueueInfo>) invocation -> info);
        return info;

    }

    private SummonerQueueInfo createGM(LeagueSummoner summoner, RiotAPI api) throws IllegalAccessException, IOException, LeagueAPIException, NoSuchFieldException {
        SummonerQueueInfo info = SummonerQueueInfo.createEmptyQueueInfo(summoner);
        Field tierField = SummonerQueueInfo.class.getDeclaredField("tier");
        tierField.setAccessible(true);
        tierField.set(info, "GRANDMASTER");
        Field summonerIdField = SummonerQueueInfo.class.getDeclaredField("summonerId");
        summonerIdField.setAccessible(true);
        summonerIdField.set(info, summoner.getId());
        info.setAPI((RiotAPIImpl) api);
        when(summoner.getSoloQueueInfo()).then((Answer<SummonerQueueInfo>) invocation -> info);
        return info;
    }

    @Test
    void testHandleLOLCommand() {
        testNoSummoner();
        testUnranked();
        testGold();
        testNotExist();
        testISE();
        testApiExp();
    }

    private void testNoSummoner() {
        //todo slash
        //sendCommand("lol rank");
        //assertEquals(1, channel.getMessages().size());
        //assertLastMessage("Please specify a summoner name or use !setsummoner to save your own summoner name.", channel);
    }

    private void testUnranked() {
        //todo slash
        //sendCommand("lol rank TestUnrankedSummoner");
        //assertEquals(2, channel.getMessages().size());
        //assertLastMessage("Summoner TestUnrankedSummoner currently not ranked in soloQ.", channel);
    }

    private void testGold() {
        //todo slash
        //sendCommand("lol rank TestGoldSummoner");
        //assertEquals(3, channel.getMessages().size());
        //assertLastMessage("TestGoldSummoner is currently in Gold I with 0LP.", channel);
    }

    private void testNotExist() {
        //todo slash
        //sendCommand("lol rank NotASummoner");
        //assertEquals(4, channel.getMessages().size());
        //assertLastMessage("Could not find summoner name.", channel);
    }

    private void testISE() {
        //todo slash
        //sendCommand("lol rank ISESummoner");
        //assertEquals(5, channel.getMessages().size());
        //assertLastMessage("There was an error accessing the league of legends api.", channel);
    }

    private void testApiExp() {
        //todo slash
        //sendCommand("lol rank ApiKeyExpSummoner");
        //assertEquals(6, channel.getMessages().size());
        //assertLastMessage("The league api key is expired.", channel);
    }

    @Test
    void testHandleOPGGCommand() {
        sendCommand("opgg SomeName");
        assertLastMessage("https://euw.op.gg/summoner/userName=SomeName", channel);
        sendCommand("opgg");
        assertLastMessage("Usage: !opgg [summoner name] - if a league of legends summoner is linked summoner name is optional", channel);
        // set summerner to fake user
        sendCommand("lol setsummoner TestGoldSummoner");
        sendCommand("opgg");
        assertLastMessage("https://euw.op.gg/summoner/userName=TestGoldSummoner", channel);
        assertEquals(4, channel.getMessages().size());
        sendCommand("lol setsummoner");
    }

    @Test
    void testLegal() {
        sendCommand("lol riot");
        assertEquals(1, channel.getMessages().size());
        assertEquals("t endorsed by Riot Games and", channel.getMessages().get(channel.getMessages().size() - 1).getContentRaw().substring(12,40));
    }

    @Test
    void testHelp() {
        sendCommand("lol help");
        assertEquals(1, channel.getMessages().size());
        assertLastMessage("lol - League of Legends functionality\n" +
                "```syntax: !lol command\n" +
                "commands:\n" +
                "  help - this help\n" +
                "  setsummoner NAME - sets your summoner name, leave NAME blank to remove summoner\n" +
                "  rank [NAME] - gets the soloQ rank of the specified summoner\n" +
                "  games [NAME] - gets the number of soloQ games played in current week\n" +
                "  live [NAME] - gets information about the currently played game\n" +
                "  riot - Riot Games legal note```", channel);
    }

    @Test
    void testLolLiveNoName() {
        //todo slash
        //sendCommand("lol live ");
        //assertEquals(1, channel.getMessages().size());
        //assertLastMessage("Please specify a summoner name or use !setsummoner to save your own summoner name.", channel);
    }

    @Test
    void testLolLiveNoGame() {
        //todo slash
        //sendCommand("lol live TestUnrankedSummoner");
        //assertEquals(1, channel.getMessages().size());
        //assertLastMessage("TestUnrankedSummoner is currently not in a game.", channel);
    }

    @Test
    void testLolLive() {
        //todo slash
        /*sendCommand("lol live TestGoldSummoner");
        assertEquals(1, channel.getEmbeds().size());
        assertEquals("Live game", channel.getEmbeds().get(0).getTitle());
        assertEquals("MATCHED_GAME CLASSIC EUW1", channel.getEmbeds().get(0).getFooter().getText());
        assertEquals("https://euw.op.gg/summoner/userName=TestGoldSummoner", channel.getEmbeds().get(0).getUrl());
        assertEquals(0x5383e8, channel.getEmbeds().get(0).getColorRaw());
        assertEquals(4, channel.getEmbeds().get(0).getFields().size()); // team red x2, team blue and a spacer
        assertEquals("TestUnrankedSummoner", channel.getEmbeds().get(0).getFields().get(0).getName());
        assertEquals("TestGMSummoner", channel.getEmbeds().get(0).getFields().get(1).getName());
        assertEquals("\u200E", channel.getEmbeds().get(0).getFields().get(2).getName()); // is LRM
        assertEquals("TestGoldSummoner", channel.getEmbeds().get(0).getFields().get(3).getName());
        assertEquals("TestChamp", channel.getEmbeds().get(0).getFields().get(0).getValue());
        assertEquals("TestChamp - GM", channel.getEmbeds().get(0).getFields().get(1).getValue());
        assertEquals("TestChamp - G1", channel.getEmbeds().get(0).getFields().get(3).getValue());
        sendCommand("lol live NotASummoner");
        assertLastMessage("Could not find summoner name.", channel);*/
    }

    @Test
    void testSetSummoner() throws NoSuchFieldException, IllegalAccessException {
        sendCommand("lol setsummoner TestGoldSummoner");
        assertLastMessage("Set FakeUser's summoner name to TestGoldSummoner.", channel);
        Field rankedStorageField = LeagueCommandHandler.class.getDeclaredField("rankedStorage");
        rankedStorageField.setAccessible(true);
        SummonerRankedStorage storage = (SummonerRankedStorage) rankedStorageField.get(handler);
        assertEquals(goldSummoner, storage.getUserSummonerMap().get(user));
        sendCommand("lol setsummoner");
        assertLastMessage("Removed FakeUser's summoner entry.", channel);
        sendCommand("lol setsummoner NotASummoner");
        assertEquals(3, channel.getMessages().size());
        assertLastMessage("Could not find summoner name \"NotASummoner\".", channel);
        // clear up
        sendCommand("lol setsummoner");
        assertFalse(storage.getUserSummonerMap().containsKey(user));
    }

    @Test
    void testLolGameCount() throws NoSuchFieldException, IllegalAccessException, IOException, LeagueAPIException {
        sendCommand("lol games NotASummoner");
        assertLastMessage("Could not find summoner name.", channel);
        sendCommand("lol games TestGoldSummoner");
        assertLastMessage("Summoner's games are not counted", channel);
        sendCommand("lol games TestUnrankedSummoner");
        assertLastMessage("Summoner TestUnrankedSummoner currently not ranked in soloQ.", channel);
        // setup old data
        Field rankedStorageField = LeagueCommandHandler.class.getDeclaredField("rankedStorage");
        rankedStorageField.setAccessible(true);
        SummonerRankedStorage storage = (SummonerRankedStorage) rankedStorageField.get(handler);
        Field snapshotsField = SummonerRankedStorage.class.getDeclaredField("snapshots");
        snapshotsField.setAccessible(true);
        List<SummonerRankedStorage.SummonerRankedSnapshot> snapshots = (List<SummonerRankedStorage.SummonerRankedSnapshot>) snapshotsField.get(storage);
        SummonerRankedStorage.SummonerRankedSnapshot snapshot = new SummonerRankedStorage.SummonerRankedSnapshot();
        Field qiField = SummonerRankedStorage.SummonerRankedSnapshot.class.getDeclaredField("summonerQueueInfos");
        qiField.setAccessible(true);
        List<SummonerQueueInfo> summonerQueueInfos = new ArrayList<>();
        SummonerQueueInfo gm = createGM(gmSummoner, riotAPI);
        SummonerQueueInfo gold = createGold(goldSummoner, riotAPI);
        summonerQueueInfos.add(gold);
        summonerQueueInfos.add(null);
        summonerQueueInfos.add(gm);
        snapshots.add(snapshot);
        qiField.set(snapshot, summonerQueueInfos);
        // test for old data
        sendCommand("lol games TestGMSummoner");
        assertLastMessage("TestGMSummoner has played 0 games since last Thursday 18:00", channel);
        assertEquals(4, channel.getMessages().size());
    }


    @Test
    void testLolClash() {
        // todo slash
        /*sendCommand("lol clash TestGoldSummoner");
        assertLastMessage("Player is not in a clash team.", channel);
        sendCommand("lol clash NotASummoner");
        assertLastMessage("Could not find summoner name.", channel);
        sendCommand("lol clash TestGMSummoner");
        assertLastMessage("https://euw.op.gg/multi/query=TestGMSummoner%2C", channel);*/
    }

    @Test
    void testTFT() throws NoSuchFieldException, IllegalAccessException {
        sendCommand("tft help");
        assertEquals(1, channel.getMessages().size());
        assertLastMessage("TFT commands are disabled.", channel);
        // enable tft for testing
        Field tftField = LeagueCommandHandler.class.getDeclaredField("TFT_DISABLED");
        tftField.setAccessible(true);
        boolean tftOldValue = tftField.getBoolean(handler);
        tftField.setBoolean(handler, false);
        // test tft
        sendCommand("tft rank NotASummoner");
        assertLastMessage("Could not find summoner name.", channel);
        sendCommand("tft rank TestUnrankedSummoner");
        assertLastMessage("Summoner is currently not ranked in TFT.", channel);
        sendCommand("tft rank TestGoldSummoner");
        assertLastMessage("TestGoldSummoner is currently in Gold I with 0LP.", channel);
        sendCommand("tft not_a_sub");
        assertLastMessage("Usage: !tft rank [summoner name]", channel);
        sendCommand("tft rank");
        assertLastMessage("Please specify a summoner name or use !setsummoner to save your own summoner name.", channel);
        sendCommand("tft rank ISESummoner");
        assertLastMessage("There was an error accessing the league of legends api.", channel);
        sendCommand("tft rank ApiKeyExpSummoner");
        assertLastMessage("The league api key is expired.", channel);
        assertEquals(8, channel.getMessages().size());
        // reset tft field
        tftField.setBoolean(handler, tftOldValue);
    }


    @Test
    void testInvalid() {
        sendCommand("lol not_a_sub");
        assertLastMessage("unknown command try !lol help", channel);
        sendCommand("not_a_command"); // nothing
        assertEquals(1, channel.getMessages().size());
    }

    @SuppressWarnings("ConstantConditions")
    @Test
    void testCanHandleCommand() {
        assertTrue(handler.canHandleCommand(new Command("lol"), null));
        assertTrue(handler.canHandleCommand(new Command("tft"), null));
        assertTrue(handler.canHandleCommand(new Command("opgg"), null));
        assertTrue(handler.canHandleCommand(new Command("lol games"), null));
        assertFalse(handler.canHandleCommand(new Command("opg"), null));
        assertFalse(handler.canHandleCommand(new Command("lolx"), null));
        assertFalse(handler.canHandleCommand(new Command("!lol"), null));
        assertFalse(handler.canHandleCommand(new Command("!"), null));
        assertFalse(handler.canHandleCommand(new Command(""), null));
        assertThrows(Exception.class, () -> handler.canHandleCommand(null, null));
    }

    private void sendCommand(String command) {
        FakeMessage message = new FakeMessage("!" + command, channel);
        handler.handleCommand(new Command(command), new FakeMessageEvent(message, member));
    }
}